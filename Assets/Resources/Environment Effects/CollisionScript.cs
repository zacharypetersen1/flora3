﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionScript : MonoBehaviour {
    int waveNumber;
    public float distanceX, distanceZ, distanceY;
    public float[] waveAmplitude;
    public float magnitudeDivider;
    public Vector2[] impactPos;
    public float[] distance;
    public float speedWaveSpread;
    Vector2 mag;

    Mesh mesh;
    Material material;
	// Use this for initialization
	void Start () {
        mesh = GetComponent<MeshFilter>().mesh;
        material = GetComponent<MeshRenderer>().material;

        waveAmplitude = new float[4];
        impactPos = new Vector2[4];
        distance = new float[4];
        magnitudeDivider = 0.1f;
        speedWaveSpread = 1;
        mag = new Vector2(1, 0);
    }

    // Update is called once per frame
    void Update()
    {

        for (int i = 0; i < 4; i++)
        {
            waveAmplitude[i] = GetComponent<Renderer>().material.GetFloat("_WaveAmplitude" + (i + 1));
            if (waveAmplitude[i] > 0)
            {
                distance[i] += speedWaveSpread;
                GetComponent<Renderer>().material.SetFloat("_Distance" + (i + 1), distance[i]);
                GetComponent<Renderer>().material.SetFloat("_WaveAmplitude" + (i + 1), waveAmplitude[i] * 0.98f);
            }
            if (waveAmplitude[i] < 0.01)
            {
                GetComponent<Renderer>().material.SetFloat("_WaveAmplitude" + (i + 1), 0);
                distance[i] = 0;
            }

        }
    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "basicAttack")
        {           
            waveNumber++;
            if (waveNumber == 5)
            {
                waveNumber = 1;
            }
            Debug.Log(waveNumber);
            waveAmplitude[waveNumber - 1] = 0;
            distance[waveNumber - 1] = 0;

            distanceX = this.transform.position.x - collision.gameObject.transform.position.x;
            distanceZ = this.transform.position.z - collision.gameObject.transform.position.z;
            //distanceY = this.transform.position.y - collision.gameObject.transform.position.y;

            impactPos[waveNumber - 1].x = collision.transform.position.x;
            impactPos[waveNumber - 1].y = collision.transform.position.z;

            GetComponent<Renderer>().material.SetFloat("_xImpact" + waveNumber, collision.transform.position.x);
            GetComponent<Renderer>().material.SetFloat("_zImpact" + waveNumber, collision.transform.position.z);
            //GetComponent<Renderer>().material.SetFloat("_yImpact" + waveNumber, collision.transform.position.y);


            GetComponent<Renderer>().material.SetFloat("_OffsetX" + waveNumber, distanceX / mesh.bounds.size.x * 2.5f);
            GetComponent<Renderer>().material.SetFloat("_OffsetZ" + waveNumber, distanceZ / mesh.bounds.size.z * 2.5f);
            //GetComponent<Renderer>().material.SetFloat("_OffsetY" + waveNumber, distanceZ / mesh.bounds.size.y * 2.5f);

            GetComponent<Renderer>().material.SetFloat("_WaveAmplitude" + waveNumber, 7 * magnitudeDivider);
            //collision.rigidbody.velocity.magnitude
        }
    }

    /*
    void OnCollisionEnter(Collision col)
    {
        if (col.rigidbody)
        {
            waveNumber++;
            if (waveNumber == 5)
            {
                waveNumber = 1;
            }
            waveAmplitude[waveNumber - 1] = 0;
            distance[waveNumber - 1] = 0;

            distanceX = this.transform.position.x - col.gameObject.transform.position.x;
            distanceZ = this.transform.position.z - col.gameObject.transform.position.z;
            //distanceY = this.transform.position.y - col.gameObject.transform.position.y;
            impactPos[waveNumber - 1].x = col.transform.position.x;
            impactPos[waveNumber - 1].y = col.transform.position.z;

            GetComponent<Renderer>().material.SetFloat("_xImpact" + waveNumber, col.transform.position.x);
            GetComponent<Renderer>().material.SetFloat("_zImpact" + waveNumber, col.transform.position.z);
            //GetComponent<Renderer>().material.SetFloat("_yImpact" + waveNumber, col.transform.position.y);

            GetComponent<Renderer>().material.SetFloat("_OffsetX" + waveNumber, distanceX / mesh.bounds.size.x * 2.5f);
            GetComponent<Renderer>().material.SetFloat("_OffsetZ" + waveNumber, distanceZ / mesh.bounds.size.z * 2.5f);
            //GetComponent<Renderer>().material.SetFloat("_OffsetY" + waveNumber, distanceY / mesh.bounds.size.z * 2.5f);
            
            GetComponent<Renderer>().material.SetFloat("_WaveAmplitude" + waveNumber, col.rigidbody.velocity.magnitude * magnitudeDivider);

        }
    }
    */
}
