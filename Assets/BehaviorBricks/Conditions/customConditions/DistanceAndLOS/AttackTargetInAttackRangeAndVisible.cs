﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/DistanceAndLOS/AttackTargetInAttackRangeAndVisible")]
    [Help("checks if the AttackTarget enemy is within this allys attackRange")]
    public class AttackTargetInAttackRangeAndVisible : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                GameObject target = body.AttackTarget;
                if (target == null)
                {
                    //Debug.Log("nearestEnemy is null in checkAlly");
                    return false;
                }
                ENT_DungeonEntity attackENT = target.GetComponent<ENT_DungeonEntity>();
                if (attackENT == null)
                {
                    return false;
                }
                //optimized to remove square root operations
                return UTL_Math.sqrDistance(attackENT.getNearestAttackablePoint(gameObject.transform.position), gameObject.transform.position) < body.attackRange * body.attackRange;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}