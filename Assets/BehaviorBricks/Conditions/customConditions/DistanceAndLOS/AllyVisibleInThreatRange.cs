﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/DistanceAndLOS/AllyVisibleInThreatRange")]
    [Help("checks if any allies (including twig) are visible and within the minions threatRange")]
    public class AllyVisibleInThreatRange : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestAlly = am.getNearestVisibleAlly(body, body.threatRange, true); //including twig
                if (nearestAlly == null) return false;
                if (Vector3.Distance(nearestAlly.transform.position, gameObject.transform.position) > body.threatRange) return false;
                if (!body.isFacing(nearestAlly.transform.position)) return false;
                //if (!UTL_Dungeon.checkLOSWalls(UTL_Math.vec3ToVec2(gameObject.transform.position), UTL_Math.vec3ToVec2(nearestEnemy.transform.position))) return false;
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}