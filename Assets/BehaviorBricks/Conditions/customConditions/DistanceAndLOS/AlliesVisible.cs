﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/DistanceAndLOS/AlliesVisible")]
    [Help("checks if any allies are within LOS and sightRange (use to see if you can see an ally)")]
    public class AlliesVisible : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestAlly = am.getNearestVisibleAlly(body, body.sightRange);
                if (nearestAlly == null) return false;
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}