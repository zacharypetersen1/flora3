﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/Distance/PlayerWithinDistance")]
    [Help("checks if the monster is near the player.")]
    public class PlayerWithinDistance : GOCondition
    {
        [InParam("distance")]
        [Help("Distance at which the player should be detected")]
        public float distance;

        public override bool Check()
        {
            //Debug.Log("checkPlayerInDistance");
            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                if (distance == 0)
                    return monsterBody.isNearPlayer();
                else
                    return monsterBody.isNearPlayer(distance);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}