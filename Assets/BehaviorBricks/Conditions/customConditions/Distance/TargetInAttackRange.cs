﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/Distance/TargetinAttackRange")]
    [Help("checks if an enemy is within the minions attackRange")]
    public class TargetInAttackRange : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestEnemy(gameObject.transform.position, false);
                if (nearestEnemy == null)
                    return false;
                ENT_DungeonEntity enemyENT = nearestEnemy.GetComponent<ENT_DungeonEntity>();
                float sqrRange = body.attackRange * body.attackRange;
                bool result = UTL_Math.sqrDistance(enemyENT.getNearestAttackablePoint(gameObject.transform.position), gameObject.transform.position) < sqrRange;
                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}