﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/WithinIndividualFollowRadius")]
    [Help("Checks if the monster is within IndividualFollowRadius of Twig")]
    public class WithinIndividualFollowRadius : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                return body.amINearTwig();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}