﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/NotWithinFarWaitRadius")]
    [Help("Checks if the monster is notwithin PLY_AllyManager.DISTANCE_TO_STRAY_FROM_GOTO")]
    public class NotWithinFarWaitRadius : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                //Debug.Log("self not near twig res: " + !body.amINearTwig(gameObject.transform.position));
                bool result = Vector3.Distance(body.WaitPos, body.transform.position) > PLY_AllyManager.DISTANCE_TO_STRAY_FROM_GOTO;
                //Debug.Log("notWithinFarWaitRadius: " + result);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}