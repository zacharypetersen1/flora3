﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/InAttackRangeOfThreatToTwig")]
    [Help("checks if the current ally is in range of attacking the threat to twig")]
    public class InAttackRangeOfThreatToTwig : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject threat = am.getNearestEnemyToTwig();
                if (threat == null) return false;
                if (Vector3.Distance(threat.transform.position, gameObject.transform.position) > body.attackRange) return false;
                if (!body.isFacing(threat.transform.position)) return false;
                //if (!UTL_Dungeon.checkLOSWalls(UTL_Math.vec3ToVec2(gameObject.transform.position), UTL_Math.vec3ToVec2(nearestAlly.transform.position))) return false;
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}