﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;
using System.Collections.Generic;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/EnemyNearWaitPos")]
    [Help("Checks if an enemy is near their waitPos")]
    public class EnemyNearWaitPos : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                float nearDist = 10f;
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                List<GameObject> enemiesNearPos = body.allyManager.getAllEnemiesWithinRange(body.myGoToPos, nearDist);
                return enemiesNearPos.Count > 0;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}