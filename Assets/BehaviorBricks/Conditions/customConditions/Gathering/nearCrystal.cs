﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/Gathering/nearCrystal")]
    [Help("checks if a minion is within pickupRange of it's target crystal")]
    public class nearCrystal : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                //holy mother of god this line of code should be better
                //basically trying to request a pickup from the nearest pathable crystal
                //should be refactored so it will try other crystals if first is full.
                return body.nearCrystal();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}