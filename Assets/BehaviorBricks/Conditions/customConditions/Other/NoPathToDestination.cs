﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;
using UnityEngine.AI;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/NoPathToDestination")]
    [Help("Checks if the minion does not have a path to its destination")]
    public class NoPathToDestination : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                NavMeshAgent agent = body.Agent;
                float remainingDist = agent.remainingDistance;
                float directDist;
                if ((agent.pathPending || remainingDist == Mathf.Infinity) && body.pathTimer < 1f) // if path is not ready then for one second
                {
                    //Debug.Log("pathPending");
                    return false;
                }
                else if (agent.pathStatus == NavMeshPathStatus.PathComplete) // if path reaches destination only teleport if its very out of the way
                {
                    remainingDist = agent.remainingDistance;
                    directDist = Vector3.Distance(gameObject.transform.position, agent.destination);
                    //Debug.Log("remainingDist: " + remainingDist + " directDist: " + directDist);
                    return ((remainingDist > directDist * 2 && remainingDist > 20));
                }
                else
                {
                    //Debug.Log("No path possible");
                    return true; //if we get here we have no path and we must teleport
                }
                //Debug.Log("remainingDist: " + remainingDist + " directDist: " + directDist);
                 //|| (remainingDist == 0 && directDist > 3)); // || remainingDist > directDist + 10
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}