﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/WaitSearch")]
    [Help("wait for one second at the start of the search tree")]
    public class WaitSearch : GOAction
    {
        float timer = 1F;

        public override void OnStart()
        {

        }

        public override TaskStatus OnUpdate()
        {
            timer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            if (timer <= 0F)
            {
                gameObject.GetComponent<ENT_Body>().startSearch = false;
                return TaskStatus.COMPLETED;
            }
            return TaskStatus.RUNNING;
        }
    }
}