﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/TeleportToGoToPos")]
    [Help("teleports the monster to a their myGoToPos")]
    public class TeleportToGoToPos : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;
        ENT_Body monsterBody;
        public override void OnStart()
        {
            monsterBody = gameObject.GetComponent<ENT_Body>();
            monsterBody.teleport(monsterBody.myGoToPos);
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}