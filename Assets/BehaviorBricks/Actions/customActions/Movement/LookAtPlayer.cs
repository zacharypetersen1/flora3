﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/LookAtPlayer")]
    [Help("rotate to look towards the player")]
    public class LookAtPlayer : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body monster = gameObject.GetComponent<ENT_Body>();
                //Debug.Log(gameObject.name + "got to lookAtPlayer");
                monster.shouldRotate = true;
                monster.lookTowardsVec = monster.Player.transform.position;
                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}