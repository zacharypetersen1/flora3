﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/Hangout")]
    [Help("Tells the character to hangout at a target position when they don't see any enemies")]
    public class Hangout : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("Hangout");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.hangout();
            }
            catch
            {
                throw new UnityException("Hangout behavior is broken and sad");
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
