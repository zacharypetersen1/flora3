﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToPositionNonblocking")]
    [Help("starts character moving to target position. returns immediately rather than waiting for move to complete")]
    public class NonblockingMoveToPosition : GOAction
    {
        public Vector2 targetPosition;
        private ENT_Body monster;

        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                monster = gameObject.GetComponent<ENT_Body>();
                monster.goToPosition(targetPosition);
                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}