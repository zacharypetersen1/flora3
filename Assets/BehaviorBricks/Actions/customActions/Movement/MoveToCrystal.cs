﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToCrystal")]
    [Help("Moves the monster towards the nearest Crystal")]
    public class moveToCrystal : GOAction
    {
        public override void OnStart()
        {
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.crystal = body.CrystalSpawner.getNearestCrystal(gameObject.transform.position);
                body.goToTargetCrystal();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}