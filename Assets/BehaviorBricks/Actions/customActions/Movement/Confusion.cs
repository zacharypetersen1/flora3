﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/Confusion")]
    [Help("makes the minion wander around")]
    public class Confusion : GOAction
    {


        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body monster = gameObject.GetComponent<ENT_Body>();
                monster.traverseConfusedPath();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
