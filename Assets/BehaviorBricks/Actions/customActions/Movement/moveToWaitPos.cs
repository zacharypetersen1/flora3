﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToWaitPos")]
    [Help("Moves the monster to a specified position")]
    public class MoveToWaitPos : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;
        ENT_Body monsterBody;
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                monsterBody = gameObject.GetComponent<ENT_Body>();

                /*
                //Debug.Log("waitPos: " + waitPos);
                Vector3 vec = gameObject.transform.position - monsterBody.WaitPos;
                float magnitude = vec.magnitude;
                Vector3 dirToTarget = vec / magnitude;
                Vector3 finalPos = dirToTarget * (monsterBody.myFollowDistance);
                finalPos = monsterBody.WaitPos + finalPos;
                finalPos = monsterBody.WaitPos;
                */

                Vector3 vec = gameObject.transform.position - monsterBody.WaitPos;
                Vector3 dirToTarget = vec.normalized;
                Vector3 finalPos = dirToTarget * (monsterBody.myFollowDistance);
                finalPos += monsterBody.WaitPos;

                if (!monsterBody.isAtPosition(finalPos) && Vector3.Distance(finalPos, monsterBody.WaitPos) < Vector3.Distance(gameObject.transform.position, monsterBody.WaitPos))
                {
                    monsterBody.goToPosition(finalPos);
                }
                else
                {
                    monsterBody.dontMove();
                }

                //monsterBody.goToPosition(monsterBody.WaitPos);
                //monster.ActionFunctionName();
            }
            catch
            {
                throw new UnityException("MoveToWaitPos is broken and sad");
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}