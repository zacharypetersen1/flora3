﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToNearestRemnant")]
    [Help("Moves the necro minion towards the nearest remnant")]
    public class MoveToNearestRemnant : GOAction
    {
        public override void OnStart()
        {
            ENT_NecroMinion body = gameObject.GetComponent<ENT_NecroMinion>();
            body.goToNearestRemnant();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}