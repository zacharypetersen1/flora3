﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/EnemyNecroAbility")]
    [Help("activates the necro ability on a necromancer minion that is an enemy")]
    public class EnemyNecroAbility : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("attack");  
            try
            {
                ENT_NecroMinion monsterBody = gameObject.GetComponent<ENT_NecroMinion>();
                //need this check as otherwise BB will sometimes run this node once more before switching trees
                if (!monsterBody.IsAbilityActive)
                {
                    gameObject.GetComponent<ENT_MinionAbility>().activateEnemyAbility();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;

        }
    }
}

