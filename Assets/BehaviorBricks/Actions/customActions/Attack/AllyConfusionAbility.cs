﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/AllyConfusionAbility")]
    [Help("activates the confusion ability on a confuion minion that is an ally")]
    public class AllyConfusionAbility : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("attack");  
            try
            {
                ENT_ConfusionMinion monsterBody = gameObject.GetComponent<ENT_ConfusionMinion>();
                //need this check as otherwise BB will sometimes run this node once more before switching trees
                if (!monsterBody.IsAbilityActive && monsterBody.CurrentTree == ENT_Body.BehaviorTree.AllyActivateTree)
                {
                    monsterBody.Confusion();
                }
                monsterBody.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree; //reset to previous tree
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;

        }
    }
}

