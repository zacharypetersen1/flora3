﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/EnemySlowAbility")]
    [Help("activates the slow ability on a slow minion that is an enemy")]
    public class EnemySlowAbility : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("attack");  
            try
            {
                ENT_SlowMinion monsterBody = gameObject.GetComponent<ENT_SlowMinion>();
                //need this check as otherwise BB will sometimes run this node once more before switching trees
                
                if (!monsterBody.IsAbilityActive)
                {
                    //Debug.Log("SLOW");
                    monsterBody.Slow();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;

        }
    }
}

