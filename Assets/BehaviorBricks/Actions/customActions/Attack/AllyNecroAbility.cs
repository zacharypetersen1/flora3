﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/AllyNecroAbility")]
    [Help("activates the slow ability on a slow minion that is an ally")]
    public class AllyNecroAbility : GOAction
    {
        float timer = 1f;
        public override void OnStart()
        {
            //Debug.Log("attack");  
            timer = 1f;      
            try
            {
                ENT_NecroMinion monsterBody = gameObject.GetComponent<ENT_NecroMinion>();
                //need this check as otherwise BB will sometimes run this node once more before switching trees
                if (!monsterBody.IsAbilityActive && monsterBody.CurrentTree == ENT_Body.BehaviorTree.AllyActivateTree)
                {
                    gameObject.GetComponent<ENT_MinionAbility>().activateAllyAbility();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            timer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            if(timer <= 0)
            {
                return TaskStatus.COMPLETED;
            }else
            {
                return TaskStatus.RUNNING;
            }
            
        }
    }
}

