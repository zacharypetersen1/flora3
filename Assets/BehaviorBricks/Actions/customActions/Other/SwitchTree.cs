﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Other/SwitchTree")]
    [Help("Don't use this broken behavior")]
    public class SwitchTree : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");
            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                // Don't use this behavior
                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}