﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Other/SwitchTreeWander")]
    [Help("switches to the monster work tree")]
    public class SwitchTreeWander : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("ChangeGoal");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.CurrentTree = ENT_Body.BehaviorTree.WorkTree; //actually sets it to work tree
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
