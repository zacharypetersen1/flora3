﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Other/SwitchToLastTree")]
    [Help("switches to the monster attack tree")]
    public class SwitchToLastTree : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("ChangeGoal");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                
                if (body.LastTree == ENT_Body.BehaviorTree.ConfusionTree)
                {
                    body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                    body.commandGroup.removeFromList(gameObject);
                    body.allyManager.followGroup.addToList(gameObject);
                    body.commandGroup = body.allyManager.followGroup;
                }
                else
                {
                    body.CurrentTree = body.LastTree;
                    body.commandGroup.removeFromList(gameObject);
                    body.lastGroup.addToList(gameObject);
                    //body.lastGroup.removeFromList(gameObject);
                    body.commandGroup = body.lastGroup;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
