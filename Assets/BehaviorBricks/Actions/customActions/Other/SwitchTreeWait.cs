﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{
    [Action("Custom/Other/SwitchTreeWait")]
    [Help("switches to the wait treee")]
    public class SwitchTreeWait : GOAction
    {
        public override void OnStart()
        {
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree; 
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
