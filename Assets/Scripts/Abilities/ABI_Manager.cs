﻿// Zach

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ABI_Manager : MonoBehaviour {


    public static ABI_Manager manager;



    //
    // Contains all of the different types of abilities
    //
    public enum abilityType
    {
        Cleansing, Moving, Purify, CommandActivate, CommandGoTo, CommandFollow, Vines
    }



    //
    // maps ability numbers to ability types
    //
    public Dictionary<int, abilityType> abilityNumToName = new Dictionary<int, abilityType>
    {
        {0, abilityType.Moving },
        {1, abilityType.Cleansing },
        {3, abilityType.CommandActivate },
        {4, abilityType.CommandGoTo },
        {5, abilityType.CommandFollow }

    };



    //
    // maps ability name to ability class
    //
    public Dictionary<abilityType, ABI_Ability> abilities;



    //
    // Runs once when obj is created
    //
    void Start()
    {
        manager = this;
        PLY_PlayerDriver driver = gameObject.GetComponent<PLY_PlayerDriver>();
        
        // initialize all abilities
        abilities = new Dictionary<abilityType, ABI_Ability>
        {
            { abilityType.Cleansing,        new ABI_Purify(driver, gameObject, this) },
            { abilityType.Moving,           new ABI_Moving(driver, gameObject, this) },
            { abilityType.CommandActivate,  new ABI_CommandActivate(driver, gameObject, this) },
            { abilityType.CommandGoTo,      new ABI_CommandGoTo(driver, gameObject, this) },
            { abilityType.CommandFollow,    new ABI_CommandFollow(driver, gameObject, this) }
        };

        // call setup function that is exposed for implimenting ability-specific stuff
        foreach(var ability in abilities.Values)
        {
            ability.start();
        }
    }



    //
    // Per frame operations on abilities
    //
    void Update()
    {
        foreach (abilityType a in abilityNumToName.Values)
        {

            // if ability is not ready, recharge it
            if (!abilityIsReady(a))
            {
                abilities[a].curCharge -= Time.deltaTime;

                // make sure charge doesn't go below 0
                if (abilities[a].curCharge < 0)
                {
                    abilities[a].curCharge = 0;
                }
            }
        }
    }



    //
    // checks if ability is recharged and ready to use
    //
    public bool abilityIsReady(abilityType a)
    {
        return abilities[a].curCharge == 0;
    }
    public bool abilityIsReady(int abilityNum)
    {
        return abilities[abilityNumToName[abilityNum]].curCharge == 0;
    }


    //
    // returns scalar from [0, 1] representing how much ability is recharged. 1 = is recharged
    //
    public float getAbilityChargeScalar(int abilityNum)
    {
        abilityType a = abilityNumToName[abilityNum];
        return (abilities[a].getRechargeTime() - abilities[a].curCharge) / abilities[a].getRechargeTime();
    }



    //
    //return amount of time left on recharge
    //
    public float getCurCharge(abilityType a)
    {
        return abilities[a].curCharge;
    }



    //
    // reduces amount of charge in ability bar based on how much charge should be lost for one use
    //
    public void reduceAbilityCharge(abilityType a)
    {
        abilities[a].curCharge = abilities[a].getRechargeTime();
    }



    //
    // checks mute status of an ability
    //
    public bool isMuted(abilityType a)
    {
        return abilities[a].muted;
    }
    


    //
    // sets mute status of an ability
    //
    public void setMuteStatus(abilityType a, bool status)
    {
        abilities[a].muted = status;
    }



    //
    // sets mute status of all abilities
    //
    public void setMuteStatusAll(bool status)
    {
        foreach(abilityType a in abilityNumToName.Values)
        {
            setMuteStatus(a, status);
        }
    }
}
