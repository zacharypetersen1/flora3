﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_ConfusionAbility : MonoBehaviour
{

    ENT_ConfusionMinion body;
    string affectsTag = "Minion";
    public float confusionAbilityRadius = 30;

    float killTimer = 0;
    //float PULSE_FREQUENCY = 10;
    //bool resetTimer = false;

    bool purity = false;
    public bool Purity
    {
        get { return purity; }
        set { purity = value; }
    }

    // Use this for initialization
    void Start()
    {
        body = GetComponentInParent<ENT_ConfusionMinion>();
        killTimer = 3F;
        //transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        killTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (killTimer <= 0)
        {
            Destroy(gameObject);
        }
    }

    //
    // Runs on collision
    //
    void OnTriggerStay(Collider collider)
    {
        if (/*body != null && body.isAbilityActive*/ true)
        {
            if (collider.tag == affectsTag)
            {
                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                if (purity != other.Purity)
                {
                    other.receive_combat_message(new CBI_CombatMessenger(0, 0, 0, new List<CBI_CombatMessenger.status>() { CBI_CombatMessenger.status.confusion }, transform.position));
                }

            }
        }
    }

}
