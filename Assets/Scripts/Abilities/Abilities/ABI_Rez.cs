﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Rez : MonoBehaviour {

    bool firstFrame = true;
    static float rezRadius = 12.5f;

    private void Awake()
    {
        GetComponent<SphereCollider>().radius = rezRadius;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "remnant")
        {
            ENT_Remnant sprout = other.gameObject.GetComponent<ENT_Remnant>();
            if (sprout != null)
            {
                sprout.Rez();
            }
        }
    }

    private void Update()
    {
        if (firstFrame)
            firstFrame = false;
        else Destroy(gameObject);
    }

}
