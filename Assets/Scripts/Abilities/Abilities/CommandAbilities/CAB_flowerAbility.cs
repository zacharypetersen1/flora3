﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAB_flowerAbility : MonoBehaviour {

    public bool fromAlly = true;
    ParticleSystem[] ps;

    // Use this for initialization
    void Start()
    {
        ps = GetComponentsInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var p in ps)
        {
            if (p.IsAlive()) return;
        }
        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Minion")
        {
            ENT_DungeonEntity body = other.gameObject.GetComponent<ENT_DungeonEntity>();
            if (body == null) return;
            if (fromAlly == body.isAlly)
            {
                body.receive_combat_message(new CBI_CombatMessenger(0, 5, 0, new List<CBI_CombatMessenger.status>(), other.transform.position));
            }
        }
    }
}
