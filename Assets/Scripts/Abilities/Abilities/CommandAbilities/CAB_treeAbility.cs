﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAB_treeAbility : MonoBehaviour {

    public bool growing = false;
    public int maxTimer = 60;
    public int growthTimer = 0;
    public int shrinkTimer = 0;
    public bool shrinking = false;
    public Vector3 startScale;
    public Vector3 endScale;
    public float growthScalar = 1.01f;

	// Use this for initialization
	void Start () {
        startScale = transform.parent.transform.localScale;
        endScale = startScale * 2;
        growthTimer = shrinkTimer = maxTimer;
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if (growing && growthTimer >= 0)
        {
            transform.parent.transform.localScale *= (growthScalar);
            --growthTimer;
        }
        else
        {
            growing = false;
            growthTimer = maxTimer;
        }

        if (shrinking && shrinkTimer >= 0)
        {
            transform.parent.transform.localScale /= (growthScalar);
            --shrinkTimer;
        }
        else
        {
            shrinking = false;
            shrinkTimer = maxTimer;
        }
        

    }

    public void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            growing = true;
        }
    }


    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            shrinking = true;
        }
    }
}
