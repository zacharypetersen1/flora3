﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ABI_Ability {



    protected PLY_PlayerDriver driver;
    protected CHA_Motor motor;
    protected ABI_Manager manager;
    protected GameObject player;
    public float curCharge = 0;
    public bool muted = false;
    public bool active = false;
    public float heldThreshold = 0.4f;
    public bool wasHeld = false;

    public static Vector3 direction;

    protected static GameObject cursor;



    //
    // Initializes the ability
    //
    public void storeReferences(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        driver = setDriver;
        motor = driver.GetComponent<CHA_Motor>();
        manager = setManager;
        player = twig;
    }



    //
    // Sets up the ability
    //
    public virtual void start()
    {
        
    }



    //
    // Runs once when ability is first activated
    //
    public abstract void buttonDown();



    //
    // Runs once when ability is first released
    //
    public virtual void buttonUp() { }





    //
    // Runs once per frame while ability is being held down
    //
    public virtual void buttonHeldDown() { }



    //
    // Get amount of recharge time for this ability
    //
    public virtual float getRechargeTime()
    {
        return 5;
    }



    //
    // getter for held threshold
    //
    public virtual float getHeldThreshold()
    {
        return 0.25f;
    }


    //
    //Get direction of Caster (direction comes from camera dolly)
    //
    //WAS THERE SUPPOSED TO BE A FUNCTION HERE????????//////

    //
    //gets player forward facing vector
    //
    public virtual Vector3 getDirection()
    {
        Vector3 dir = UTL_Math.angleRadToUnitVec(CAM_Camera.getDollyPhysicalRotZ() * Mathf.Deg2Rad);
        return dir;
    }


    public GameObject getGoToCursor()
    {
        return cursor;
    }



    //
    // Runs once per frame
    //
    public virtual void update()
    {
        direction = getDirection();
    }
}
