﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Moving : ABI_Ability {


    float surfRechargeTime = 0.4f;
    public bool isSurfing = false;
    bool waitingToCancelSurf = false;
    float minVelocity = 0.12f;
    
    // FMOD parameters
	[FMODUnity.EventRef]
    float corruptVal = 0;
    float corruptValInterp = 0.01f;
    float boostVal = 0;
    float boostValInterp = 0.01f;

    public static ABI_Moving singleton;

    GameObject bumpObj;
    GameObject surfLog;
    GameObject surfTailVines;
    Animator surfLogAnim;
    Animator playerAnim;
    ParticleSystem.EmissionModule vineTrailEmitter;
    
    //
    // constructor
    //
    public ABI_Moving(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
        TME_Timer.addTimer("surfTimer", TME_Time.type.player);
        surfLog = GameObject.Find("SurfLogObject");
        surfLogAnim = surfLog.GetComponent<Animator>();
        playerAnim = GameObject.Find("TwigMesh").GetComponent<Animator>();
        surfTailVines = GameObject.Find("SurfTailVines");
        ParticleSystem vineTrailSystem = GameObject.Find("VineCircleTrail").GetComponent<ParticleSystem>();
        vineTrailEmitter = vineTrailSystem.emission;
        singleton = this;
    }



    //
    // Init
    //
    public override void start()
    {
        bumpObj = GameObject.Find("PlayerBumper");
        //Debug.Log("bumpObj" + bumpObj);
        bumpObj.SetActive(false);
    }



    //
    // Update
    //
    public override void update()
    {
        if (bumpObj == null) bumpObj = GameObject.Find("PlayerBumper");

        // check if surf needs to be canceled after being in air
        if(waitingToCancelSurf && !motor.isInAir())
        {
            if (!INP_PlayerInput.getButton("Ability0"))
            {
                stopSurfing();
            }
            waitingToCancelSurf = false;
        }

        // Fade music track for surfing
        boostVal += isSurfing ? boostValInterp : -boostValInterp;
        boostVal = Mathf.Clamp(boostVal, 0, 1);
        if (MUS_System.singleton != null) MUS_System.singleton.MusicBoost(boostVal);

        // Fade music track for corruption
        corruptVal += MAP_NatureMap.natureTypeAtPos(driver.transform.position) == MAP_NatureData.type.corrupt ? corruptValInterp : -corruptValInterp*2;
        corruptVal = Mathf.Clamp(corruptVal, 0, 1);
        if (MUS_System.singleton != null)
        {
            MUS_System.singleton.MusicCorrupt(corruptVal);
            MUS_System.singleton.ambientCorrupt(corruptVal);
            MUS_System.singleton.surfSoundCorrupt(corruptVal);
        }

        surfLogAnim.SetFloat("yVelocity", playerAnim.GetFloat("yVelocity"));
    }



    //
    // activate TreeRoots
    //
    public override void buttonDown()
    {

    }



    //
    // Runs once when button is released
    //
    public override void buttonUp()
    {
        // Check if surfing needs to be canceled
        if (isSurfing)
        {
            if (motor.isInAir())
            {
                waitingToCancelSurf = true;
            }
            else
            {
                stopSurfing();
            }
        }
    }



    //
    // Runs once per frame after button is detected as down
    //
    public override void buttonHeldDown()
    {
        //Debug.Log("buttonHeldDown");
        if (!isSurfing && ableToStartSurf())
        {
            //Debug.Log("shouldStartSurfing");
			startSurfingMount();
			MUS_System.singleton.surfSoundStop ();
			MUS_System.singleton.surfSoundStart ();
			MUS_System.singleton.surfSoundTrue(1);
			MUS_System.singleton.surfSoundSpeed(0);
        }

        if (isSurfing && !ableToMaintainSurf())
        {
			//stopSurfing();
        }
    }



    //
    // Checks to see if the player can surf
    //
    public bool ableToStartSurf()
    {
        return motor.onGround() == CHA_Motor.groundType.terrainGround &&
            onSurfableGround() && !ABI_Purify.busyRezing;
    }



    //
    //Checks if player can maintain surfing
    //
    public bool ableToMaintainSurf()
    {
        CHA_Motor.groundType groundType = motor.onGround();
        return groundType != CHA_Motor.groundType.meshGround && (groundType == CHA_Motor.groundType.air || onSurfableGround());
    }



    //
    // Checks to see if the player is currently on surfable ground
    //
    public bool onSurfableGround()
    {
        return MAP_NatureMap.natureTypeAtPos(driver.transform.position) != MAP_NatureData.type.rock;
    }



    //
    // Checks to see if the player is currectly moving fast enough to maintain surfing
    //
    public bool atSurfableVelocity()
    {
        return motor.getVelocityMagnitude() > minVelocity;
    }



    //
    // Start surfing mount process
    //
    public void startSurfingMount()
    {
        // Set log position and animation
        surfLog.transform.position = motor.transform.position;
        surfLog.transform.rotation = motor.transform.rotation;
        surfLog.transform.Rotate(Vector3.up * 90);
        surfLog.transform.parent = motor.transform;
        surfLogAnim.SetBool("isSurfIdle", true);

        playerAnim.SetBool("isSurfing", true);
        //Debug.Log("Got to end of startSurfing");
        isSurfing = true;
    }



    //
    // Runs when mount is complete
    //
    public void onSurfMounted()
    {
        // Only run if surf has not been canceled yet
        if(isSurfing == true)
        {
            // Set surf tail vines position
            surfTailVines.transform.position = motor.transform.position;
            surfTailVines.transform.rotation = motor.transform.rotation;
            surfTailVines.transform.Rotate(Vector3.up * 90);
            surfTailVines.transform.parent = motor.transform;

            // Start up vine trail
            vineTrailEmitter.enabled = true;

            // Set surfing values
            driver.setState(PLY_MoveState.type.surf);
            motor.setSpeed(driver.surfingSpeed);
            motor.rotationVelocity = driver.surfingRotVelMax;
            motor.clampVelocityToDirection = true;

            // Set camera FOV
            CAM_Camera.setSurfFov(true);
        }
    }



    //
    // Runs when jump is triggered while surfing
    //
    public void onJumpWhileSurfing()
    {
        // Set surf tail vines position
        surfTailVines.transform.position = Vector3.down * 500;
        surfTailVines.transform.parent = null;

        // Stop vine trail
        vineTrailEmitter.enabled = false;

        // Trigger log animation
        surfLogAnim.SetBool("inAir", true);
        surfLogAnim.SetTrigger("doJump");
        
    }



    //
    // RUns when landing while surfing
    //
    public void onLandWhileSurfing()
    {
        // Set surf tail vines position
        surfTailVines.transform.position = motor.transform.position;
        surfTailVines.transform.rotation = motor.transform.rotation;
        surfTailVines.transform.Rotate(Vector3.up * 90);
        surfTailVines.transform.parent = motor.transform;

        // Start up vine trail
        vineTrailEmitter.enabled = true;

        // Trigger log animation
        surfLogAnim.SetBool("inAir", false);
    }



    //
    // Stop surfing
    //
    public void stopSurfing()
    {
        // Set log position and animation
        surfLog.transform.position = Vector3.down * 500;
        surfLog.transform.parent = null;
        surfLogAnim.SetBool("isSurfIdle", false);

        // Set surf tail vines position
        surfTailVines.transform.position = Vector3.down * 500;
        surfTailVines.transform.parent = null;

        // Shut down vine trail
        vineTrailEmitter.enabled = false;

        driver.setState(PLY_MoveState.type.normal);
        if (bumpObj != null) bumpObj.SetActive(false);
        TME_Timer.setTime("surfTimer", surfRechargeTime);
        playerAnim.SetBool("isSurfing", false);
        isSurfing = false;
        motor.setSpeed(driver.runningSpeed);
        motor.rotationVelocity = driver.runningRotVelocity;
        motor.clampVelocityToDirection = false;

        MUS_System.singleton.surfSoundTrue(0);

        CAM_Camera.setSurfFov(false);
    }



    //
    // overwrite the held threshold to a low value
    //
    public override float getHeldThreshold()
    {
        return 0.15f;
    }



    //private void disableCorruptParticles()
    //{
    //    ParticleSystem.EmissionModule em = GameObject.Find("SurfParticle_Particle").GetComponent<ParticleSystem>().emission;
    //}
}
