﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


//
// Handles purification "weapon"
//
public class ABI_Purify : ABI_Ability {

    public static bool canCancelRez = false;

    // The different types of purification "attacks"
    public enum purifyTypes {
        launchShot, quickShot, multiShot
    }

    public purifyTypes currentType = purifyTypes.quickShot;
    public static bool busyRezing = false;
    public static bool rezUnlocked = false;

    PLY_AllyManager am;

    // Stores instances of all purify types
    Dictionary<purifyTypes, ABI_PurifyType> typeList = new Dictionary<purifyTypes, ABI_PurifyType>{
        {purifyTypes.launchShot, new ABI_LaunchShot() },
        {purifyTypes.quickShot, new ABI_QuickShot() },
        {purifyTypes.multiShot, new ABI_MultiShot() }
    };


    //
    // constructor
    //
    public ABI_Purify(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    public override void start()
    {
        foreach(var ability in typeList.Values)
        {
            ability.start();
        }
        UIX_ShotIndicator.setShotIndicator(currentType);
        if (!am)
        {
            am = GameObject.FindWithTag("Player").GetComponent<PLY_AllyManager>();
        }
    }



    // DEBUG Listen for player to change selected purify type
    public override void update()
    {
        if (!SelectionManager.isInSelectMode && !TUT_TutorialManager.isMessagePlaying)
        {
            // This code updated purify shot type based on scroll wheel
            /*float scrollDelta = Input.GetAxis("Mouse ScrollWheel")*10;
            //Debug.Log(scrollDelta);
            if(scrollDelta != 0)
            {
                currentType = (purifyTypes) (((int)currentType+(int)scrollDelta+3)%3);
                //Debug.Log(currentType);
                // Trigger UI change here
                UIX_ShotIndicator.setShotIndicator(currentType);
            }*/

            typeList[currentType].update();
        }
    }

    bool RezAllowed()
    {
        bool result = motor.onGround() != CHA_Motor.groundType.air
            && !driver.surfing
            && rezUnlocked
            && !SelectionManager.isInSelectMode
            && PLY_SproutDetector.sproutInRange
            && am.allyCount + am.wateredCount < am.MaxFollowers;

        return result;

    }
    //&& !TUT_TutorialManager.isMessagePlaying

    //
    // Runs once when ability is first activated;
    //
    public override void buttonDown()
    {
        if (RezAllowed())
        {
            PLY_Anim.animator.SetBool("doRez", true);
            GameObject.Find("RezStampCharge").GetComponent<RezStampCharge>().activate();
            //busyRezing = true;
        }
        /*else
        {
            Debug.Log("Rez not allowed, " + PLY_SproutDetector.sproutInRange);
        }*/
        /*if(!SelectionManager.isInSelectMode && !TUT_TutorialManager.isMessagePlaying)
        {
            typeList[currentType].groundShotDown();
        }*/
    }



    //
    // Runs once when ability is first released
    //
    public override void buttonUp()
    {
        
        PLY_Anim.animator.SetBool("doRez", false);
        if (canCancelRez)
        {
            driver.configureRunning();
            canCancelRez = false;
        }
    }



    //
    // Runs once per frame while ability is being held down
    //
    public override void buttonHeldDown()
    {
       /* if (!SelectionManager.isInSelectMode && !TUT_TutorialManager.isMessagePlaying)
        {
            typeList[currentType].shotHeld();
        }*/
    }

}
