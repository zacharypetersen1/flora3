﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
// Represents a single type of "Weapon" for purifying
//
public abstract class ABI_PurifyType {

    // Runs on startup
    virtual public void start() { }

    // Runs when mouse is started being held down 
    virtual public void groundShotDown() { }

    // Runs when mouse is pressed down while in air
    virtual public void airShotDown() { }

    // Runs when mouse is lifted
    virtual public void shotUp() { }

    // Runs when mouse is pressed down while surfing
    virtual public void surfShotDown() { }

    // Runs while mouse is held down
    virtual public void shotHeld() { }

    // Runs every frame
    virtual public void update() { }
}
