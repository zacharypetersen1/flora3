﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_ShotTypeStandard : ABI_PurifyType
{

    protected float clipSize = 4;
    float shotCount;
    protected float deltaShotTime = 0.5f;
    float time;
    bool reloading = false;
    protected float reloadTime = 1.5f;
    float curReloadTime = 0f;
    bool automatic = false;
    PLY_Ammo.ammoTypes ammoType;
    protected GameObject player;
    int ammoPerShot = 1;
    public float autoReloadRate = 0.1f;

    public override void start()
    {
        shotCount = clipSize;
        UIX_AmmoRadialBar.setBarValue(shotCount / clipSize);
        player = GameObject.Find("Player");
    }

    public override void groundShotDown()
    {
        time = deltaShotTime;
        if (reloading)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/twig_noammo", player.transform.position);
        }
    }

    public override void shotUp()
    {

    }

    public override void surfShotDown()
    {
        time = deltaShotTime;
        if (reloading)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/twig_noammo", player.transform.position);
        }
    }



    //
    // Runs while shot button is held down
    //
    public override void shotHeld()
    {
        if (!reloading)
        {
            // Only incriment time if this is automatic purfify type
            if (automatic)
            {
                time += Time.deltaTime;
            }

            // Shoot purification "rounds"
            while (time >= deltaShotTime)
            {
                // shoot purification here
                if (PLY_Ammo.hasAmmo(ammoType))
                {
                    doShot();
                    PLY_Ammo.addAmmo(ammoType, -ammoPerShot);

                    // check if time to reload
                    shotCount -= 1;
                    UIX_AmmoRadialBar.setBarValue(shotCount / clipSize);
                    if (shotCount == 0)
                    {
                        MUS_System.singleton.reloadSoundStart();
                        reloading = true;
                        break;
                    }
                }
                else
                {
                    FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/twig_noammo", player.transform.position);
                }

                time -= deltaShotTime;
            }
        }
    }



    //
    // Runs once per frame
    //
    public override void update()
    {
        shotCount += autoReloadRate;
        UIX_AmmoRadialBar.setBarValue(shotCount / clipSize);

        if (!reloading && shotCount != clipSize && INP_PlayerInput.getButtonDown("Reload"))
        {
            //MUS_System.singleton.reloadSoundStart();
            reloading = true;
        }

        if (reloading)
        {
            curReloadTime += TME_Manager.getDeltaTime(TME_Time.type.player);
            if (curReloadTime >= reloadTime)
            {
                //MUS_System.singleton.reloadSoundStop();
                //FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/twig_reloaddone");
                shotCount = clipSize;
                curReloadTime = 0;
                reloading = false;
                UIX_AmmoRadialBar.setBarValue(1);
            }
            else
            {
                UIX_AmmoRadialBar.setBarValue(curReloadTime / reloadTime);
            }
        }
    }



    //
    // Initializes shot
    //
    protected void initShot(float setClipSize, float setDeltaShotTime, float setReloadTime, bool isAutomatic, PLY_Ammo.ammoTypes setAmmoType, int setAmmoPerShot)
    {
        clipSize = setClipSize;
        shotCount = clipSize;
        UIX_AmmoRadialBar.setBarValue(shotCount / clipSize);
        deltaShotTime = setDeltaShotTime;
        reloadTime = setReloadTime;
        automatic = isAutomatic;
        ammoType = setAmmoType;
        ammoPerShot = setAmmoPerShot;
        player = GameObject.Find("Player");
    }



    //
    // Executes shot, will be overwritten by each shot type
    //
    protected virtual void doShot() { }
}
