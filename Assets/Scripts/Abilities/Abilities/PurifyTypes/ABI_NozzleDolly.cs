﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Used for positioning the purify nozzle
public class ABI_NozzleDolly : MonoBehaviour {

    

    public Transform cameraDolly;
    public Transform nozzleTransform;
    private CAM_Dolly camDolly;


    // Get the camera dolly component
    void Start()
    {
        camDolly = cameraDolly.GetComponent<CAM_Dolly>();
    }


	// Set rotation of nozzle dolly
	void Update () {
        transform.position = cameraDolly.transform.position;
        transform.rotation = new Quaternion(0, 0, 0, 0);
        transform.Rotate(new Vector3(0, -camDolly.horizontalRotation - 90, 0));
    }
}
