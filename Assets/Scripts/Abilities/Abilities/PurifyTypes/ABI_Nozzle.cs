﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Nozzle : MonoBehaviour {

    public ParticleSystem hose;
    public ParticleSystem quickShot;
    public GameObject multiShot;

    public static ABI_Nozzle nozzle;
    private LayerMask mask;

    //
    // Set up singleton
    //
    void Start()
    {
        nozzle = this;

        // set up layer mask
        mask = (1 << 11) | (1 << 12) | (1 << 13) | (1 << 15);
        
    }



    //
    // Look at target
    //
    void Update()
    {
        // Cast ray and set aim
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2, Camera.main.nearClipPlane));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, mask))
        {
            transform.LookAt(hit.point);
        }
        else
        {
            transform.LookAt(Camera.main.transform.position + ray.direction * 100);
        }
    }



    //
    // Sets emission state for specified particle system
    //
    public static void setParticleState(ParticleSystem system, bool enabled)
    {
        ParticleSystem.EmissionModule em = system.emission;
        em.enabled = enabled;
    }



    //
    // Sets emission state for all particle systems that are childed to specified GameObject
    //
    public static void setParticleStateChildren(GameObject parent, bool enabled)
    {
        foreach (var system in parent.GetComponentsInChildren<ParticleSystem>())
        {
            setParticleState(system, enabled);
        }
    }



    //
    // Shoots an amount of particles using specified system
    //
    public static void shootParticle(ParticleSystem system, int count)
    {
        system.Emit(count);
    }



    //
    // Shoots an amount of particles from all childed
    //
    public static void shootParticleChildren(GameObject parent, int count)
    {
        foreach (var system in parent.GetComponentsInChildren<ParticleSystem>())
        {
            if(system.transform.parent == parent.transform)
            {
                system.Emit(count);
            }
        }
    }
}
