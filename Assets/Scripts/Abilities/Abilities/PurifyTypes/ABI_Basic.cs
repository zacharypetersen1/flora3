﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Basic : ABI_PurifyType {

    public override void groundShotDown()
    {
        ABI_Nozzle.setParticleState(ABI_Nozzle.nozzle.hose, true);
    }

    public override void shotUp()
    {
        ABI_Nozzle.setParticleState(ABI_Nozzle.nozzle.hose, false);
    }

    public override void surfShotDown()
    {
        
    }
}
