﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_LaunchShot : ABI_ShotTypeStandard {

    public override void start()
    {
        initShot(6, 1.2f, 2f, true, PLY_Ammo.ammoTypes.red, 3);
    }

    protected override void doShot()
    {
        ABI_Nozzle.shootParticle(ABI_Nozzle.nozzle.hose, 6);
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/twig_purify_lobbed", player.transform.position);
    }
}
