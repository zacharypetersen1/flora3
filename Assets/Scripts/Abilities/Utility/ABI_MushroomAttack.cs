﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_MushroomAttack : MonoBehaviour {

    public ENT_MushroomBody body;
    public string[] affectsTags;

    public Collider smallCollider;
    public Collider bigCollider;
    public Collider endCollider;
    public float damagePerSecond = 1.5F;
    public bool purity;
    float timer = 0;
    ArrayList hitThisFrame = new ArrayList(5);
    bool furry = false;
    public bool Furry
    {
        set { furry = value; }
        get { return furry; }
    }
    bool hit = false;
    // Use this for initialization
    void Start () {
        body.mushroomAttacking = true;
        bigCollider.enabled = false;
        endCollider.enabled = false;
        purity = body.isAlly;

        if (furry)
        {
            damagePerSecond *= 2;
        }
	}
	
	// Update is called once per frame
	void Update () {
        manageColliders();
        hitThisFrame.Clear();

        if(timer >= 5.0f)
        {
            body.mushroomAttacking = false;
            Destroy(gameObject);
        }
	}

    void manageColliders()
    {
        timer += TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (timer > 1f)
        {
            if (!bigCollider.enabled) bigCollider.enabled = true;
        }
        if (timer > 2f)
        {
            if (!endCollider.enabled) endCollider.enabled = true;
        }
        if (timer > 4.0F)
        {
            if (bigCollider.enabled) bigCollider.enabled = false;
            if (smallCollider.enabled) smallCollider.enabled = false;
        }
        if (timer > 4.5F)
        {
            if (endCollider.enabled) endCollider.enabled = false;
        }
    }

    private IEnumerator WaitCoroutine(float waitTime, ENT_DungeonEntity other_alarm, Vector3 pos)
    {
        yield return new WaitForSeconds(waitTime);

        if (!hit)
        {
            //Debug.Log("Projectile triggered alarm");
            other_alarm.receive_combat_message(new CBI_CombatMessenger(0, 0, 0, null, pos));
            hit = false;
        }
    }

    void OnTriggerStay(Collider col)
    {
        //print("hit " + col.name);
        if (body != null)
        {
            if (col.gameObject == body.gameObject)
            {
                //skip the mushroom that spawned the attack
                return;
            }
        }
        int numAffectsTags = affectsTags.Length;
        for (int i = 0; i < numAffectsTags; ++i)
        {
            string tag = affectsTags[i];
            if (col.CompareTag(tag))
            {
                if (col.tag == "alarmSphere")
                {
                    ENT_DungeonEntity other_alarm = col.gameObject.GetComponentInParent<ENT_DungeonEntity>();
                    if (purity != other_alarm.Purity)
                    {
                        //wait .5 seconds before sending message to minion
                        StartCoroutine(WaitCoroutine(.5F, other_alarm, transform.position));
                    }
                    continue;
                }

                ENT_DungeonEntity entity = col.gameObject.GetComponent<ENT_DungeonEntity>();
                if (entity == null)
                {
                    entity = col.gameObject.GetComponentInParent<ENT_DungeonEntity>();
                }
                if (entity != null)
                {

                    bool otherPurity = entity.isAlly;
                    if (purity != otherPurity)
                    {
                        hit = true;
                        if (!hitThisFrame.Contains(entity))
                        {
                            float damage = damagePerSecond * TME_Manager.getDeltaTime(TME_Time.type.enemies);
                            entity.receive_combat_message(new CBI_CombatMessenger(damage, 0, 0, null, transform.position));

                            hitThisFrame.Add(entity);
                        }
                        else // do half extra damage
                        {
                            ENT_Body Tbody = col.GetComponent<ENT_Body>();
                            if (Tbody != null && Tbody.entity_type == ENT_DungeonEntity.monsterTypes.treeMinion)
                            {
                                //trees take double damage if overlapping colliders
                                float damage = damagePerSecond * TME_Manager.getDeltaTime(TME_Time.type.enemies);
                                entity.receive_combat_message(new CBI_CombatMessenger(damage, 0, 0, null, transform.position));
                            }
                        }

                    }
                }
            }
        }
    }
}
