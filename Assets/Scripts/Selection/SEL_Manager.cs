﻿// Andrei & Sterling
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class SEL_Manager : MonoBehaviour {

    public SelectionManager selectionManager;
    public GameObject crosshair;
    public PLY_AllyManager allyManager;
    public static SEL_Manager singleton;

    private GameObject unitContainer;

    UIX_Pause pauseScript;

    //
    // Use this for initialization
    //
    void Start () {
        selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>();
        Cursor.visible = false;
        crosshair = GameObject.Find("Crosshair2");
        singleton = this;
        if (!allyManager)
        {
            allyManager = GameObject.FindGameObjectWithTag("Player").GetComponent<PLY_AllyManager>();
        }
        pauseScript = GameObject.FindGameObjectWithTag("Scripts").GetComponent<UIX_Pause>();
        unitContainer = GameObject.Find("UnitContainer");
    }



    //
    // Update is called once per frame
    //
    void Update() {

        if (pauseScript.isPaused())
            return;

        //selectedList = selectionManager.GetSelectedObjectsAsList();

        if (!SelectionManager.isInSelectMode && INP_PlayerInput.getButtonDown("SelectMode"))
        {
            //selectionManager.selectAll();
            activateSelectMode();
            CAM_Dolly.snapToDefaultScrollValue(true);
        }
        else if(SelectionManager.isInSelectMode && INP_PlayerInput.getButtonDown("SelectMode"))
        {
            //selectionManager.ClearSelection();
            deactivateSelectMode();
            CAM_Dolly.snapToDefaultScrollValue(false);
        }

        //commands shared across both modes
        if (INP_PlayerInput.getButtonDown("SelectAll"))
        {
            selectionManager.toggleSelectAll();
        }
        else if (INP_PlayerInput.getButtonDown("ActivateAlly1"))
        {
            selectMinionsOfType(ENT_DungeonEntity.monsterTypes.treeMinion);
        }
        else if (INP_PlayerInput.getButtonDown("ActivateAlly2"))
        {
            selectMinionsOfType(ENT_DungeonEntity.monsterTypes.flowerMinion);
        }
        else if (INP_PlayerInput.getButtonDown("ActivateAlly3"))
        {
            selectMinionsOfType(ENT_DungeonEntity.monsterTypes.mushroomMinion);
        }
        else if (INP_PlayerInput.getButtonDown("ActivateAlly4"))
        {
            selectMinionsOfType(ENT_DungeonEntity.monsterTypes.slowMinion);
        }
        else if (INP_PlayerInput.getButtonDown("ActivateAlly5"))
        {
            selectMinionsOfType(ENT_DungeonEntity.monsterTypes.confusionMinion);
        }
        else if (INP_PlayerInput.getButtonDown("ActivateAlly6"))
        {
            selectMinionsOfType(ENT_DungeonEntity.monsterTypes.necroMinion);
        }
        else if (INP_PlayerInput.getButtonDown("NextUnit"))
        {
            selectNextUnit();
        }
        else if (INP_PlayerInput.getButtonDown("NextGroup"))
        {
            selectNextGroup();
        }

    }

    private void selectNextUnitFromGroupSelection()
    {
        if (selectionManager.GetSelectedCount() == 0)
        {
            //if we have no one selected just select the first ally
            if (allyManager.allyCount > 0)
            {
                selectUnitFromGroup(allyManager.getAllAllies[0]);
            }
            return;
        }
        GameObject currSelection = selectionManager.GetSelectedObjectFromIndex(0);
        int index = selectionManager.GetGroupSelectedIndexFromObject(currSelection);
#if UNITY_EDITOR
        if (index < 0)
            Debug.LogError("SEL_Manager.selectNextUnitFromGroupSelection() Failed!");
#endif
        int groupCount = selectionManager.GetGroupSelectedCount();
        index = (index + 1) % groupCount;
        GameObject newSelection = selectionManager.GetGroupSelectedObjectFromIndex(index);
        selectUnitFromGroup(newSelection);
    }

    private void addAllAlliesToGroupSelection()
    {
        int groupSelectedCount = selectionManager.GetGroupSelectedCount();
        GameObject currSelection = groupSelectedCount == 0 ? null : selectionManager.GetSelectedObjectFromIndex(0); //can be null
        List<GameObject> allies = allyManager.getAllAllies;
        int allyCount = allyManager.allyCount;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allies[i];
            if (ally == currSelection)
                continue;
            selectionManager.AddGameObjectToGroupSelection(ally);
        }
    }

    private void selectNextGroup(ENT_Body.monsterTypes currentSelection)
    {
        ENT_Body.monsterTypes desiredSelection = ENT_DungeonEntity.monsterTypes.treeMinion;
        switch(currentSelection)
        {
            case ENT_DungeonEntity.monsterTypes.treeMinion:
                desiredSelection = ENT_DungeonEntity.monsterTypes.flowerMinion;
                break;
            case ENT_DungeonEntity.monsterTypes.flowerMinion:
                desiredSelection = ENT_DungeonEntity.monsterTypes.mushroomMinion;
                break;
            case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                desiredSelection = ENT_DungeonEntity.monsterTypes.slowMinion;
                break;
            case ENT_DungeonEntity.monsterTypes.slowMinion:
                desiredSelection = ENT_DungeonEntity.monsterTypes.confusionMinion;
                break;
            case ENT_DungeonEntity.monsterTypes.confusionMinion:
                desiredSelection = ENT_DungeonEntity.monsterTypes.necroMinion;
                break;
            case ENT_DungeonEntity.monsterTypes.necroMinion:
                desiredSelection = ENT_DungeonEntity.monsterTypes.treeMinion;
                break;

        }
        //desired selection is flowers
        int minionCount = 0;
        switch (desiredSelection)
        {
            case ENT_DungeonEntity.monsterTypes.treeMinion:
                minionCount = allyManager.currTrees;
                break;
            case ENT_DungeonEntity.monsterTypes.flowerMinion:
                minionCount = allyManager.currFlowers;
                break;
            case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                minionCount = allyManager.currMushrooms;
                break;
            case ENT_DungeonEntity.monsterTypes.slowMinion:
                minionCount = allyManager.currSlow;
                break;
            case ENT_DungeonEntity.monsterTypes.confusionMinion:
                minionCount = allyManager.currConfusions;
                break;
            case ENT_DungeonEntity.monsterTypes.necroMinion:
                minionCount = allyManager.currNecro;
                break;
        }

        if (minionCount <= 0)
        {
            //try next group, since we made sure we have at least one ally before calling this function it'll eventually work
            selectNextGroup(desiredSelection);
            return;
        }
        else
        {
            selectMinionsOfType(desiredSelection);
        }
    }

    public void selectNextGroup()
    {
        int allyCount = allyManager.allyCount;
        if (allyCount <= 0)
            return;

        GameObject topSelectedMinion = selectionManager.GetTopSelectedObject();
        if (topSelectedMinion)
        {
            ENT_Body.monsterTypes minionType = topSelectedMinion.GetComponent<ENT_Body>().entity_type;
            selectNextGroup(minionType);
        }
        else
        {
            //hack to start over at tree minions
            selectNextGroup(ENT_DungeonEntity.monsterTypes.turretMinion);
        }
    }

    public void selectNextUnit()
    {
        int selectedCount = selectionManager.GetSelectedCount();
        int groupSelectedCount = selectionManager.GetGroupSelectedCount();

        if (selectedCount <= 0)
        {
            addAllAlliesToGroupSelection();
            selectNextUnitFromGroupSelection();
            return;
        }
           

        //if we have more than one minion selected, choose one to select
        if (selectedCount > 1)
        {
            //right now just chooses arbitrary first minion
            GameObject newSelection = null;
            for (int i = 0; i < groupSelectedCount && !newSelection; ++i)
            {
                newSelection = selectionManager.GetGroupSelectedObjectFromIndex(i);
            }
            selectUnitFromGroup(newSelection);
        }

        //if we have just one minion go to the next selection
        else
        {
            if (groupSelectedCount > 1)
            {
                selectNextUnitFromGroupSelection();
            }
            else
            {
                //if we only have one group selected minion start cycling through our other minions, i.e. our group selection becomes all of our minions
                addAllAlliesToGroupSelection();
                selectNextUnitFromGroupSelection();
            }

        }
    }

    private void selectUnitFromGroup(GameObject minion)
    {
#if UNITY_EDITOR
            if (!minion)
                Debug.LogError("SEL_Manager.selectNextUnit() Failed!");
#endif
        selectionManager.ClearSelection(false);
        selectionManager.AddGameObjectToSelection(minion);
    }

    public void selectMinionsOfType(ENT_Body.monsterTypes type)
    {
        selectionManager.ClearSelection();
        List<GameObject> newSelection = allyManager.getAlliesOfType(type);
        foreach (GameObject minion in newSelection)
        {
            selectionManager.AddGameObjectToSelection(minion);
        }
    }

    //
    // Activates select mode
    //
    public static void activateSelectMode()
    {
        //singleton.selectionManager.selectAll();
        singleton.crosshair.SetActive(false);
        SelectionManager.isInSelectMode = true;
        INP_MouseCursor.position = new Vector2(Screen.width / 2, Screen.height / 2);
        //TME_Manager.setTimeScalar(TME_Time.type.enemies, 0.5f);
        //TME_Manager.setTimeScalar(TME_Time.type.environment, 0.5f);
    }



    //
    // Deactivates select mode
    //
    public static void deactivateSelectMode()
    {
        singleton.crosshair.SetActive(true);
        SelectionManager.isInSelectMode = false;
        //TME_Manager.setTimeScalar(TME_Time.type.enemies, 1.0f);
        //TME_Manager.setTimeScalar(TME_Time.type.environment, 1.0f);
    }

    public static void removeFromSelection(GameObject go)
    {
        singleton.selectionManager.RemoveGameObjectFromSelection(go);
    }

    public static int getSelectedCount()
    {
        return singleton.selectionManager.GetSelectedCount();
    }

    public static void SelectNextMinion()
    {
        singleton.selectNextUnit();
    }

    //the minion whos ability we would activate when pressing the minion ability button
    public static GameObject GetTopSelectedMinion()
    {
        GameObject result = singleton.selectionManager.GetTopSelectedObject();

        return result;
    }

    public static bool IsSelected(GameObject minion)
    {
        return singleton.selectionManager.isSelected(minion);
    }

   public static List<GameObject> GetSelected()
    {
        return singleton.selectionManager.GetSelectedObjectsAsList();
    }

    public static void AddToSelection(GameObject minion)
    {
        singleton.selectionManager.AddGameObjectToSelection(minion);
    }

    //
    // Returns pair of dictionaries, one containing amount of selected minions stored by type, and one containing the total amount of minions stored by type
    //
    public Pair<Dictionary<ENT_DungeonEntity.monsterTypes, int>, Dictionary<ENT_DungeonEntity.monsterTypes, int>> getSelectedByType()
    {
        // Count total minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (Transform child in unitContainer.transform)
        {
            if (child != null)
            {
                var type = child.GetComponent<ENT_Body>().entity_type;
                if (totalCount.ContainsKey(type))
                {
                    totalCount[type] += 1;
                }
                else totalCount.Add(type, 1);
            }
        }

        Dictionary<ENT_DungeonEntity.monsterTypes, int> selectedCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        List<GameObject> selectedList = GetSelected();
        foreach (GameObject obj in selectedList)
        {
            if (obj != null)
            {
                var type = obj.GetComponent<ENT_Body>().entity_type;
                if (selectedCount.ContainsKey(type))
                {
                    selectedCount[type] += 1;
                }
                else selectedCount.Add(type, 1);
            }
        }
        return new Pair<Dictionary<ENT_DungeonEntity.monsterTypes, int>, Dictionary<ENT_DungeonEntity.monsterTypes, int>>(selectedCount, totalCount);
    }



    //
    // 
    //
    public Dictionary<ENT_DungeonEntity.monsterTypes, int> getHovered()
    {
        // Count total minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (Transform child in GameObject.Find("UnitContainer").transform)
        {
            if (child != null)
            {
                var type = child.GetComponent<ENT_Body>().entity_type;
                if (totalCount.ContainsKey(type))
                {
                    totalCount[type] += 1;
                }
                else totalCount.Add(type, 1);
            }
        }

        // Count selected minions
        Dictionary <ENT_DungeonEntity.monsterTypes, int> selectedCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (GameObject obj in GetComponent<SelectionManager>().GetHoveredObjects())
        {
            if (obj != null)
            {
                var type = obj.GetComponent<ENT_Body>().entity_type;
                if (selectedCount.ContainsKey(type))
                {
                    selectedCount[type] += 1;
                }
                else selectedCount.Add(type, 1);
            }
        }

        return selectedCount;
    }

    // My failed / unfinished attempt to use generics

    //Dictionary<ENT_DungeonEntity.monsterTypes, int> countMinions<T>(List<T> minions)
    //{
    //    Dictionary<ENT_DungeonEntity.monsterTypes, int> count = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
    //    foreach (T obj in minions)
    //    {
    //        var type = obj.GetComponent<ENT_Body>().entity_type;
    //        if (count.ContainsKey(type))
    //        {
    //            count[type] += 1;
    //        }
    //        else count.Add(type, 1);
    //    }
    //    return count;
    //}
}
