﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEL_Cursor : MonoBehaviour {

    List<GameObject> hoveredList;
    SelectionManager selectionManager;
    private LayerMask mask;
    public GameObject selectionProjector;
    public Renderer rend;
    public float scrollSpeedAdjustRate = 50.0f;
    public float minRadius = 1.0f;
    public float maxRadius = 10.0f;

    private GameObject _centerMinion;
    public GameObject centerMinion
    {
        get
        {
            return _centerMinion;
        }
    }

    private float _radius = 5;
    public float radius
    {
        get
        {
            return _radius;
        }
        set
        {
            _radius = value;
            OnRadiusChanged();
        }
    }

    List<GameObject> removeHoveredList;

    // Use this for initialization
    void Start () {
        mask = (1 << 11);
        selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>();
        hoveredList = new List<GameObject>();
        removeHoveredList = new List<GameObject>();
        rend = selectionProjector.GetComponent<Renderer>();
        _centerMinion = null;
    }

    void UpdatePosition()
    {
        // Raycast from cursor position
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(INP_MouseCursor.position.x, INP_MouseCursor.position.y, Camera.main.nearClipPlane));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
        {
            transform.position = hit.point;
        }
        else
        {
            transform.position = Vector3.down * 500;
        }

    }

    void UpdateHoveredObjects()
    {
        //Remove destroyed GameObjects from hovered list
        _centerMinion = null;
        float bestDistSqrXz = float.MaxValue;
        Vector3 center = transform.position;
        foreach (GameObject obj in hoveredList)
        {
            if (obj == null)
            {
                removeHoveredList.Add(obj);
                continue;
            }

            //set center minion to minion closest to the center of the selection radius (ignoring Y)
            float distSqrXz = UTL_Math.SqrDistanceXz(center, obj.transform.position);
            if (distSqrXz < bestDistSqrXz)
            {
                bestDistSqrXz = distSqrXz;
                _centerMinion = obj;
            }
        }
        foreach (GameObject obj in removeHoveredList)
        {
            hoveredList.Remove(obj);
        }
        removeHoveredList.Clear();
    }

    void OnRadiusChanged()
    {
        SphereCollider sphereCol = GetComponent<SphereCollider>();
        sphereCol.radius = _radius;
        Projector proj = selectionProjector.GetComponent<Projector>();
        proj.orthographicSize = _radius;
    }

    void UpdateSize()
    {
        if (INP_PlayerInput.getButton("ChangeSelectRadius"))
        {
            float rawInput = Input.GetAxis("Mouse ScrollWheel");
            //Debug.Log(rawInput);
            float scaledInput = rawInput * scrollSpeedAdjustRate * TME_Manager.getDeltaTime(TME_Time.type.player);
            float newRadius = radius + scaledInput;
            radius = Mathf.Clamp(newRadius, minRadius, maxRadius);
        }
    }
	
	// Update is called once per frame
	void Update () {

        // Check if need to update selection cursor
        if (SelectionManager.isInSelectMode)
        {
            UpdateSize();
            UpdatePosition();
            UpdateHoveredObjects();  
        }
        else
        {
            transform.position = Vector3.down * 500;
        }
	}

    private void OnTriggerEnter(Collider other)
    {

        foreach (GameObject selectable in selectionManager.getSelectables())
        {
            if (selectable == other.gameObject)
            {
                hoveredList.Add(other.gameObject);
                break;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        hoveredList.Remove(other.gameObject);
    }

    public List<GameObject> getHovered()
    {
        return hoveredList;
    }
}
