﻿//Zach

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CAM_State {



    protected CAM_Camera cameraScript;
    protected CAM_Dolly dolly;
    protected CAM_TargetedDolly targetDolly;
    public Vector3 targetPosition;
    public Quaternion targetRotation;
    public float distance;
    public float speedScalar = 1;


    //
    // constructor
    //
    public CAM_State()
    {
        cameraScript = GameObject.Find("Camera").GetComponent<CAM_Camera>();
        dolly = GameObject.Find("CameraDolly").GetComponent<CAM_Dolly>();
        GameObject tcdObject = GameObject.Find("TargetedCameraDolly");
        if (tcdObject != null) targetDolly = tcdObject.GetComponent<CAM_TargetedDolly>();
        distance = Vector3.Distance(dolly.transform.position, targetPosition) * speedScalar;
    }

    /// <summary>
    /// optional Initializtation function
    /// </summary>
    public virtual void Init()
    {

    }

    //
    // all types of states
    //
    public enum type
    {
        free, locked, returning, targeting
    }


    //
    // runs when this is the active camera state
    //
    public abstract void FixedUpdate();
    //public abstract void Update();
}
