﻿//Zach
//Devon
//Eli

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//
// Camera state when camera is centered around player and player can control it
//
public class CAM_Free : CAM_State
{
    private Vector3 groundPos;
    private Vector3 adjustedPos;
    private float adjustRatio;
    private int steps;
    public static bool frozen = false;
	private int layerTerrain;
	public int layerTerrainMask;
    public GameObject player;
    private Vector3 ppos;
    private Vector3 LOSvec;

    public override void Init()
    {
        layerTerrain = 14; //Must be the "ruins" layer
        layerTerrainMask = 1 << layerTerrain;
        //Debug.Log(layerTerrainMask);
        //Debug.Log(LayerMask.NameToLayer("Ruins"));
        player = GameObject.Find("Player");
    }

    //
    // runs once per frame
    // Smoothly follows camera dolly position, "f" value controls slide speed
    //
    public override void FixedUpdate()
    //public override void Update()
    {
        dolly.adjust();

		Transform target = dolly.getCamTransform();
        cameraScript.transform.position = target.position;
        cameraScript.transform.rotation = target.rotation;

		//Check if camera is below the ground
        adjustedPos = cameraScript.transform.position;
        groundPos = UTL_Dungeon.snapPositionToTerrain(adjustedPos);
        if (groundPos.y + 1.0f > adjustedPos.y)
        {
            adjustRatio = groundPos.y + 1.0f - adjustedPos.y;
            steps = 0;
            while (groundPos.y + 1.0f > adjustedPos.y && steps < 1000)
            {
                adjustedPos.x += 0.1f * adjustRatio * cameraScript.transform.forward.x;
                adjustedPos.y += 0.1f * adjustRatio * cameraScript.transform.forward.y;
                adjustedPos.z += 0.1f * adjustRatio * cameraScript.transform.forward.z;
                groundPos = UTL_Dungeon.snapPositionToTerrain(adjustedPos);
                steps++;
            }
            if (steps < 1000)
            {
                cameraScript.transform.position = adjustedPos;
            }
        }


        //Check if camera has LOS to player through terrain objects
        ppos = new Vector3(player.transform.position.x, player.transform.position.y + 2, player.transform.position.z);
        //Debug.Log("Player pos: "+ppos);  -->Tested, Ppos is correct
        LOSvec = (adjustedPos - ppos);
        //Debug.Log("adjustedPos: "+adjustedPos);  -->Tested, Adjustedpos is also correct
        //Debug.DrawLine(adjustedPos, ppos);
        if (Physics.Raycast(ppos, LOSvec, 2*LOSvec.magnitude, layerTerrainMask)) {
            //Debug.Log(adjustRatio);
            steps = 0;
            while (Physics.Raycast(ppos, LOSvec, 2*LOSvec.magnitude, layerTerrainMask) && steps < 1000) {
                LOSvec = (adjustedPos - ppos);
                adjustedPos.x += 0.1f * cameraScript.transform.forward.x;
                adjustedPos.y += 0.1f * cameraScript.transform.forward.y;
                adjustedPos.z += 0.1f * cameraScript.transform.forward.z;
                steps++;
            }
            if (steps < 1000)
            {
                //Debug.Log("Camera LoS is being blocked by an object with terrain layer " + layerTerrain);
                cameraScript.transform.position = adjustedPos;
            }
        }
    }
}
