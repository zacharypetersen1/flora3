﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAM_Dolly : MonoBehaviour
{

    CHA_Motor motor;                                        // player's transform
    Transform targetTransform;
    public Vector3 directionVec = new Vector3(-8, 13, 0);   // will become unit vector indicating cam direction

    // var's related to scrolling the camera
    public float targetScrollValue = 5;
    public float curScrollValue = 5;                        // magnitude, or distance of camera away
    public float scrollSpeed = 16;
    float[] scrollWaypoints = new float[] { 3, 6, 16, 26 }; // scroll values for: [min normal mode, max normal mode, min select mode, max select mode]

    //public float magnitudeUpperClamp = 20;
    public float rotationLimit = 10;
    public float magnitudeLowerClamp = 2;
    public float minVerticalClamp = 85; // Clamps camera when looking up
    public float maxVerticalClamp = 315; // Clamps camera when looking down

    //bool influenceCamRotation = false;                    // determines behavior of camera dolly
    public float horizontalRotation = 0;
    public float verticleRotation = 0;
    float targetRot = 0;

    public float targetTurnAngle = 0;
    private float curTurnAngle = 0;
    public float camHorizontalSpeed = 2;
    public float camVerticleSpeed = 2;
    public float maxRotationSpeed = 3f;

    public static bool locked = false;
    static CAM_Dolly singleton;
    SelectionManager selectionManager;



    //
    // Use this for initialization
    //
    void Start()
    {
        motor = GameObject.Find("Player").GetComponent<CHA_Motor>();
        //directionVec = directionVec.normalized;
        targetTransform = new GameObject("CameraDollyTarget").transform;
        singleton = this;
        selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>();
    }



    //
    // Update is called once per frame
    //
    void FixedUpdate()
    {

        // interpolate magnitude toward target magnitude
        curScrollValue = Mathf.Lerp(curScrollValue, targetScrollValue, 0.075f);

        // move dolly toward player
        transform.position = motor.transform.position;//Vector3.Slerp(transform.position, motor.transform.position, 0.5f);

        // update dolly's rotation
        transform.rotation = new Quaternion(0, 0, 0, 0);
        transform.Rotate(new Vector3(0, -horizontalRotation /*- curTurnAngle*/, 0));
        transform.Rotate(new Vector3(-verticleRotation, 0, 0), Space.Self);
    }



    //
    // gets the camera target location based on the current state of the dolly
    //
    public Transform getCamTransform()
    {
        // get [0, 1] scalar for camera angle and apply sin & exponential curve to shape it
        float rotationScalar = Mathf.Min(1f, (curScrollValue - magnitudeLowerClamp) / (rotationLimit - magnitudeLowerClamp));
        rotationScalar = Mathf.Sin((Mathf.PI / 2) * rotationScalar);
        rotationScalar = 1 - rotationScalar;
        rotationScalar = Mathf.Pow(rotationScalar, 2);

        // move the target transform to the target camera positon
        targetTransform.position = transform.position;
        targetTransform.rotation = transform.rotation;
        targetTransform.Translate(directionVec * curScrollValue);
        targetTransform.Rotate(new Vector3(30, 0, 0), Space.Self);
        //targetTransform.Rotate(new Vector3(30 + 40 * rotationScalar, 0, 180), Space.Self);

        return targetTransform;
    }



    //
    // gets a mix of the dolly's rotation and also the turn angle- the physical angle of the camera
    //
    public float getPhysicalRot()
    {
        return horizontalRotation + curTurnAngle;
    }

    public void UpdateScroll()
    {
        if (INP_PlayerInput.getButton("ChangeSelectRadius"))
            return;

        targetScrollValue = targetScrollValue + -Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;

        // check if scrolling has should cause a switch into tactical or tps mode
        if (!SelectionManager.isInSelectMode && targetScrollValue > scrollWaypoints[1])
        {
            targetScrollValue += scrollWaypoints[2] - scrollWaypoints[1];
            //selectionManager.selectAll();
            SEL_Manager.activateSelectMode();
        }
        else if (SelectionManager.isInSelectMode && targetScrollValue < scrollWaypoints[2])
        {
            targetScrollValue -= scrollWaypoints[2] - scrollWaypoints[1];
            //selectionManager.ClearSelection();
            SEL_Manager.deactivateSelectMode();
        }
        targetScrollValue = Mathf.Clamp(targetScrollValue, scrollWaypoints[0], scrollWaypoints[3]);
        //targetScrollValue = SelectionManager.isInSelectMode ? 20 : 3;
        directionVec.y = UTL_Math.mapToNewRange(curScrollValue, 3, 20, 2.1f, 1.5f);
    }

    //
    // allows dolly to adjust by listening to input
    //
    public void adjust()
    {
        // listen for input for the camera
        Vector2 camAdjust = INP_PlayerInput.get2DAxis("Camera", false) * Time.fixedDeltaTime * INP_PlayerInput.sensativity;
        //fixedDeltaTime because this is called in fixedUpdate
        //Vector2 camAdjust = INP_PlayerInput.get2DAxis("Camera", false) * Time.maximumDeltaTime * INP_PlayerInput.sensativity * 0.5f;

        // adjust the camera based on scrolling input
        if (EFX_StoryMenu.active)
        {
            return;
        }

        UpdateScroll();

        // calculate new rotation
        if (!SelectionManager.isInSelectMode)
        {
            horizontalRotation = UTL_Math.calcRotationDeg(horizontalRotation, camAdjust.x * -camHorizontalSpeed);
            verticleRotation = UTL_Math.calcRotationDeg(verticleRotation, camAdjust.y * camVerticleSpeed);
        }
        clampVertical();
    }



    //
    // Clamps camera to prevent it from going upside down
    //
    public void clampVertical()
    {
        if (verticleRotation > minVerticalClamp && verticleRotation < maxVerticalClamp)
        {
            if (Mathf.Abs(maxVerticalClamp - minVerticalClamp) / 2 > verticleRotation) verticleRotation = minVerticalClamp;
            else verticleRotation = maxVerticalClamp;
        }
    }



    //
    // Snaps dolly to default scroll value based on tactical or tps mode
    //
    public static void snapToDefaultScrollValue(bool isTacticalMode)
    {
        singleton.targetScrollValue = isTacticalMode ? 20 : 4.25f;
    }
}