﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NOI_CompoundPerlin : NOI_Noise {



    float[] multipliers;
    float magnitude;



    //
    // constructor
    //
    public NOI_CompoundPerlin(float[] setMultipliers, float setMagnitude)
    {
        multipliers = setMultipliers;
        magnitude = setMagnitude;
    }



    //
    // returns noise value at specified location
    //
    public override float getValue(float x, float y)
    {
        float val = 0;
        for(int i = 0; i < multipliers.Length; i++)
        {
            val += (Mathf.PerlinNoise(x * multipliers[i], y * multipliers[i]));
        }
        return (val / multipliers.Length) * magnitude;
    }
}
