﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NOI_PerlinNoise : NOI_Noise {



    private float spread;
    private float magnitude;
    private float seed;



    //
    // constructor
    //
    public NOI_PerlinNoise(float setSpread, float setMagnitude)
    {
        spread = setSpread;
        magnitude = setMagnitude;
        seed = UnityEngine.Random.Range(0, 1000000);
    }



    //
    // gets noise value at specified x and y value
    //
    public override float getValue(float x, float y)
    {
        return (Mathf.PerlinNoise(seed + x * spread, seed + y * spread)) * magnitude;
    }
}
