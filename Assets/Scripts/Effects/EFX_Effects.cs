﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EFX_Effects
{

    //
    // Creates an effect that looks like the ground is cracking here
    //
    public static void createGroundCrackAppear(Vector3 position)
    {
        // Clone the object
        GameObject obj = UTL_Resources.cloneAtLocation("Environment Effects/Spouts/GrassSpoutAppear", position);

        // Orient the object
        Vector2 terrainPosition = UTL_Dungeon.worldToTerrainPosition(position);
        Vector3 terrainNormal = Terrain.activeTerrain.terrainData.GetInterpolatedNormal(terrainPosition.x, terrainPosition.y);
        Vector3 lookatPosition = obj.transform.position + terrainNormal;
        obj.transform.LookAt(lookatPosition);
        obj.transform.Translate(Vector3.forward * 0.15f, Space.Self);
    }

    //
    // Creates an effect that looks like the ground is cracking here
    //
    public static void createGroundCrackDisappear(Vector3 position)
    {
        // Clone the object
        GameObject obj = UTL_Resources.cloneAtLocation("Environment Effects/Spouts/GrassSpoutDisappear", position);

        // Orient the object
        Vector2 terrainPosition = UTL_Dungeon.worldToTerrainPosition(position);
        Vector3 terrainNormal = Terrain.activeTerrain.terrainData.GetInterpolatedNormal(terrainPosition.x, terrainPosition.y);
        Vector3 lookatPosition = obj.transform.position + terrainNormal;
        obj.transform.LookAt(lookatPosition);
        obj.transform.Translate(Vector3.forward * 0.15f, Space.Self);
    }
}