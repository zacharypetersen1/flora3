﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_SelfDestroy : MonoBehaviour {

    ParticleSystem ps;

	// Use this for initialization
	void Start () {
        ps = gameObject.GetComponent<ParticleSystem>();
        if (ps == null)
        {
            ps = gameObject.GetComponentInChildren<ParticleSystem>();
        }
	}
	
	// Update is called once per frame
	void Update () {

        // destroy when particle system is not alive
        if (!ps.IsAlive())
        {
            Destroy(gameObject);
        }
    }
}
