﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_SignText : MonoBehaviour {

    // Reference to the text GameObject
    public GameObject object_to_animate;

    // Final position relative to camera
    public float x_offset = 0;
    public float y_offset = 1.5f;
    public float z_offset = 0;
    public float init_y_offset = -.37f; // How much to move
    public float speed = .02f;
    public float min_size = 0;
    public float sizeScalar = 1f;

    // p0 - p3 affect the easing function of the destroy animation
    // use this website to find desired p values http://cubic-bezier.com/#.35,-0.21,1,.46
    /*float p0 = 0f;
    float p1 = -.12f;
    float p2 = 1f;
    float p3 = -.31f;*/

    bool active = false;
    float counter = 0;
    Vector3 init_p;

    void Start()
    {
        init_p = new Vector3(x_offset, y_offset + init_y_offset, z_offset);
        object_to_animate.transform.localPosition = init_p;
    }

    // Update is called once per frame
    void Update()
    {
        if (object_to_animate != null)
        {
            if (active)
            {
                set_size(counter);
                set_position(counter);
                if (counter < 1) counter += speed;
                else if (counter > 1) counter = 1;
            }
            else
            {
                set_size(counter);
                set_position(counter);
                if (counter > 0) counter -= speed;
                else if (counter < 0) counter = 0;
            }
            set_rotation();
        }
        else Debug.Log("EFX_SignText: Object to animate not specified " + gameObject.name);
    }
    void set_position(float c)
    {
        var anim_offset = easeOut(c);
        var tar_p = new Vector3(x_offset,  y_offset - init_y_offset * (1 - anim_offset), z_offset);
        var tar_p_lerped = Vector3.Lerp(object_to_animate.transform.localPosition, tar_p, Time.deltaTime * 30);
        object_to_animate.transform.localPosition = tar_p_lerped;
    }

    void set_size(float c)
    {
        float size = min_size + (1 - min_size) * easeOut(c);
        object_to_animate.transform.localScale = new Vector3(-1 * size, size, size) * sizeScalar;
    }

    void set_rotation()
    {
        object_to_animate.transform.LookAt(Camera.main.transform);
    }

    static float easeOut(float x)
    {
        return -(2.5f) * Mathf.Pow(x, 3) + (3.5f) * Mathf.Pow(x, 2);
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            active = true;
        }
    }
    public void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            active = false;
        }
    }
}