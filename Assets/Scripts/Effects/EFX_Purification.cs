﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_Purification : MonoBehaviour {

    ParticleSystem darkPS;
    ParticleSystem.EmissionModule darkEmitter;
    ParticleSystem.ShapeModule darkShape;
    ParticleSystem.MainModule darkMain;

    ParticleSystem crystal0PS;
    ParticleSystem.EmissionModule crystal0Emitter;
    ParticleSystem.ShapeModule crystal0Shape;
    ParticleSystem.MainModule crystal0Main;

    ParticleSystem crystal0PS_;
    ParticleSystem.EmissionModule crystal0Emitter_;
    ParticleSystem.ShapeModule crystal0Shape_;
    ParticleSystem.MainModule crystal0Main_;

    ParticleSystem crystal1PS;
    ParticleSystem.EmissionModule crystal1Emitter;
    ParticleSystem.ShapeModule crystal1Shape;
    ParticleSystem.MainModule crystal1Main;

    ParticleSystem crystal1PS_;
    ParticleSystem.EmissionModule crystal1Emitter_;
    ParticleSystem.ShapeModule crystal1Shape_;
    ParticleSystem.MainModule crystal1Main_;

    ParticleSystem crystal2PS;
    ParticleSystem.EmissionModule crystal2Emitter;
    ParticleSystem.ShapeModule crystal2Shape;
    ParticleSystem.MainModule crystal2Main;

    ParticleSystem crystal2PS_;
    ParticleSystem.EmissionModule crystal2Emitter_;
    ParticleSystem.ShapeModule crystal2Shape_;
    ParticleSystem.MainModule crystal2Main_;

    ParticleSystem finish0PS;
    //ParticleSystem.EmissionModule finish0Emitter;
    //ParticleSystem.ShapeModule finish0Shape;
    //ParticleSystem.MainModule finish0Main;

    ParticleSystem finish0PS_;
    //ParticleSystem.EmissionModule finish0Emitter_;
    //ParticleSystem.ShapeModule finish0Shape_;
    //ParticleSystem.MainModule finish0Main_;

    ParticleSystem finish1PS;
    //ParticleSystem.EmissionModule finish1Emitter;
    //ParticleSystem.ShapeModule finish1Shape;
    //ParticleSystem.MainModule finish1Main;

    ParticleSystem finish1PS_;
    //ParticleSystem.EmissionModule finish1Emitter_;
    //ParticleSystem.ShapeModule finish1Shape_;
    //ParticleSystem.MainModule finish1Main_;

    ParticleSystem finish2PS;
    //ParticleSystem.EmissionModule finish2Emitter;
    //ParticleSystem.ShapeModule finish2Shape;
    //ParticleSystem.MainModule finish2Main;

    ParticleSystem finish2PS_;
    //ParticleSystem.EmissionModule finish2Emitter_;
    //ParticleSystem.ShapeModule finish2Shape_;
    //ParticleSystem.MainModule finish2Main_;

    GameObject crystal0;
    GameObject crystal1;
    GameObject crystal2;

    Renderer rend0;
    Renderer rend1;
    Renderer rend2;

    bool crystal0Shattered = false;
    bool crystal1Shattered = false;
    bool crystal2Shattered = false;

    Vector3 crystal0StartScale;
    Vector3 crystal1StartScale;
    Vector3 crystal2StartScale;

    const float crystal0BreakPoint = 0.8F;
    const float crystal1BreakPoint = 0.4F;
    const float crystal2BreakPoint = 0.03F;

    float darkSpeed;

    //ParticleSystem greenPS;
    //ParticleSystem.EmissionModule greenEmitter;
    //ParticleSystem.ShapeModule greenShape;

    bool purifying;
    bool aborting;
    bool finishing;
    int shatterFrame;
    [HideInInspector]
    public float amountPurified;

    // Use this for initialization
    void Start() {
        setUpParticleSystems();
        purifying = false;
        aborting = false;
        finishing = false;
        shatterFrame = 0;
        darkSpeed = darkMain.simulationSpeed;
    }

    void setUpParticleSystems()
    {
        darkPS = gameObject.transform.Find("DarkPurity").GetComponentInChildren<ParticleSystem>();

        try
        {
            Transform CrystalPurityEffect0 = gameObject.transform.Find("CPurityEffect0");
            crystal0PS = CrystalPurityEffect0.GetChild(0).GetComponent<ParticleSystem>();
            crystal0PS_ = CrystalPurityEffect0.GetChild(1).GetComponent<ParticleSystem>();

            finish0PS = CrystalPurityEffect0.GetChild(2).GetComponent<ParticleSystem>();
            finish0PS_ = CrystalPurityEffect0.GetChild(3).GetComponent<ParticleSystem>();

            crystal0 = finish0PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject;
            crystal0StartScale = crystal0.transform.localScale;
            rend0 = crystal0.GetComponent<Renderer>();
        }
        catch
        {
            print("CrystalPurityEffect0 not set up right on "+gameObject.transform.parent.name);
        }


        try
        {
            Transform CrystalPurityEffect1 = gameObject.transform.Find("CPurityEffect1");
            crystal1PS = CrystalPurityEffect1.GetChild(0).GetComponent<ParticleSystem>();
            crystal1PS_ = CrystalPurityEffect1.GetChild(1).GetComponent<ParticleSystem>();

            finish1PS = CrystalPurityEffect1.GetChild(2).GetComponent<ParticleSystem>();
            finish1PS_ = CrystalPurityEffect1.GetChild(3).GetComponent<ParticleSystem>();

            crystal1 = finish1PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject;
            crystal1StartScale = crystal1.transform.localScale;
            rend1 = crystal1.GetComponent<Renderer>();
        }
        catch
        {
            print("CrystalPurityEffect1 not set up right on " + gameObject.transform.parent.name);
        }


        try
        {
            Transform CrystalPurityEffect2 = gameObject.transform.Find("CPurityEffect2");
            crystal2PS = CrystalPurityEffect2.GetChild(0).GetComponent<ParticleSystem>();
            crystal2PS_ = CrystalPurityEffect2.GetChild(1).GetComponent<ParticleSystem>();

            finish2PS = CrystalPurityEffect2.GetChild(2).GetComponent<ParticleSystem>();
            finish2PS_ = CrystalPurityEffect2.GetChild(3).GetComponent<ParticleSystem>();

            crystal2 = finish2PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject;
            crystal2StartScale = crystal2.transform.localScale;
            rend2 = crystal2.GetComponent<Renderer>();
        }
        catch
        {
            print("CrystalPurityEffect2 not set up right on " + gameObject.transform.parent.name);
        }

        darkEmitter = darkPS.emission;
        crystal0Emitter = crystal0PS.emission;
        crystal0Emitter_ = crystal0PS_.emission;
        crystal1Emitter = crystal1PS.emission;
        crystal1Emitter_ = crystal1PS_.emission;
        crystal2Emitter = crystal2PS.emission;
        crystal2Emitter_ = crystal2PS_.emission;

        darkMain = darkPS.main;
        crystal0Main = crystal0PS.main;
        crystal0Main_ = crystal0PS_.main;
        crystal1Main = crystal1PS.main;
        crystal1Main_ = crystal1PS_.main;
        crystal2Main = crystal2PS.main;
        crystal2Main_ = crystal2PS_.main;

        darkShape = darkPS.shape;
        crystal0Shape = crystal0PS.shape;
        crystal0Shape_ = crystal0PS_.shape;
        crystal1Shape = crystal1PS.shape;
        crystal1Shape_ = crystal1PS_.shape;
        crystal2Shape = crystal2PS.shape;
        crystal2Shape_ = crystal2PS_.shape;

        darkEmitter.enabled = false;
        crystal0Emitter.enabled = false;
        crystal0Emitter_.enabled = false;
        crystal1Emitter.enabled = false;
        crystal1Emitter_.enabled = false;
        crystal2Emitter.enabled = false;
        crystal2Emitter_.enabled = false;
    }

    // Update is called once per frame
    void FixedUpdate() {

        if (aborting)
        {
            growCrystal0();
            growCrystal1();
            growCrystal2();
        }
        if (purifying || aborting)
        {
            darkMain.simulationSpeed = (3 * darkSpeed) * (1 - amountPurified);
            //print("amount purified = " + amountPurified);
            //darkShape.normalOffset = -.1f * (1 - amountPurified);
        }
        if (purifying)
        {
            if (!crystal0Shattered)
            {
                if (amountPurified <= crystal0BreakPoint) //1/3 purified
                {
                    shatterCrystal0();
                    crystal0Shattered = true;
                }
            }
            if (!crystal1Shattered)
            {
                if (amountPurified <= crystal1BreakPoint) //1/3 purified
                {
                    shatterCrystal1();
                    crystal1Shattered = true;
                }
            }
            if (!crystal2Shattered)
            {
                if (amountPurified <= crystal2BreakPoint) //1/3 purified
                {
                    shatterCrystal2();
                    crystal2Shattered = true;
                }
            }
        }
        if (finishing)
        {
            shatterCrystals();
        }
    }

    public void shatterCrystal0()
    {
        finish0PS.Play();
        finish0PS_.Play();
        finish0PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject.GetComponent<MeshRenderer>().enabled = false;
        crystal0Emitter.enabled = false;
        crystal0Emitter_.enabled = false;
    }

    public void shatterCrystal1()
    {
        finish1PS.Play();
        finish1PS_.Play();
        finish1PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject.GetComponent<MeshRenderer>().enabled = false;
        crystal1Emitter.enabled = false;
        crystal1Emitter_.enabled = false;
    }

    public void shatterCrystal2()
    {
        finish2PS.Play();
        finish2PS_.Play();
        finish2PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject.GetComponent<MeshRenderer>().enabled = false;
        crystal2Emitter.enabled = false;
        crystal2Emitter_.enabled = false;
    }

    //returns true when finished
    void shatterCrystals()
    {
        darkMain.simulationSpeed = darkSpeed;


        if (shatterFrame >= 15)
        {
            //finishing = false;
            crystal0Emitter.enabled = false;
            crystal1Emitter.enabled = false;
            crystal2Emitter.enabled = false;
            crystal0Emitter_.enabled = false;
            crystal1Emitter_.enabled = false;
            crystal2Emitter_.enabled = false;
            shatterFrame = 0;
            return;
        }

        if (shatterFrame >= 10 && darkEmitter.enabled == true)
        {
            darkEmitter.enabled = false;
        }
        else
        {
            darkMain.startSpeed = .005F * shatterFrame;
        }

        shatterFrame++;
        return;
    }

    public void onPurifyStart()
    {
        darkEmitter.enabled = true;
        crystal0Emitter.enabled = true;
        crystal1Emitter.enabled = true;
        crystal2Emitter.enabled = true;
        crystal0Emitter_.enabled = true;
        crystal1Emitter_.enabled = true;
        crystal2Emitter_.enabled = true;
        //greenEmitter.enabled = true;
        darkShape.normalOffset = 0f;
        purifying = true;
    }

    public void onPurifyCancel()
    {
        darkEmitter.enabled = false;
        //greenEmitter.enabled = false;
        purifying = false;
        aborting = true;
        crystal0Emitter.enabled = false;
        crystal1Emitter.enabled = false;
        crystal2Emitter.enabled = false;
        crystal0Emitter_.enabled = false;
        crystal1Emitter_.enabled = false;
        crystal2Emitter_.enabled = false;
    }

    public void onPurifyFinish()
    {
        //darkEmitter.enabled = false;
        //crystal0Emitter.enabled = false;
        //crystal1Emitter.enabled = false;
        //crystal2Emitter.enabled = false;
        //crystal0Emitter_.enabled = false;
        //crystal1Emitter_.enabled = false;
        //crystal2Emitter_.enabled = false;
        //greenEmitter.enabled = false;
        purifying = false;
        finishing = true;
        /*
        crystal0Main.startSpeed = 6;
        crystal0Main_.startSpeed = 6;
        crystal1Main.startSpeed = 6;
        crystal1Main_.startSpeed = 6;
        crystal2Main.startSpeed = 6;
        crystal2Main_.startSpeed = 6;

        crystal0PS.Emit(200);
        crystal0PS_.Emit(200);
        crystal1PS.Emit(200);
        crystal1PS_.Emit(200);
        crystal2PS.Emit(200);
        crystal2PS_.Emit(200);*/

    }


    public void growCrystal0()
    {
        float scalar = Mathf.Max((amountPurified - crystal0BreakPoint), 0);
        if (scalar > 0)
        {
            crystal0Shattered = false;
            rend0.enabled = true;
            crystal0.transform.localScale = (scalar * (1 / (1 - crystal0BreakPoint))) * crystal0StartScale;
        }
        
    }

    public void growCrystal1()
    {
        float scalar = Mathf.Max((amountPurified - crystal1BreakPoint), 0);
        if (scalar > 0)
        {
            crystal1Shattered = false;
            rend1.enabled = true;
            crystal1.transform.localScale = (scalar * (1 / (1 - crystal1BreakPoint))) * crystal1StartScale;
        }

    }

    public void growCrystal2()
    {
        float scalar = Mathf.Max((amountPurified - crystal2BreakPoint), 0);
        if (scalar > 0)
        {
            crystal2Shattered = false;
            rend2.enabled = true;
            crystal2.transform.localScale = (scalar * (1 / (1 - crystal2BreakPoint))) * crystal2StartScale;
        }

    }
    /*
    public IEnumerable growCrystal0()
    {
        float i = 0;
        GameObject crystal = finish0PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject;
        if (i == 0)
        {
            crystal.GetComponent<MeshRenderer>().enabled = true;

        }
        while (i < 1)
        {
            crystal.transform.localScale = new Vector3(1, 1, 1) * i;
            i += .02F;
            yield return i;   
        }  
    }

    public IEnumerable growCrystal1()
    {
        float i = 0;
        GameObject crystal = finish1PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject;
        if (i == 0)
        {
            crystal.GetComponent<MeshRenderer>().enabled = true;

        }
        while (i < 1)
        {
            crystal.transform.localScale = new Vector3(1, 1, 1) * i;
            i += .02F;
            yield return i;
        }
    }

    public IEnumerable growCrystal2()
    {
        float i = 0;
        GameObject crystal = finish2PS.gameObject.transform.parent.GetComponent<PSMeshRendererUpdater>().MeshObject;
        if (i == 0)
        {
            crystal.GetComponent<MeshRenderer>().enabled = true;

        }
        while (i < 1)
        {
            crystal.transform.localScale = new Vector3(1, 1, 1) * i;
            i += .02F;
            yield return i;
        }
    }
    */

    public void onPurifyCancelFinish()
    {
        darkEmitter.enabled = false;
        aborting = false;
    }
}
