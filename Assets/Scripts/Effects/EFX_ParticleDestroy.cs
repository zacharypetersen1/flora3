﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
// Destroys gameobject when particle system is done
//
public class EFX_ParticleDestroy : MonoBehaviour {

    private void Start()
    {
        Destroy(gameObject, GetComponent<ParticleSystem>().main.duration);
    }

}