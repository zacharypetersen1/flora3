﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_DestroyChildrenParticles : MonoBehaviour {

    public GameObject obj1;
    public GameObject obj2;
    ParticleSystem ps1;
    ParticleSystem ps2;

    // Use this for initialization
    void Start()
    {
        try
        {
            ps1 = obj1.GetComponent<ParticleSystem>();
            ps2 = obj2.GetComponent<ParticleSystem>();
        }catch{
            throw new System.Exception("not all particle systems in EFX_DestroyChildrenParticles");
        }
        

        if (ps1.main.duration > ps2.main.duration)
        {

        }else
        {
            ps1 = ps2;
        }
    }

    // Update is called once per frame
    void Update()
    {

        // destroy when particle system is not alive
        try
        {
            if (!ps1.IsAlive())
            {
                Destroy(gameObject);
            }
        }catch
        {
            throw new System.Exception("ps1 does not exist");
        }
        
    }
}
