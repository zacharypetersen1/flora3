﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TFX_TextureApplier : TFX_TextureEffect {



    Texture2D tex;
    float scalingValue;


    //
    // Constructor
    //
    public TFX_TextureApplier(Texture2D setTex, float setScalingValue)
    {
        tex = setTex;
        scalingValue = setScalingValue;
    }



    //
    // applies effect on pixel
    //
    public override Color alter(float x, float y, Color initial)
    {
        x = ((x * scalingValue) % 1) * tex.width;
        y = ((y * scalingValue) % 1) * tex.height;
        return tex.GetPixel((int)x, (int)y);
    }
}
