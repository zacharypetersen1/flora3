﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TFX_PerlinShading : TFX_TextureEffect {



    float sprScalar;
    float magScalar;
    NOI_PerlinNoise noise;



    //
    // constructor
    //
    public TFX_PerlinShading(float spreadScalar, float magnitudeScalar)
    {
        sprScalar = spreadScalar;
        magScalar = magnitudeScalar;
        noise = new NOI_PerlinNoise(spreadScalar, magnitudeScalar);
    }



    //
    // applies effect to single pixel
    //
    public override Color alter(float x, float y, Color initial)
    {
        float shade = noise.getValue(x, y);
        return new Color(initial.r + shade, initial.g + shade, initial.b + shade, initial.a);
    }
}
