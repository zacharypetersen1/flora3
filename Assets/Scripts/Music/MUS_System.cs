﻿// Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MUS_System : MonoBehaviour {

    //static FMOD.Studio.System system;

    [FMODUnity.EventRef]
    public string music_world = "event:/Music/music_world";
    public bool playMusic = true;
    FMOD.Studio.EventInstance musWorldEvent;

    [FMODUnity.EventRef]
    public string ambience = "event:/Music/ambience";
    public bool playAmbience = true;
    FMOD.Studio.EventInstance ambEvent;

	[FMODUnity.EventRef]
	public string surfSound = "event:/SFX/twig_surf";
	FMOD.Studio.EventInstance surfEvent;

    [FMODUnity.EventRef]
    string reloadSound = "event:/SFX/twig_reload";
    FMOD.Studio.EventInstance reloadEvent;

    //Vector3 zero = new Vector3(0,0,0);

    bool fmodNull = false;

    public static MUS_System singleton;

    MUS_Menu menuMusic;

    // Use this for initialization
    void Start() {
        //FMOD.Studio.System.create(out system);
        //system.initialize(1000, FMOD.Studio.INITFLAGS.LIVEUPDATE, FMOD.INITFLAGS.NORMAL, System.IntPtr.Zero);
        
        singleton = this;
        try
        {
            musWorldEvent = FMODUnity.RuntimeManager.CreateInstance(music_world);
        }
        catch(System.Exception e)
        {
            Debug.LogError(e);
            fmodNull = true;
        }
        if (!fmodNull)
        {
            ambEvent = FMODUnity.RuntimeManager.CreateInstance(ambience);
            surfEvent = FMODUnity.RuntimeManager.CreateInstance(surfSound);
            reloadEvent = FMODUnity.RuntimeManager.CreateInstance(reloadSound);

            if (playMusic)
            {
                musWorldEvent.start();
            }
            MusicIdle(1);

            if (playAmbience)
            {
                ambEvent.start();
            }
            if (GameObject.Find("MusicMenu") != null)
            {
                menuMusic = GameObject.Find("MusicMenu").GetComponent<MUS_Menu>();
                menuMusic.stopMusic();
            }
        }

        if (playAmbience)
        {
            ambEvent.start();
        }
        //FMODUnity.RuntimeManager.PlayOneShot(ambience, zero);
    }

    void OnDestroy()
    {
        musWorldEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    //
    // Music control
    //
    public void MusicIdle(float dPar)
    {
        if (fmodNull) return;
        musWorldEvent.setParameterValue("Idle", dPar);
    }
    
    public void MusicCorrupt(float dPar)
    {
        if (fmodNull) return;
        musWorldEvent.setParameterValue("Corrupt", dPar);
    }
    
    public void MusicBoost(float dPar)
    {
        if (fmodNull) return;
        musWorldEvent.setParameterValue("Surfing", dPar);
    }

    public void StopMusic()
    {
        musWorldEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    //
    // Ambient control
    //
    public void ambientCorrupt(float dPar)
    {
        if (fmodNull) return;
        ambEvent.setParameterValue("corrupt", dPar);
    }

    public void StopAmbience()
    {
        ambEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    //
    // Surf control
    //
    public void surfSoundStart()
    {
        if (fmodNull) return;
        surfEvent.start ();
	}

	public void surfSoundStop()
    {
        if (fmodNull) return;
        surfEvent.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
	}

	public void surfSoundSpeed(float dPar)
    {
        if (fmodNull) return;
        surfEvent.setParameterValue ("speed", dPar);
	}

	public void surfSoundTrue(float dPar)
    {
        if (fmodNull) return;
        surfEvent.setParameterValue ("isSurfing", dPar);
	}

    public void surfSoundCorrupt(float dPar)
    {
        if (fmodNull) return;
        surfEvent.setParameterValue ("corrupt", dPar);
    }

    // Reload control
    public void reloadSoundStart()
    {
        if (fmodNull) return;
        reloadEvent.setParameterValue("Active", 1);
        reloadEvent.start();
    }

    public void reloadSoundStop()
    {
        if (fmodNull) return;
        reloadEvent.setParameterValue("Active", 0);
        reloadEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
