﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AIR_Respawn {

    static Vector3 spawnPoint;
    public static Vector3 SpawnPoint
    {
        get { return spawnPoint; }
        set
        {
            spawnPoint = UTL_Dungeon.snapPositionToTerrain(value);
        }
    }

    static GameObject player;
    static PLY_AllyManager allyManager;

    public static void Respawn()
    {
        if(player == null)
        {
            player = GameObject.Find("Player");
        }
        //player.SetActive(false);
        player.transform.position = spawnPoint;
        player.GetComponent<CHA_Motor>().resetPastLocation();
        //player.SetActive(true);
        if (allyManager == null)
        {
            allyManager = player.GetComponent<PLY_AllyManager>();
        }
        allyManager.recallMinions(allyManager.getAllAllies);
        allyManager.commandModule.FollowCommand(allyManager.getAllAllies);
    }
}
