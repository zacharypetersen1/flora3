﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_Brain : MonoBehaviour
{
    /*//FOR THE MONSTER
    [HideInInspector]
    public float CL_MAX;
    [HideInInspector]
    public float CL;
    int teamSize;
    int teamNearMe;
    int targetingMe;

    status myStatus;
    groundType myGround;
    monsterType myType;
    mood myMood;
    goal myGoal;
    work myJob; 

    BehaviorExecutor AI;
    BehaviorTree currentTree;

    private bool purity;
    [HideInInspector]
    public PLY_AllyManager allyManager;
    public ENT_Body myBody;

    //public BrickAsset TrackTree;
    public BrickAsset AttackTree;
    public BrickAsset WorkTree;

    public BrickAsset AllyFollowTree;
    public BrickAsset AllyWaitTree;
    public BrickAsset AllyGoToTree;

    public bool shouldBeWorking;

    public GameObject spawnedBy
    {
        get { return myBody.spawnedBy; }
        set { myBody.spawnedBy = value; }
    }

    //tells the monster the area it should wait around.
    private Vector2 waitPos;

    public Vector2 WaitPos {
        get { return waitPos; }
        set { waitPos = value; }
    }

    public UIX_CameraFacingBillboard_Minion healthBar;

    #region ENUMS DEC & GET/SET
    public enum status
    {
        //statuses go here
        none, blind, stuck
    }

    public enum groundType
    {
        grass,rock,corruption
    }

    //CHANGE THIS SHIT YO!!!!!!!!!
    public enum monsterType
    {
        basic, special_one, special_two
    }

    public enum mood
    {
        neutral, scared, angry
    }

    public enum goal
    {
        //behavior goals
        track, attack, wander, patrol, defend, wait, work
    }

    public enum work
    {
        //idle behaviors
        gather, transport, maintain, corrupt
    }

    public status MyStatus
    {
        get { return myStatus; }
        set { myStatus = value; }
    }

    public groundType MyGround
    {
        get { return myGround; }
        set { myGround = value; }
    }

    public monsterType MyType
    {
        get { return myType; }
        set { myType = value; }
    }

    public mood MyMood
    {
        get { return myMood; }
        set { myMood = value; }
    }

    public goal MyGoal
    {
        get { return myGoal; }
        set { myGoal = value; }
    }

    public work MyJob
    {
        get { return myJob; }
        set { myJob = value; }
    }

    public enum BehaviorTree
    {
        AttackTree, WorkTree, AllyFollowTree, AllyWaitTree, AllyGoToTree
    }
    #endregion

    #region GET & SET FUCNTIONS

    public int TeamSize
    {
        get { return teamSize; }
        set { teamSize = value; }
    }

    public int TeamNearMe
    {
        get { return teamNearMe; }
        set { teamNearMe = value; }
    }

    public int TargetingMe
    {
        get { return targetingMe; }
        set { targetingMe = value; }
    }

    public bool Purity
    {
        get { return purity; }
        set { purity = value; }
    }

    public ENT_Body MyBody
    {
        get { return myBody; }
        set { myBody = value; }
    }

    public BehaviorTree CurrentTree
    {
        get { return currentTree; }
        set
        {
            currentTree = value;
            switch (currentTree)
            {
                case BehaviorTree.AttackTree:
                    AI.behavior = AttackTree;
                    break;
                case BehaviorTree.WorkTree:
                    AI.behavior = WorkTree;
                    break;
                case BehaviorTree.AllyFollowTree:
                    AI.behavior = AllyFollowTree;
                    break;
                case BehaviorTree.AllyWaitTree:
                    AI.behavior = AllyWaitTree;
                    break;
                case BehaviorTree.AllyGoToTree:
                    AI.behavior = AllyGoToTree;
                    break;
            }
        }
    }
    #endregion 

    // Use this for initialization
    void Awake () {
        //purity = true;
        allyManager = GameObject.Find("Player").GetComponent<PLY_AllyManager>();
        AI = gameObject.GetComponent<BehaviorExecutor>();
        myBody = gameObject.GetComponent<ENT_Body>();
        CL = CL_MAX;
        healthBar = gameObject.GetComponentInChildren<UIX_CameraFacingBillboard_Minion>();
        //allyManager.addAlly(gameObject);
    }
	
	// Update is called once per frame
	void FixedUpdate () {

    }

    public GameObject getNearestAlly()
    {
        return allyManager.getNearestAlly(gameObject.transform.position);
    }

    public GameObject getNearestEnemy()
    {
        return allyManager.getNearestEnemy(gameObject.transform.position);
    }

    // WIP

    public void onPurify()
    {
        purity = true;
        allyManager.removeEnemy(gameObject);
        allyManager.addAlly(gameObject);
        allyManager.setState(allyManager.getState());
    }

    public void onCorrupt()
    {
        purity = false;
        allyManager.removeAlly(gameObject);
        allyManager.addEnemy(gameObject);
        currentTree = BehaviorTree.WorkTree;
    }

    public void onCleanse()
    {
        if (purity)
        {
            allyManager.removeAlly(gameObject);
        }
        else
        {
            allyManager.removeEnemy(gameObject);
        }
    }
    */
}
