﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIE_CircleEncounter : MonoBehaviour {

    float timer = 0;
    public float maxTimer = 0;

    public bool repeat = true;
    float repeatTimer = 0;
    public float maxRepeatTimer = 60f;

    enum State
    {
        primed, active, inactive
    }
    State myState;
    State MyState
    {
        set
        {
            if (value == State.active)
            {
                timer = maxTimer;
                myState = value;
            }
            else if (value == State.inactive && repeat != false)
            {
                repeatTimer = maxRepeatTimer;
                myState = value;
            }
            else
            {
                myState = value;
            }
        }
    }

    public int flowerT1 = 0;
    public int treeT1 = 0;
    public int mushroomT1 = 0;
    public int confusionT2 = 0;
    public int slowT2 = 0;
    public int necromancerT2 = 0;

    public int randomT1 = 0;
    public int randomT2 = 0;

    SphereCollider col;
    float diameter;
    Vector3 center;

    Vector3 spawnCenter;

    public bool UseColliderInfo = true;
    public float spawnRadius = 10;
    
    Pair<int, ENT_DungeonEntity.monsterTypes>[] typeToUse = new Pair<int, ENT_DungeonEntity.monsterTypes>[6];

    private void Awake()
    {
        typeToUse[0] = new Pair<int, ENT_DungeonEntity.monsterTypes>(flowerT1, ENT_DungeonEntity.monsterTypes.flowerMinion);
        typeToUse[1] = new Pair<int, ENT_DungeonEntity.monsterTypes>(treeT1, ENT_DungeonEntity.monsterTypes.treeMinion); 
        typeToUse[2] = new Pair<int, ENT_DungeonEntity.monsterTypes>(mushroomT1, ENT_DungeonEntity.monsterTypes.mushroomMinion); 
        typeToUse[3] = new Pair<int, ENT_DungeonEntity.monsterTypes>(slowT2, ENT_DungeonEntity.monsterTypes.slowMinion); 
        typeToUse[4] = new Pair<int, ENT_DungeonEntity.monsterTypes>(necromancerT2, ENT_DungeonEntity.monsterTypes.necroMinion); 
        typeToUse[5] = new Pair<int, ENT_DungeonEntity.monsterTypes>(confusionT2, ENT_DungeonEntity.monsterTypes.confusionMinion); 
    }

    // Use this for initialization
    void Start () {
        MyState = State.primed;
        timer = maxTimer;

        col = gameObject.GetComponent<SphereCollider>();
        diameter = col.radius;
        center = gameObject.transform.position;
        //print((int)difficulty);
	}
	
	// Update is called once per frame
	void Update () {
        if (myState == State.active)
        {
            timer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if (timer <= 0)
            {
                spawnTrap();
                if (repeat == false)
                {
                    destroyTrap();
                }
                MyState = State.inactive;
            }
        }
        else if (myState == State.inactive)
        {
            repeatTimer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if (repeatTimer <= 0)
            {
                MyState = State.primed;
            }
        }
    }

    void spawnTrap()
    {
        // spawn tier one minions
        for (int i = 0; i < typeToUse.Length; i++)
        {
            for(int j = 0; j < typeToUse[i].value1; j++)
            {
                if(spawnRadius == 0)
                {
                    spawnRadius = diameter;
                }
                Vector3 spawnPosition = UTL_Dungeon.getRandomPointInCircle(spawnCenter, spawnRadius);
                GameObject newMinion = UTL_Dungeon.spawnMinion(typeToUse[i].value2, spawnPosition, false);
            }
        }

        for(int i = 0; i < randomT1; i++)
        {
            if (spawnRadius == 0)
            {
                spawnRadius = diameter;
            }
            Vector3 spawnPosition = UTL_Dungeon.getRandomPointInCircle(spawnCenter, spawnRadius);
            GameObject newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(0, 3), spawnPosition, false);
        }

        for(int i = 0; i < randomT2; i++)
        {
            if (spawnRadius == 0)
            {
                spawnRadius = diameter;
            }
            Vector3 spawnPosition = UTL_Dungeon.getRandomPointInCircle(spawnCenter, spawnRadius);
            GameObject newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(3, 6), spawnPosition, false);
        }
    }

    void destroyTrap()
    {
        Destroy(gameObject);
    }

    void setSpawn(Vector3 position)
    {
        if (UseColliderInfo)
        {
            spawnCenter = center;
            spawnRadius = diameter;
        }else
        {
            spawnCenter = position;
        }
        
    }

    void OnTriggerEnter(Collider allyCollider)
    {
        if (myState == State.primed)
        {
            if (allyCollider.tag == "Player")
            {
                setSpawn(allyCollider.transform.position);
                MyState = State.active;
            }
            else if (allyCollider.tag == "Minion")
            {
                if (allyCollider.gameObject.GetComponent<ENT_Body>().isAlly)
                {
                    setSpawn(allyCollider.transform.position);
                    MyState = State.active;
                }
            }
        }
    }

}
