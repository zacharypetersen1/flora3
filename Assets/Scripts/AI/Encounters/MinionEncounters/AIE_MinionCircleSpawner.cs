﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIE_MinionCircleSpawner : MonoBehaviour {

    public int enemyFlowerT1 = 0;
    public int enemyTreeT1 = 0;
    public int enemyMushroomT1 = 0;
    public int enemyConfusionT2 = 0;
    public int enemySlowT2 = 0;
    public int enenmyNecromancerT2 = 0;

    public int enemyRandomT1 = 0;
    public int enemyRandomT2 = 0;

    public int allyFlowerT1 = 0;
    public int allyTreeT1 = 0;
    public int allyMushroomT1 = 0;
    public int allyConfusionT2 = 0;
    public int allySlowT2 = 0;
    public int allyNecromancerT2 = 0;

    public int allyRandomT1 = 0;
    public int allyRandomT2 = 0;

    AIE_MinionEngagementManager manager;

    BoxCollider col;
    //float diameter;
    Vector3 center;
    float length, width;

    Pair<int, ENT_DungeonEntity.monsterTypes>[] enemiesToUse = new Pair<int, ENT_DungeonEntity.monsterTypes>[6];
    Pair<int, ENT_DungeonEntity.monsterTypes>[] alliesToUse = new Pair<int, ENT_DungeonEntity.monsterTypes>[6];

    private void Awake()
    {
        //Set enemies to spawn
        enemiesToUse[0] = new Pair<int, ENT_DungeonEntity.monsterTypes>(enemyFlowerT1, ENT_DungeonEntity.monsterTypes.flowerMinion);
        enemiesToUse[1] = new Pair<int, ENT_DungeonEntity.monsterTypes>(enemyTreeT1, ENT_DungeonEntity.monsterTypes.treeMinion);
        enemiesToUse[2] = new Pair<int, ENT_DungeonEntity.monsterTypes>(enemyMushroomT1, ENT_DungeonEntity.monsterTypes.mushroomMinion);
        enemiesToUse[3] = new Pair<int, ENT_DungeonEntity.monsterTypes>(enemySlowT2, ENT_DungeonEntity.monsterTypes.slowMinion);
        enemiesToUse[4] = new Pair<int, ENT_DungeonEntity.monsterTypes>(enenmyNecromancerT2, ENT_DungeonEntity.monsterTypes.necroMinion);
        enemiesToUse[5] = new Pair<int, ENT_DungeonEntity.monsterTypes>(enemyConfusionT2, ENT_DungeonEntity.monsterTypes.confusionMinion);

        //Set allies to spawn
        alliesToUse[0] = new Pair<int, ENT_DungeonEntity.monsterTypes>(allyFlowerT1, ENT_DungeonEntity.monsterTypes.flowerMinion);
        alliesToUse[1] = new Pair<int, ENT_DungeonEntity.monsterTypes>(allyTreeT1, ENT_DungeonEntity.monsterTypes.treeMinion);
        alliesToUse[2] = new Pair<int, ENT_DungeonEntity.monsterTypes>(allyMushroomT1, ENT_DungeonEntity.monsterTypes.mushroomMinion);
        alliesToUse[3] = new Pair<int, ENT_DungeonEntity.monsterTypes>(allySlowT2, ENT_DungeonEntity.monsterTypes.slowMinion);
        alliesToUse[4] = new Pair<int, ENT_DungeonEntity.monsterTypes>(allyNecromancerT2, ENT_DungeonEntity.monsterTypes.necroMinion);
        alliesToUse[5] = new Pair<int, ENT_DungeonEntity.monsterTypes>(allyConfusionT2, ENT_DungeonEntity.monsterTypes.confusionMinion);
    }

    // Use this for initialization
    void Start()
    {
        col = gameObject.GetComponent<BoxCollider>();
        //diameter = col.radius;
        gameObject.transform.position = UTL_Dungeon.snapPositionToTerrain(gameObject.transform.position);

        center = transform.position;
        if (col.size.z >= col.size.x)
        {
            width = col.size.z;
            length = col.size.x;
        }
        else
        {
            width = col.size.x;
            length = col.size.z;
        }

        manager = GetComponentInParent<AIE_MinionEngagementManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.MyState == AIE_MinionEngagementManager.State.active)
        {
            manager.timer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if (manager.timer <= 0)
            {
                spawnTrap();
                if (manager.repeat == false)
                {
                    Destroy(gameObject.transform.parent.gameObject);
                }
                manager.MyState = AIE_MinionEngagementManager.State.inactive;
            }
        }
        else if (manager.MyState == AIE_MinionEngagementManager.State.inactive)
        {
            manager.repeatTimer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if (manager.repeatTimer <= 0)
            {
                manager.MyState = AIE_MinionEngagementManager.State.primed;
            }
        }

    }

    Vector3 enemyLocation()
    {
        return new Vector3(center.x + Random.Range(-1 * (length / 2), length / 2), center.z, center.z + Random.Range(0, width / 2));
    }

    Vector3 allyLocation()
    {
        return new Vector3(center.x + Random.Range(-1 * (length / 2), length / 2), center.z, center.z - Random.Range(0, width / 2));
    }


    void spawnTrap()
    {
        // spawn enemies first
        //Debug.Log(center);
        for (int i = 0; i < enemiesToUse.Length; i++)
        {
            for (int j = 0; j < enemiesToUse[i].value1; j++)
            {
                Vector3 spawnPosition = enemyLocation();
                GameObject newMinion = UTL_Dungeon.spawnMinion(enemiesToUse[i].value2, spawnPosition, false);
                newMinion.transform.rotation = Quaternion.Euler(center - newMinion.transform.position);
            }
        }

        for (int i = 0; i < enemyRandomT1; i++)
        {
            Vector3 spawnPosition = enemyLocation();
            GameObject newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(0, 3), spawnPosition, false);
            newMinion.transform.rotation = Quaternion.Euler(center - newMinion.transform.position);
        }

        for (int i = 0; i < enemyRandomT2; i++)
        {
            Vector3 spawnPosition = enemyLocation();
            GameObject newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(3, 6), spawnPosition, false);
            newMinion.transform.rotation = Quaternion.Euler(center - newMinion.transform.position);
        }

        /////////////////////////////////////////////////////////
        // spawn allies second
        for (int i = 0; i < alliesToUse.Length; i++)
        {
            for (int j = 0; j < alliesToUse[i].value1; j++)
            {
                Vector3 spawnPosition = allyLocation();
                GameObject newMinion = UTL_Dungeon.spawnMinion(alliesToUse[i].value2, spawnPosition, true);
                newMinion.transform.rotation = Quaternion.Euler(center - newMinion.transform.position);
            }
        }

        for (int i = 0; i < allyRandomT1; i++)
        {
            Vector3 spawnPosition = allyLocation();
            GameObject newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(0, 3), spawnPosition, true);
            newMinion.transform.rotation = Quaternion.Euler(center - newMinion.transform.position);
        }

        for (int i = 0; i < allyRandomT2; i++)
        {
            Vector3 spawnPosition = allyLocation();
            GameObject newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(3, 6), spawnPosition, true);
            newMinion.transform.rotation = Quaternion.Euler(center - newMinion.transform.position);
        }
    }

    void destroyTrap()
    {
        Destroy(gameObject);
    }
}
