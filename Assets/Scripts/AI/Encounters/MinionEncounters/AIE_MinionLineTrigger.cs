﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIE_MinionLineTrigger : MonoBehaviour {

    AIE_MinionEngagementManager manager;

    BoxCollider col;
    float length = 0;
    // Use this for initialization

    void Start()
    {
        manager = GetComponentInParent<AIE_MinionEngagementManager>();
        col = gameObject.GetComponent<BoxCollider>();
        if (col.size.x > length)
        {
            length = col.size.x;
        }
        else if (col.size.y > length)
        {
            length = col.size.y;
        }
        else if (col.size.z > length)
        {
            length = col.size.z;
        }

        col.transform.position = UTL_Dungeon.snapPositionToTerrain(col.transform.position);
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnTriggerEnter(Collider allyCollider)
    {
        if (manager.MyState == AIE_MinionEngagementManager.State.primed)
        {
            if (allyCollider.tag == "Player")
            {
                manager.MyState = AIE_MinionEngagementManager.State.active;
            }
            else if (allyCollider.tag == "Minion")
            {
                if (allyCollider.gameObject.GetComponent<ENT_Body>().isAlly)
                {
                    manager.MyState = AIE_MinionEngagementManager.State.active;
                }
            }
        }
    }

}
