﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_MinionHider : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Minion")
        {
            other.GetComponent<ENT_Body>().ShowMinion(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Minion")
        {
            other.GetComponent<ENT_Body>().ShowMinion(false);
        }
    }
}
