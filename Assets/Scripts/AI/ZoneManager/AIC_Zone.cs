﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_Zone {

    ArrayList myMinions;
    ArrayList myRemnants;
    ArrayList myStructures;
    public AIC_Zone[] myNeighbors;
    public AIC_Zone[] myDistantNeighbors;
    bool _enabled = true;
    bool _structuresEnabled = true;
    GameObject indicator;
    public int x;
    public int y;
    bool debug = false;
    static Material testMat;
    static Material defaultMat;

    public bool enabled
    {
        get { return _enabled; }
        set
        {
            setEnabled(value);
        }
    }
    public bool structureEnabled
    {
        get { return _structuresEnabled; }
        set
        {
            setEnabled(value, false, true);
        }
    }

    public AIC_Zone(int x, int y)
    {
        myMinions = new ArrayList();
        myRemnants = new ArrayList();
        myStructures = new ArrayList();
        myNeighbors = new AIC_Zone[8]; //number of possible neighbors
        myDistantNeighbors = new AIC_Zone[4];
        this.x = x;
        this.y = y;
        if (testMat == null) testMat = Resources.Load<Material>("GhostMat");
    }

    public void addMinionToZone(GameObject minion)
    {
        myMinions.Add(minion);
    }

    public void removeMinionFromZone(GameObject minion)
    {
        myMinions.Remove(minion);
    }

    public void addRemnantToZone(GameObject remnant)
    {
        myRemnants.Add(remnant);
        setRemnantEnabled(remnant);
    }

    public void removeRemnantFromZone(GameObject remnant)
    {
        myRemnants.Remove(remnant);
    }

    public void addStructureToZone(GameObject structure)
    {
        myStructures.Add(structure);
        setStructureEnabled(structure);
    }

    public void removeStructureFromZone(GameObject structure)
    {
        myStructures.Remove(structure);
    }

    public void disableFirstFrame()
    {
        _enabled = false;
        int minionCount = myMinions.Count;
        for (int i = 0; i < minionCount; ++i)
        {
            GameObject minion = myMinions[i] as GameObject;
            minion.SetActive(false);
        }
        int remnantCount = myRemnants.Count;
        for (int i = 0; i < remnantCount; ++i)
        {
            GameObject remnant = myRemnants[i] as GameObject;
            setRemnantEnabled(remnant);
        }
        int structureCount = myStructures.Count;
        for (int i = 0; i < structureCount; ++i)
        {
            GameObject structure = myStructures[i] as GameObject;
            if (structure)
                setStructureEnabled(structure);
        }
    }

    public void setEnabled(bool enable, bool mainZone = false, bool structuresOnly = false)
    {
        if (debug)
        {
            if (enable == true)
            {
                if (indicator == null)
                {
                    indicator = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    indicator.transform.position = new Vector3((x * AIC_ZoneManager.cellSize) - (AIC_ZoneManager.cellSize / 2), 2 * AIC_ZoneManager.cellSize, (y * AIC_ZoneManager.cellSize) - (AIC_ZoneManager.cellSize / 2));
                    indicator.transform.localScale = new Vector3(AIC_ZoneManager.cellSize, AIC_ZoneManager.cellSize, AIC_ZoneManager.cellSize);
                    indicator.GetComponent<Collider>().enabled = false;
                    Renderer rend = indicator.GetComponent<Renderer>();
                    if (defaultMat == null)
                        defaultMat = rend.material;
                    if (mainZone)
                    {
                        rend.material = testMat;
                        Debug.Log("Set Zone " + x + ", " + y + " as main zone");
                    }
                    else
                        rend.material = defaultMat;
                }
                else
                {
                    Renderer rend = indicator.GetComponent<Renderer>();
                    if (defaultMat == null)
                        defaultMat = rend.material;
                    if (mainZone)
                    {
                        rend.material = testMat;
                        Debug.Log("Set Zone " + x + ", " + y + " as main zone");
                    }
                    else
                        rend.material = defaultMat;
                }
            }
            else
            {
                if (indicator != null) GameObject.Destroy(indicator);
            }
        }
        if (_enabled == enable && (!structuresOnly || _structuresEnabled == enable))
            return;
        if (!structuresOnly)
        {
            _enabled = enable;

            int minionCount = myMinions.Count;
            for (int i = 0; i < minionCount; ++i)
            {
                GameObject minion = myMinions[i] as GameObject;
                setMinionEnabled(minion);
            }
            int remnantCount = myRemnants.Count;
            for (int i = 0; i < remnantCount; ++i)
            {
                GameObject remnant = myRemnants[i] as GameObject;
                setRemnantEnabled(remnant);
            }
        }

        _structuresEnabled = enable;
        int structuresCount = myStructures.Count;
        for (int i = 0; i < structuresCount; ++i)
        {
            GameObject structure = myStructures[i] as GameObject;
            if (structure)
                setStructureEnabled(structure);
        }
    }

    public void setStructureEnabled(GameObject structure)
    {
        ENT_Structure structScript = structure.GetComponent<ENT_Structure>();
        if (_structuresEnabled)
        {
            structure.SetActive(true);
            Collider[] cols = structure.GetComponentsInChildren<Collider>();
            int colCount = cols.Length;
            for (int i = 0; i < colCount; ++i)
            {
                Collider col = cols[i];
                col.enabled = false;
                col.enabled = true;
            }
        }
        else
        {
            structure.SetActive(false);
        }
        structScript.OnZoneStatusChange(_structuresEnabled);
    }

    public void setMinionEnabled (GameObject minion)
    {
        //Debug.Log("setting status of " + minion.name + " to " + _enabled);
        //Debug.Log(_enabled + " " + minion.name);
        ENT_Body body = minion.GetComponent<ENT_Body>();
        if (_enabled)
        {
            minion.SetActive(true);
            body.SetPSEnabled(false);
            Collider col = minion.GetComponent<Collider>();
            col.enabled = false;
            col.enabled = true;
            Animator anim = minion.GetComponentInChildren<Animator>();
            anim.SetBool("isAppear", true);
        }
        else
        {
            if (body.inFinalWavesBattle) return; // do not turn off minions in the final wave battle

            Animator anim = minion.GetComponentInChildren<Animator>();
            if (anim != null && anim.isInitialized)
            {
                anim.SetBool("isAppear", false);
                body.disableAI = true;
                body.reappear = false;
            }
        }
        body.OnZoneStatusChange(_enabled);
    }

    public void setRemnantEnabled(GameObject remnant)
    {
        bool setFullGrown = false;
        if (_enabled && !remnant.activeInHierarchy)
            setFullGrown = true;
        remnant.SetActive(_enabled);
        if (setFullGrown)
            remnant.GetComponent<ENT_Remnant>().setFullyGrown();
    }

    public bool isNeighbor(AIC_Zone zone, bool includeDistantNeighbors = true)
    {
        int neighborCount = myNeighbors.Length;
        for (int i = 0; i < neighborCount; ++i)
        {
            AIC_Zone z = myNeighbors[i];
            if (z == zone)
                return true;
        }
        if (includeDistantNeighbors) return isDistantNeighbor(zone);
        else return false;
    }

    public bool isDistantNeighbor(AIC_Zone zone)
    {
        int distNeighborCount = myDistantNeighbors.Length;
        for (int i = 0; i < distNeighborCount; ++i)
        {
            AIC_Zone z = myDistantNeighbors[i];
            if (z == zone)
                return true;
        }
        return false;
    }
}
