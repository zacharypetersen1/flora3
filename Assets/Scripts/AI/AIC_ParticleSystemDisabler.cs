﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_ParticleSystemDisabler : MonoBehaviour
{

    public PLY_AllyManager am;
    public float activeRadius = 30; //structure particle systems are active within this radius
    
    private float radiusSqr;
    private int frameCounter = 0;

    //private int debugCounter = 0;
    
    private void Start()
    {
        am = GameObject.FindWithTag("Player").GetComponent<PLY_AllyManager>();
        radiusSqr = activeRadius * activeRadius;
    }

    private void UpdateEnemies()
    {
        List<GameObject> allEnemies = am.getAllEnemies;
        int enemyCount = allEnemies.Count;
        GameObject enemy;
        for (int i = 0; i < enemyCount; ++i)
        {
            enemy = allEnemies[i];
            if (!enemy.activeInHierarchy)
                continue;

            UpdateParticleSystems(enemy);
        }
    }

    private void UpdateAllies()
    {
        List<GameObject> allAllies = am.getAllAllies;
        int allyCount = allAllies.Count;
        GameObject ally;
        for (int i = 0; i < allyCount; ++i)
        {
            ally = allAllies[i];
            //allies are never disabled
            UpdateParticleSystems(ally);
        }
    }
    
    private void Update()
    {
        //run every 30 frames
        if (++frameCounter < 30)
        {
            return;
        }
        frameCounter = 0;
        //debugCounter = 0;

        UpdateEnemies();

        UpdateAllies();
        //Debug.Log("Minions enabled: " + debugCounter);
    }
    
    private void UpdateParticleSystems(GameObject obj)
    {
        float sqrDist = UTL_Math.sqrDistance(obj.transform.position, gameObject.transform.position);
        bool psEnabled = sqrDist < radiusSqr;

        //if (psEnabled)
        //    ++debugCounter;
    
        ENT_Body structureScript = obj.GetComponent<ENT_Body>();
        structureScript.SetPSEnabled(psEnabled);
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.tag == "Minion")
    //    {
    //        ENT_Body body = other.gameObject.GetComponent<ENT_Body>();
    //        if (body != null)
    //        {
    //            body.SetPSEnabled(true);
    //        }
    //    }
    //}
    //
    //void OnTriggerExit(Collider other)
    //{
    //    if (other.tag == "Minion")
    //    {
    //        ENT_Body body = other.gameObject.GetComponent<ENT_Body>();
    //        if (body != null)
    //        {
    //            body.SetPSEnabled(false);
    //        }
    //    }
    //}
}
