﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIG_Group : MonoBehaviour {

    //working is default state, UnderAttack is set when twig gets close enough
    public enum groupState { Undef, Wandering, Gathering, Depositing, UnderAttack}

    //array of allowed states. 
    protected List<groupState> AllowedStates =
        new List<groupState>() { groupState.Undef, groupState.Wandering, groupState.Gathering, groupState.Depositing, groupState.UnderAttack };

    public List<GameObject> minionsInGroup;
    public groupState currState;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void setState(groupState ns)
    {
        if (!AllowedStates.Contains(ns)) return;
        currState = ns;
        //still need to set state of all minions in MinionsInGroup
    }
}
