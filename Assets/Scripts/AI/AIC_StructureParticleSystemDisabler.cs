﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_StructureParticleSystemDisabler : MonoBehaviour {

    //as it is written this is actually way slower than colliders. We need some sort of spatial hash for this to be reasonable
    //time to make a k-d tree?

    public PLY_AllyManager am;
    public float activeRadius = 150; //structure particle systems are active within this radius
    
    private float radiusSqr;

    private int frameCounter = 15;
    
    private void Start()
    {
        am = GameObject.FindWithTag("Player").GetComponent<PLY_AllyManager>();
        radiusSqr = activeRadius * activeRadius;
    }

    private void Update()
    {
        //run every 30 frames
        if (++frameCounter < 30)
        {
            return;
        }
        frameCounter = 0;

        List<GameObject> allStructures = am.getAllStructures;
        int count = allStructures.Count;
        GameObject structure;
        for (int i = 0; i < count; ++i)
        {
            structure = allStructures[i];
            //should really figure this out...
            if (!structure)
                continue;
            //could maybe be break
            if (!structure.activeInHierarchy)
                continue;
            UpdateParticleSystems(structure);
        }
    }
    
    private void UpdateParticleSystems(GameObject structure)
    {
        float sqrDist = UTL_Math.sqrDistance(structure.transform.position, gameObject.transform.position);
        bool psEnabled = sqrDist < radiusSqr;
    
        ENT_Structure structureScript = structure.GetComponent<ENT_Structure>();
        structureScript.SetPSEnabled(psEnabled);
    }


    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.tag == "enemyStructure")
    //    {
    //        ENT_Structure structure = other.gameObject.transform.root.GetComponent<ENT_Structure>();
    //        if (structure != null) structure.SetPSEnabled(true);
    //        /*else
    //        {
    //            print("structure is null for " + other.gameObject);
    //        }*/
    //    }
    //}
    //
    //void OnTriggerExit(Collider other)
    //{
    //    if (other.tag == "enemyStructure")
    //    {
    //        ENT_Structure structure = other.gameObject.transform.root.GetComponent<ENT_Structure>();
    //        if (structure != null) structure.SetPSEnabled(false);
    //        /*else
    //        {
    //            print("structure is null for " + other.gameObject);
    //        }*/
    //    }
    //}
}
