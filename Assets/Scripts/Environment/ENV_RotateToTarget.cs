﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_RotateToTarget : MonoBehaviour {

    public Vector3 addRot = Vector3.one * 50;
    public float val = 0;
    public float targetVal = 0;
    public float incriment = 0.1f;
    private Vector3 startRot;



	// Use this for initialization
	void Start () {
        startRot = transform.localEulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
        if(Mathf.Abs(targetVal - val) <= incriment * Time.deltaTime)
        {
            val = targetVal;
        }
        else
        {
            val += Mathf.Sign(targetVal - val) * incriment * Time.deltaTime;
        }
        transform.localEulerAngles = startRot;
        transform.Rotate(addRot * val, Space.Self);
	}


}
