﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_Crystal : MonoBehaviour {

    public float rotationSpeed = 1F;

    //when you destroy a crystal you should remove it from ENV_CrystalSpawner.crystalList

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Rotate(Vector3.up * rotationSpeed);
	}

    public static void attachCrystal(GameObject carrying)
    {
        GameObject newCrystal = UTL_Resources.cloneAtLocation("Environment Effects/purify", carrying.transform.position);
        newCrystal.transform.SetParent(carrying.transform);
        newCrystal.transform.position = new Vector3(newCrystal.transform.position.x, newCrystal.transform.position.y, newCrystal.transform.position.z - 5);
    }
}
