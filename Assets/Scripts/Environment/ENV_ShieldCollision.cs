﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_ShieldCollision : MonoBehaviour {

    GameObject rings;
    GameObject hit;
    string State = "active";

    public GameObject Rings
    {
        get { return rings; }
        set { rings = value; }
    }

    public GameObject Hit
    {
        get { return hit; }
        set { hit = value; }
    }

    private void Update()
    {
        if(hit != null)
        {
            if (!hit.GetComponent<ParticleSystem>().IsAlive())
            {
                Destroy(hit);
            }
        }

        if (rings != null)
        {
            if (!rings.GetComponent<ParticleSystem>().IsAlive())
            {
                Destroy(rings);
            }
        }

    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "basicAttack")
        {
        }
        else
        {
            Physics2D.IgnoreLayerCollision(0, 9);
        }      
    }

    
    private void OnCollisionStay(Collision collision)
    {
        Physics2D.IgnoreLayerCollision(0, 9);
    }

    

    //
    //Detect attack collision
    // 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "basicAttack")
        {
            /*
            RaycastHit2D contact_point = Physics2D.Raycast(transform.position, -transform.up);
            if (contact_point)
            {
                GameObject rings = UTL_GameObject.cloneAtLocation("Environment Effects/Shield/Collision Rings", contact_point.point);
                GameObject hit = UTL_GameObject.cloneAtLocation("Environment Effects/Hit", contact_point.point);
                hit.transform.GetChild(1).gameObject.active = false;
                rings.transform.position = UTL_Math.vec2ToVec3(contact_point.point,-2);
                hit.transform.position = UTL_Math.vec2ToVec3(contact_point.point, -2);
            }
            */
        }
        else
        {
            //Physics2D.IgnoreLayerCollision(0, 9);
        }
    }
   

    private void Start()
    {

    }
}
