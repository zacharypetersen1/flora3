﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_SlowObj : MonoBehaviour {
    public float DC = 0.75f;
    public float lifetime = 15F;
    float lifeTimer;
    float startGrowthTimer = 0f;
    float timer;
    public float distToGrow = 3;
    public bool purity;
    bool growing;
    string[] affectsTag = new string[2] { "Minion", "Player"};
    ParticleSystem[] effects;
    bool alreadyOn = false;
    Vector3 tempVec = new Vector3(0f, 0f, 0f); //to save on garbage generated each frame

    public GameObject lightningEffect;

    void Awake()
    {
        startGrowthTimer = Random.Range(.5f, .8f);
        timer = startGrowthTimer;
        lifeTimer = lifetime;
        growing = true;
        effects = GetComponentsInChildren<ParticleSystem>();
        transform.localScale = new Vector3(Random.Range(.6f, 1f), 1, Random.Range(.6f, 1f));
    }
	// Use this for initialization
	void Start () {
        SetupLightningEffect();
        
	}

    void SetupLightningEffect()
    {
        if (purity)
        {
            foreach (ParticleSystem effect in effects)
            {
                effect.gameObject.SetActive(false);
            }
        }
        else
        {
            foreach (ParticleSystem effect in effects)
            {
                effect.Stop();
            }
        }
    }

    void StartLightningEffect()
    {
        foreach (ParticleSystem effect in effects)
        {
            effect.Play();
        }
    }

    // Update is called once per frame
    void Update () {
        if (lifetime <= 0)
        {
            killYourself();
        }
        else lifetime -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
		if(timer> 0)
        {
            timer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            tempVec.x = transform.position.x;
            tempVec.z = transform.position.z;
            if(growing)
            {
                tempVec.y = transform.position.y + (distToGrow / startGrowthTimer) * TME_Manager.getDeltaTime(TME_Time.type.environment);
            }
            else
            {
                tempVec.y = transform.position.y - (distToGrow / startGrowthTimer) * TME_Manager.getDeltaTime(TME_Time.type.environment);
            }
            transform.position = tempVec;
        }
        else
        {
            if (!alreadyOn)
            {
                StartLightningEffect();
                alreadyOn = true;
            }
            if (!growing)
            {
                Destroy(gameObject);
            }
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        foreach(string tag in affectsTag)
        {
            if (collider.tag == tag)
            {
                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                if (purity != other.Purity)
                {
                    other.receive_combat_message(new CBI_CombatMessenger(DC, 0, 0, new List<CBI_CombatMessenger.status>() { CBI_CombatMessenger.status.slow }, transform.position));
                    //effectEntitiy(collider);
                    killYourself();
                }
                
            }
        }
        
    }

    void effectEntitiy(Collider collider)
    {
        GameObject obj = collider.gameObject;
        GameObject newEffect = UTL_Resources.cloneAsChild(lightningEffect, obj);
        PSMeshRendererUpdater meshUpdater = newEffect.GetComponent<PSMeshRendererUpdater>();
        meshUpdater.MeshObject = obj;
        meshUpdater.UpdateMeshEffect();
        newEffect.AddComponent<EFX_KillInXSeconds>();
    }

    public void killYourself()
    {
        if (growing)
        {
            growing = false;
            timer = startGrowthTimer;
        }
    }
}
