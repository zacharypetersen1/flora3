﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_FallingBridge : MonoBehaviour {

    [HideInInspector]
    public bool falling = false;
    [HideInInspector]
    public bool canFall = true;
    public ENT_Structure obstruction;
    public int rotationSpeed = 1; //Testing revealed that the value doesn't matter, only positive or negative for direction
    protected float newRotation = 0;
    protected Vector3 rotationAxis;

	void Start () {
        rotationAxis = transform.parent.forward;
    }
	
	// Update is called once per frame
	void Update () {
		if (falling == true)
        {
            transform.rotation = Quaternion.AngleAxis(newRotation, rotationAxis);
            newRotation += rotationSpeed;
        }
        else if (canFall)
        {
            if (obstruction.State == "cleansed")
            {
                falling = true;
            }
            else
            {
                falling = false;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        //print(other.tag);
        if(other.tag == "ground")
        {
            falling = false;
            canFall = false;
        }
    }
}
