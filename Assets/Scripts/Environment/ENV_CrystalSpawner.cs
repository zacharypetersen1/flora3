﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Harold

public class ENV_CrystalSpawner : MonoBehaviour {

    public float SecondsPerSpawnCycle = 5F; //how frequently to spawn crystals in seconds
    public int CrystalsSpawnedPerCycle = 1; //how many crystals to spawn per cycle
    public Texture2D decoratorMap; //defines where in the scene is corruption, rock, and grass
    public Color spawnTerrainColor; // set this to corruption color
    public GameObject crystalObj; //The crystal object this will spawn
    public bool useRandScale = true;
    public float minScale = 0.8f;
    public float maxScale = 1.5f;

    GameObject[] rock_polygons;
    GameObject[] dirt_polygons;
    GameObject[] structure_polygons;
    //GameObject[] decorator_polygons;

    //public int worldWidth= 200;
    //public int worldHeight = 200;

    public List<GameObject> crystalList;

    private float crystalSpawnCounter = 0F;
    private int crystalCount = 0;

	// Use this for initialization
	void Start () {
        //spawnTerrainColor.a = 255;
        rock_polygons = GameObject.FindGameObjectsWithTag("rockWall");
        dirt_polygons = GameObject.FindGameObjectsWithTag("dirtWall");
        structure_polygons = GameObject.FindGameObjectsWithTag("enemyStructure");
        //structure_polygons = GameObject.FindGameObjectsWithTag("decorator");
    }
	
	// Update is called once per frame
	void Update () {
        if (crystalSpawnCounter >= SecondsPerSpawnCycle)
        {
            spawnCrystals();
            crystalSpawnCounter = 0f;
            //print("spawnCycle");
        }
        crystalSpawnCounter += Time.deltaTime;
	}

    void spawnCrystals()
    {
        for (int i = 0; i < CrystalsSpawnedPerCycle; ++i)
        {
            GameObject newCrystal = spawnCrystal();
            if (newCrystal != null) crystalList.Add(newCrystal);
        }
    }

    GameObject spawnCrystal()
    {

        Vector2 location = generateVector2();
        GameObject temp = null;
        Vector2 pixelCoord = convertWorldToDecoratorMap(location);
        //if (decoratorMap.GetPixel((int)pixelCoord.x, (int)pixelCoord.y).Equals(spawnTerrainColor))
        if (true)
        {
            //print("spawned a crystal");
            temp = UTL_Resources.cloneAtLocation(crystalObj, location);
            temp.transform.position = location;
            temp.gameObject.tag = "crystal";
            temp.gameObject.name = "groundCrystal_" + crystalCount++;
            temp.transform.position = UTL_Dungeon.snapPositionToTerrain(temp.transform.position);
            temp.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)), Space.Self);//RandomRotation
            temp.GetComponent<ENV_GroundCrystal>().spawner = this;

            // apply scale if marked
            if (useRandScale)
            {
                temp.transform.localScale *= minScale + (maxScale - minScale) * Random.Range(0f, 1f);
            }
        }
        return temp;
    }

    private Vector2 convertWorldToDecoratorMap(Vector2 world)
    {
        Vector2 map = Vector2.zero;
        if (world.x >= 0) map.x = world.x;
        else map.x = world.x + UTL_Dungeon.max_x;
        if (world.y >= 0) map.y = world.y;
        else map.y = world.y + UTL_Dungeon.max_y;
        return map;
    }

    public Vector2 generateVector2()
    {
        int max_x = UTL_Dungeon.max_x;
        int max_y = UTL_Dungeon.max_y;
        int min_x = UTL_Dungeon.min_x;
        int min_y = UTL_Dungeon.min_y;
        Vector2 point;
        do
        {
            point = new Vector2(Random.Range(min_x, max_x), Random.Range(min_y, max_y));
        } while (checkPointinCollider(point) == true);
        return point;
    }

    public bool checkPointinCollider(Vector2 point)
    {
        foreach (GameObject poly in dirt_polygons)
        {
            if (poly.GetComponent<Collider2D>().OverlapPoint(point))
            {
                return true;
            }
        }
        foreach (GameObject poly in rock_polygons)
        {
            if (poly.GetComponent<Collider2D>().OverlapPoint(point))
            {
                return true;
            }
        }
        foreach (GameObject poly in structure_polygons)
        {
            if (poly.GetComponent<Collider2D>().OverlapPoint(point))
            {
                return true;
            }
        }
        if (UTL_Dungeon.pointWithinTerrain(transform.position)) return true;
        /*foreach (GameObject poly in decorator_polygons)
        {
            poly.GetComponent<Collider>().
            if (poly.GetComponent<Collider>().OverlapPoint(UTL_Math.vec2ToVec3(point, poly.transform.position.z)))
            {
                return true;
            }
        }*/
        return false;
    }

    public void destroyCrystal(GameObject crystal)
    {
        if (crystal.GetComponent<ENV_GroundCrystal>() == null)
        {
            print("destroy crystal can only be called with groundCrystal arguments, was passed " + crystal.gameObject.name + " instead");
            return;
        }
        if (crystalList.Contains(crystal))
            crystalList.Remove(crystal);
        Destroy(crystal);
    }

    //returns nearest crystal to point. use requirePathToCrystal to force only pathable crystals to be considered
    public GameObject getNearestCrystal(Vector3 point, bool requirePathToCrystal = true, bool onlyConsiderAvailableCrystals = true)
    {
        float distance = 1000000f;
        GameObject nearest = null;
        foreach (GameObject crystal in crystalList)
        {
            float tempDist = Vector3.Distance(point, crystal.transform.position);
            if (tempDist < distance)
            {
                if (requirePathToCrystal)
                {
                    /*if (PTH_Finder.findPath(UTL_Math.vec3ToVec2(point), UTL_Math.vec3ToVec2(crystal.transform.position)) == null)*/
                        continue;
                }
                if (onlyConsiderAvailableCrystals)
                {
                    if (!crystal.GetComponent<ENV_GroundCrystal>().canCollect())
                        continue;
                }
                distance = tempDist;
                nearest = crystal;
            }
        }
        return nearest;
    }

    /*
    public void populateScene()
    {
        GameObject[] decoratorArr = Resources.LoadAll<GameObject>(assetPath);

        for (int i = 0; i < numObjects; i++)
        {
            int x = Random.Range(0, 200);
            int y = Random.Range(0, 200);
            /*print(x);
            print(y);
            print(decoratorText.GetPixel(x, y));
            print(maskColor);
            print(decoratorText.GetPixel(x, y).Equals(maskColor));
            if (decoratorText.GetPixel(x, y).Equals(maskColor))
            {
                loadSingleDecorator(getRandObject(decoratorArr), x, y);
            }
        }
    }*/
}
