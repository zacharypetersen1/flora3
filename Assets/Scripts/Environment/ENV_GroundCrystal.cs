﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_GroundCrystal : MonoBehaviour {

    private int crystalsLeft = 3;
    private int minionsCollecting = 0;

    public ENV_CrystalSpawner spawner;

    public bool canCollect()
    {
        return minionsCollecting < crystalsLeft;
    }

    //use this to try to request a pickup, will return false if no more pickups available
    public bool requestCrystalPickup()
    {
        if (!canCollect()) return false;
        minionsCollecting++;
        return true;
    }

    public void releaseCrystalHold()
    {
        if (minionsCollecting >= 1) minionsCollecting--;
    }

    public void takeCrystal(GameObject minion)
    {
        //set minionbrain.carrying to true
        ENT_Body monsterBod = minion.GetComponent<ENT_Body>();
        if (monsterBod == null)
        {
            print("Error: only minions can carry crystals. non-minion object " + minion.name + " is trying anyway (nullreferenceexception)");
            return;
        }
        monsterBod.carrying = true;
        crystalsLeft--;
        minionsCollecting--;
        ENV_Crystal.attachCrystal(minion);
        if (crystalsLeft <= 0) spawner.destroyCrystal(minion);
        //subtract one from crystals left
        //make the spinning crystal float above the minion
        //if crystals left <= 0 destroy this crystal
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
