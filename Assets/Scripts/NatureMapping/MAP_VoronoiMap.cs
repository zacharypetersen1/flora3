﻿// Zach

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MAP_VoronoiMap {

    private int density;                    // number of seeds used to create voronoi cells
    private int qDivisions;                 // number of divisions on x and y for voronoi quadrants, total # quadrants = qDivisions^2
    private VoronoiQuadrant[,] quadrants;   // 2D array of all voronoi quadrants
    private NOI_PerlinNoise noise = new NOI_PerlinNoise(3, 1); // for generating tile types



    //
    // a single point that defines a voronoi cell
    //
    class VoronoiSeed
    {
        public float x;
        public float y;
        public MAP_NatureData.type type;

        // constructor
        public VoronoiSeed(float sX, float sY, MAP_NatureData.type sT)
        {
            x = sX;
            y = sY;
            type = sT;
        }
    }



    //
    // contains all voroni seeds within a smaller square area from the entire voronoi map
    //
    class VoronoiQuadrant
    {
        public List<VoronoiSeed> seeds;

        // constructor
        public VoronoiQuadrant()
        {
            seeds = new List<VoronoiSeed>();
        }

        // adds seed to this quadrant's list
        public void addSeed(float x, float y, MAP_NatureData.type t)
        {
            seeds.Add(new VoronoiSeed(x, y, t));
        }
    }


    //
    // constructor, will generate voronoi map when called
    //
    public MAP_VoronoiMap(int setDensity, int setQDivisions)
    {
        if (setQDivisions < 4)
        {
            throw new UnityException("Voronoi Quadrant division count:" + setQDivisions + " is too small to have effect, must be > 3");
        }
        density = setDensity;
        qDivisions = setQDivisions;
        generate();
    }



    //
    // generates voronoi map
    //
    public void generate()
    {
        initQuadrants();

        for(var i = 0; i < density; i++)
        {
            generateSeed();
        }
    }



    //
    // intializes voronoi quadrant algorithm
    //
    private void initQuadrants()
    {
        quadrants = new VoronoiQuadrant[qDivisions, qDivisions];
        for (int x = 0; x < qDivisions; x++)
        {
            for (int y = 0; y < qDivisions; y++)
            {
                quadrants[x, y] = new VoronoiQuadrant();
            }
        }
    }



    //
    // returns quadrant in which point is located
    //
    private VoronoiQuadrant getQuadrant(float x, float y)
    {
        return quadrants[(int)Mathf.Floor(x * qDivisions) , (int)Mathf.Floor(y * qDivisions)];
    }



    //
    // generates single voronoi seed and adds it to correct voronoi quadrant
    //
    private void generateSeed()
    {
        // generate two value between [0, 1), make sure they are exclusive of 1
        float seed_x = UnityEngine.Random.Range(0f, 1f);
        if (seed_x == 1f)
            seed_x -= Single.Epsilon;
        float seed_y = UnityEngine.Random.Range(0f, 1f);
        if (seed_y == 1f)
            seed_y -= Single.Epsilon;

        // determine seedType add seed to correct quadrant
        MAP_NatureData.type t = getRandomMapType(seed_x, seed_y);
        getQuadrant(seed_x, seed_y).addSeed(seed_x, seed_y, t);
    }



    //
    // returns list of quadrants that are nearby to point. Range determines number of quadrant rings around central quadrant
    //
    private List<VoronoiQuadrant> getNearbyQuadrants(float x, float y, int range)
    {
        List<VoronoiQuadrant> qList = new List<VoronoiQuadrant>();

        int qx = (int)Mathf.Floor(x * qDivisions);
        int qy = (int)Mathf.Floor(y * qDivisions);

        // add central quadrant
        qList.Add(quadrants[qx, qy]);

        for (var i = 1; i <= range; i++)
        {

            // try adding row above and below
            for (int ix = -i; ix <= i; ix++)
            {
                tryAddGetNearby(qList, qx + ix, qy + i);
                tryAddGetNearby(qList, qx + ix, qy - i);
            }

            // try adding row to left and right
            for (int iy = -i + 1; iy <= i - 1; iy++)
            {
                tryAddGetNearby(qList, qx + i, qy + iy);
                tryAddGetNearby(qList, qx - i, qy + iy);
            }

        }

        return qList;
    }



    //
    // Attempts to add voronoi quadrant at specified index to list, does nothing if quarant at indeces D/N exist
    //
    private void tryAddGetNearby(List<VoronoiQuadrant> list, int ix, int iy)
    {
        if (ix < 0 || ix >= qDivisions || iy < 0 || iy >= qDivisions)
        {
            return;
        }
        list.Add(quadrants[ix, iy]);
    }



    //
    // Returns voronoi seed nearest to point
    //
    private VoronoiSeed getNearestSeed(float x, float y)
    {
        List<VoronoiQuadrant> qList = getNearbyQuadrants(x, y, 1);

        VoronoiSeed selectedSeed = null;
        float smallestPriority = Mathf.Infinity;

        foreach (var quadrant in qList)
        {
            foreach(var seed in quadrant.seeds)
            {
                float priority = euclideanPriority(x, y, seed);

                if (priority < smallestPriority)
                {
                    selectedSeed = seed;
                    smallestPriority = priority;
                }
            }
        }

        return selectedSeed;
    }



    //
    // Returns priority queue of all nearby voronoi seeds
    //
    private PriorityQueue<VoronoiSeed> getNearestSeeds(float x, float y)
    {
        // get quadrants and init priority queue
        List<VoronoiQuadrant> qList = getNearbyQuadrants(x, y, 1);
        PriorityQueue<VoronoiSeed> pq = new PriorityQueue<VoronoiSeed>();

        // loop through each quadrant and add voronoi seeds
        foreach (var quadrant in qList)
        {
            foreach (var seed in quadrant.seeds)
            {
                pq.insert(seed, euclideanPriority(x, y, seed));
            }
        }
        
        return pq;
    }



    //
    // returns voronoi type of nearest seed
    //
    public MAP_NatureData.type getVoronoiType(float x, float y)
    {
        return getNearestSeed(x, y).type;
    }
    public MAP_NatureData.type getVoronoiType(Vector2 vec)
    {
        return getNearestSeed(vec.x, vec.y).type;
    }



    //
    // returns voronoi type based on Devon's smoothing algorithm
    //
    public MAP_NatureData.type getVoronoiTypeSmooth(float x, float y, int releventSeeds)
    {

        // get priority queue of nearby seeds and 
        PriorityQueue<VoronoiSeed> pq = getNearestSeeds(x, y);
        Dictionary<MAP_NatureData.type, float> typeCount = new Dictionary<MAP_NatureData.type, float>
        {
            {MAP_NatureData.type.corrupt, 0 },
            {MAP_NatureData.type.grass, 0 },
            {MAP_NatureData.type.rock, 0 },
        };

        for(int i = 0; i < releventSeeds; i++)
        {
            Debug.Log(pq.peek().type);
            typeCount[pq.peek().type] -= pq.topPriority();
            pq.pop();
        }

        // chosenType defaults to grass
        MAP_NatureData.type chosenType = MAP_NatureData.type.grass;
        float minValue = float.MaxValue;

        // choose type with smallest value
        foreach (var element in typeCount)
        {
            if (element.Value < minValue)
            {
                chosenType = element.Key;
                minValue = element.Value;
            }
        }

        return chosenType;
    }



    //
    // Returns priority rating based on distance between locations
    //
    private float euclideanPriority(float x, float y, VoronoiSeed s)
    {
        return (x - s.x) * (x - s.x) + (y - s.y) * (y - s.y);
    }



    //
    // Returns random map type
    //
    private MAP_NatureData.type getRandomMapType(float x, float y)
    {
        int r = (int)(noise.getValue(x, y) * 3 - Single.Epsilon);
        return (MAP_NatureData.type) r;
    }
}
