﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAP_Terrain : MonoBehaviour {

    public int surfaceIndex = 0;

    private Terrain terrain;
    private TerrainData terrainData;
    private Vector3 terrainPos;
    private static MAP_Terrain singleton;

    // Use this for initialization
    void Start()
    {
        singleton = this;
        terrain = Terrain.activeTerrain;
        terrainData = terrain.terrainData;
        terrainPos = terrain.transform.position;
        terrain.basemapDistance = 20000;
    }

    // Update is called once per frame
    void Update()
    {
        surfaceIndex = GetMainTexture(transform.position);
    }

    private float[] GetTextureMix(Vector3 WorldPos)
    {
        // returns an array containing the relative mix of textures
        // on the main terrain at this world position.

        // The number of values in the array will equal the number
        // of textures added to the terrain.

        // calculate which splat map cell the worldPos falls within (ignoring y)
        int mapX = (int)(((WorldPos.x - terrainPos.x) / terrainData.size.x) * terrainData.alphamapWidth);
        int mapZ = (int)(((WorldPos.z - terrainPos.z) / terrainData.size.z) * terrainData.alphamapHeight);

        // get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
        float[,,] splatmapData = terrainData.GetAlphamaps(mapX, mapZ, 1, 1);

        // extract the 3D array data to a 1D array:
        float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];

        for (int n = 0; n < cellMix.Length; n++)
        {
            cellMix[n] = splatmapData[0, 0, n];
        }
        return cellMix;
    }

    public static int GetMainTexture(Vector3 WorldPos)
    {
        // returns the zero-based index of the most dominant texture
        // on the main terrain at this world position.
        float[] mix = singleton.GetTextureMix(WorldPos);

        float maxMix = 0;
        int maxIndex = 0;

        // loop through each mix value and find the maximum
        for (int n = 0; n < mix.Length; n++)
        {
            if (mix[n] > maxMix)
            {
                maxIndex = n;
                maxMix = mix[n];
            }
        }
        return maxIndex;
    }



    //
    // Gets the slope of the terrain at a location (in Radians)
    //
    public static float getTerrainSlopeAngle(Vector3 position)
    {
        Vector2 terrainPosition = UTL_Dungeon.worldToTerrainPosition(position);
        Vector3 terrainNormal = Terrain.activeTerrain.terrainData.GetInterpolatedNormal(terrainPosition.x, terrainPosition.y);
        return Mathf.Acos(Vector3.Dot(terrainNormal, Vector3.up));
    }



    //
    // Gets the terrain normal at a location
    //
    public static Vector3 getTerrainNormal(Vector3 position)
    {
        Vector2 terrainPosition = UTL_Dungeon.worldToTerrainPosition(position);
        return Terrain.activeTerrain.terrainData.GetInterpolatedNormal(terrainPosition.x, terrainPosition.y);
    }
}
