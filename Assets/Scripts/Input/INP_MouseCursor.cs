using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class INP_MouseCursor : MonoBehaviour
{
    Camera worldCamera;
    public static Vector2 position = new Vector2(Screen.width / 2, Screen.height / 2);
    Ray ray;
    RaycastHit[] hits;
    private Vector3 start_pos = new Vector3(0, 0, -100);
    float speed = 1; //how fast the object should rotate
    public Vector3 targetPos;

    void Start()
    {
        worldCamera = GameObject.Find("Camera").GetComponent<Camera>();
    }

    void Update()
    {

        if (SelectionManager.isInSelectMode)
        {
            position += INP_PlayerInput.get2DAxis("Camera", false) * speed;
            position.x = Mathf.Clamp(position.x, 0, (float)Screen.width);
            position.y = Mathf.Clamp(position.y, 0, (float)Screen.height);
            Vector3 mouse = Input.mousePosition;
            mouse.z = 1;
            transform.position = Camera.main.ScreenToWorldPoint(UTL_Math.vec2ToVec3(position, 1f));
            transform.LookAt(Camera.main.transform.position);
            transform.Rotate(Vector3.left * 270, Space.Self);
            checkTarget();
        }
        else
        {
            transform.position = start_pos;
        }
    }

    void checkTarget()
    {
        ray = worldCamera.ScreenPointToRay(INP_MouseCursor.position);
        //if (Physics.Raycast(ray, out hit, maxDistance, layermask))
        hits = Physics.SphereCastAll(ray, 1, UIX_Crosshair.maxDistance, UIX_Crosshair.layermask);
        if (hits.Length > 0)
        {
            foreach (var hit in hits)
            {
                if (hit.transform.tag == "ground")
                {
                    targetPos = hit.point;
                }
            }

        }
    }
}
 