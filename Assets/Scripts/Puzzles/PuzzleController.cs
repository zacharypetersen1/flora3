﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleController : MonoBehaviour {

    public puzzleA_tile tZero;
    public FakePlate fZero;
    public puzzleA_tile tOne;
    public FakePlate fOne;
    public ENV_RotateToTarget rot1;
    private bool complete = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //if any of the fake plates are active, the crystals shut down
        if (fOne.active == true || fZero.active == true) {
            tZero.active = false;
            tOne.active = false;
            rot1.targetVal = 0;
        }
        if (tZero.active == true && tOne.active == true ) {
            complete = true;
            rot1.targetVal = 1;
            //Debug.Log(complete);
        }
	}
}
