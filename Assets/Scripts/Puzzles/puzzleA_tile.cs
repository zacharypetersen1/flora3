﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzleA_tile : MonoBehaviour {


    public int max_count;
    private int count;
    public int pieceLabel;
    public bool active = false; 
    Vector3 plate;
    Vector3 new_pos_on;
    public GameObject crystal;
    //public SpriteRenderer rend;

    // Use this for initialization
    void Start()
    {
        plate = transform.position;
        new_pos_on = new Vector3(plate.x, plate.y - .5f, plate.z);
        crystal.SetActive(false);
        //rend.color = Color.gray;
    }

    // Update is called once per frame
    void Update()
    {
        if (active) crystal.SetActive(true);
        else crystal.SetActive(false);
        if (count >= max_count)//Will move the plate down if activated
        {
            active = true;
            transform.position = Vector3.Slerp(new_pos_on, plate, .15f);
            //rend.color = Color.blue;

        }
        else if (count < 1)//Will move plate to original pos if not activated
        {
            active = false;
            transform.position = Vector3.Slerp(plate, new_pos_on, .15f);
            
            //rend.color = Color.gray;
        }
    }

    void FixedUpdate()
    {
        count = 0;
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "Minion")
        {
            if (collider.GetComponent<ENT_Body>().isAlly)
            {
                count += 1;
            }

        }

        if (collider.tag == "Player")
        {
            count += 1;
        }
    }
}

