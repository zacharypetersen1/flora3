﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TUT_TutorialPausedPopup : MonoBehaviour {

    public TUT_Messages.pauseMessages tutorialMessage;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            TUT_PauseMessageManager.ShowMessage(tutorialMessage);
            Destroy(gameObject);
        }
    }
}
