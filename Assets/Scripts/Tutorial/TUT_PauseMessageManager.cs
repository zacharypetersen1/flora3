﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TUT_PauseMessageManager : MonoBehaviour{
    public static Image pauseMessageBackground;
    public static TextMeshProUGUI titleText;
    public static TextMeshProUGUI pausedText1;
    public static Image pausedImage;
    public static TextMeshProUGUI pausedText2;
    public static GameObject confirmButton;

    public static float speedScalar = 4f;
    public static float fadeInSpeedupFactor = 2.5f;

    private static Color tempColor = new Color(1f, 1f, 1f, 1f);
    private static float alpha = 0f;
    private static TUT_Messages.pauseMessages currMessage = TUT_Messages.pauseMessages.None;
    public static bool isMessagePlaying
    {
        get { return currMessage != TUT_Messages.pauseMessages.None; }
    }

    private static List<TUT_Messages.pauseMessages> messagesToSkip = new List<TUT_Messages.pauseMessages>();

    public static void SkipMessage(TUT_Messages.pauseMessages toSkip)
    {
        messagesToSkip.Add(toSkip);
    }
    

    private void Start()
    {
        pauseMessageBackground = GameObject.Find("PausedTutorialMessage").GetComponent<Image>();
        pausedImage = GameObject.Find("TutorialImage").GetComponent<Image>();
        titleText = GameObject.Find("TutorialName").GetComponent<TextMeshProUGUI>();
        pausedText1 = GameObject.Find("TutorialText1").GetComponent<TextMeshProUGUI>();
        pausedText2 = GameObject.Find("TutorialText2").GetComponent<TextMeshProUGUI>();
        confirmButton = GameObject.Find("ConfirmButton");
        setAlpha(alpha);
    }

    private static void SetPaused(bool paused)
    {
        if (paused)
        {
            Time.timeScale = 0.00001f;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    public static void ShowMessage(TUT_Messages.pauseMessages pMessage)
    {
#if UNITY_EDITOR
        if (DBG_DebugOptions.Get().skipPauseTutorials)
            return;
#endif

        if (messagesToSkip.Contains(pMessage)) return; //skip messages displayed before saving game
        TUT_PauseMessage message = TUT_Messages.getPauseMessage(pMessage);
        if (message == null)
        {
            //EndMessage();
            return;
        }
        else
        {
            currMessage = pMessage;
            titleText.text = message.title;
            pausedText1.text = message.text1;
            if (message.text2 != null)
            {
                pausedText2.text = message.text2;
            }
            else
            {
                pausedText2.text = "";
            }
            if (message.image != null)
            {
                pausedImage.sprite = message.image;
                //print("should show image, message.image: " + message.image);
            }
            SaveLoad.pauseMessagesShown.Add(pMessage);
            setAlpha(1f);
            SetPaused(true);
        }
    }

    public void EndMessageButton()
    {
        TUT_PauseMessageManager.EndMessage();
    }

    public static void EndMessage()
    {
        currMessage = TUT_Messages.pauseMessages.None;
        setAlpha(0);
        SetPaused(false);
    }

    private static void setAlpha(float alpha)
    {
        tempColor = pauseMessageBackground.color;
        tempColor.a = alpha;
        pauseMessageBackground.color = tempColor;
        tempColor = titleText.color;
        tempColor.a = alpha;
        titleText.color = tempColor;
        tempColor = pausedText1.color;
        tempColor.a = alpha;
        pausedText1.color = tempColor;
        tempColor = pausedImage.color;
        tempColor.a = alpha;
        pausedImage.color = tempColor;
        tempColor = pausedText2.color;
        tempColor.a = alpha;
        pausedText2.color = tempColor;
        confirmButton.SetActive(alpha > 0.9f); //hide confirm button if other stuff is invisible
    }
}
