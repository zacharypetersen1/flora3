﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TUT_TutorialPopup : MonoBehaviour {

    public static int numTimesFinalBattleReset = 0;

    public TUT_Messages.Messages tutorialMessage;
    public bool repeat = true;
    public bool finalBattleMessage = false;

    private bool popupShown = false;
    private int lastShownResetNum = 0;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag != "Player")
            return;

        if (finalBattleMessage)
        {
            if (lastShownResetNum > numTimesFinalBattleReset)
                return;

            lastShownResetNum = numTimesFinalBattleReset;
        }
        else if (!repeat && popupShown)
        {
            return;
        }

        TUT_TutorialManager.queueMessage(tutorialMessage);
        popupShown = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!repeat && popupShown && !finalBattleMessage)
            {
                Destroy(gameObject);
            }
        }
    }
}
