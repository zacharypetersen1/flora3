﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Contains message text and images for a pause message.
/// will display like messageText[0], images[0], messageText[1], images[1]...
/// </summary>

public class TUT_PauseMessage {
    public string title;
    public string text1;
    public string text2;
    public Sprite image;

    public TUT_PauseMessage(string title, string text1, Sprite image, string text2 = null)
    {
        this.title = title;
        this.text1 = text1;
        this.text2 = text2;
        this.image = image;
    }
}
