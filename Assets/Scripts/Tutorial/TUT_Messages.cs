﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TUT_Messages {

    #region redMessages
    public enum Messages {None, Hello, WASD, Jump, DoubleJump, FirstMinion, Q, GoTo, YellowGrass, Purification,
                          Structures, RedGrass, Shift, ShieldGenerator, CommandMode, CommandSelect, Splicer, Recipe, Abilties,
                          SurfJump, ConfusionMinion, Necrominion, Corruption, StoryPiece, FoundGrandGuardians,
                          SlowMinion, RightClick, FirstRuin, TreePower, FirstEnemy, RightClickOnEnemy, TryRezPower,
                          FlowerPower, FlowerPowerPart2, RforAbility, Tier1AbilityUsed, ShieldGeneratorBattle,
                          MushroomRuin, MushroomDesc, FlowerDesc, TreeDesc, EnemyDefeated, FirstCornerShield,
                          SightOfBigShield, SightOfBigShieldPart2, SightOfBigShieldPart3, SlowShrine, SlowRuins,
                          ConfusionShrine, ConfusionRuins, NecroShrine, NecroRuins, SecondCornerShield, FinalCornerShield,
                          SunnyPlains, CrystalMadness, CreepyArea, Furnace, Bridge, FinalBattle, ShoutForParents, GameWon, SeeParents, TurnAround};

    static Dictionary<Messages, string> messageDict = new Dictionary<Messages, string>()
    {
        {Messages.None, "" },
        {Messages.Hello, "Hi I'm Red, thanks for picking me up. Can you help me find my parents?..." },
        {Messages.WASD, "In case you've forgotten how to walk, you can use the mouse and WASD keys to move around." },
        {Messages.Jump, "Make sure not to step on the corruption! You can jump over it by pressing Spacebar" },
        {Messages.DoubleJump, "Press Spacebar twice to double jump." },
        {Messages.FirstMinion, "This is my friend Piney, he is really strong and can help us get past this crystal!" },
        {Messages.RightClick, "Right click on the crystal to ask Piney to attack it." },
        {Messages.FirstRuin, "This looks like one of the ancient Vineweaver ruins! Lets explore it and learn more about your powers." },
        {Messages.TreePower, "Woah!!!! This stone gave you the power to create more friendly Tree Flora!" },
        {Messages.FirstEnemy, "Uh oh, there are a bunch of corrupted flora up ahead! Our new friends should be able to protect us though." },
        {Messages.RightClickOnEnemy, "Your friendly Flora will follow and defend us, but you can also right click to command them." },
        {Messages.Q, "Press Q to command your Flora to follow you." },
        {Messages.GoTo, "Right click the ground to command your flora to travel to that position." },
        {Messages.YellowGrass, "Hey, you found some yellow grass! Standing in this will charge up your purification ability." },
        {Messages.Purification, "Hold left click to purify corrupted flora into allies. We're going to need more friends on our journey!" },
        {Messages.Structures, "Right click on an enemy structure to command your flora to attack it." },
        {Messages.RedGrass, "This forest is full of enemies! If you get hurt, you can heal yourself by standing in red grass." },
        {Messages.Shift, "Hold Shift to use your Vineweaver powers to move swiftly through the world." },
        {Messages.ShieldGenerator, "This crystal is shielded by some magical force! Follow the arc to find the shield generator." },
        {Messages.CommandMode, "Scroll the mouse out to enter Command Mode. Left click and drag in Command Mode to select allies." },
        {Messages.Splicer, "Splicers can combine two weaker flora into stronger ones with powerful abilites." },
        {Messages.Recipe, "Check nearby signs for the Splicer's recipe." },
        {Messages.Abilties, "Press the number key that matches the number above allied flora to activate their abilities." },
        {Messages.SurfJump, "While holding shift, press space to jump over large areas." },
        {Messages.Necrominion, "The powerful Necromushroom flora's ability temporarily turns nearby remnants into ghostly allies." },
        {Messages.ConfusionMinion, "The Bellflower flora's confusion ability causes enemies to wander aimlessly for some time." },
        {Messages.Corruption, "Watch out for the corrupted ground! It will injure you." },
        {Messages.StoryPiece, "It looks like you found a page out of someone's diary! Press M to read it." },
        {Messages.FoundGrandGuardians, "Hooray, you found my parents! It looks like they're trapped behind another shield though..." },
        {Messages.SlowMinion, "The Sundew flora can use its ability to lay sticky traps that hinder enemies." },
        {Messages.TryRezPower, "Try out your new power by left clicking near these tree Sprouts!" },
        {Messages.FlowerPower, "Awesome, another ruin! I bet this one will let you create Flower Flora!" },
        {Messages.FlowerPowerPart2, "Every ruin you find will let you have more friendly Flora at a time." },
        {Messages.RforAbility, "Every type of flora has a powerful ability, which you can use by looking at them and pressing R." },
        {Messages.Tier1AbilityUsed, "Tree, Flower, and Mushroom Flora all have to sacrifice their plant bodies to use their abilites." },
        {Messages.ShieldGeneratorBattle, "It looks like this corrupted tree is shielding that crystal! We need to destroy it!" },
        {Messages.MushroomRuin, "Alright! This ruin should give you the power to create friendly Mushroom Flora!" },
        {Messages.TreeDesc, "Trees are tough melee fighters that can take a beating and keep on moving." },
        {Messages.FlowerDesc, "Flowers are fragile ranged Flora. Try to keep them away from the enemies while they throw their razor petals." },
        {Messages.MushroomDesc, "Mushrooms have a powerful AOE spore cloud attack. They are good against groups of enemies." },
        {Messages.EnemyDefeated, "Corrupted Flora will turn into a Sprout when they are defeated. You can turn these into friends!" },
        {Messages.FirstCornerShield, "We found one of the shield generators! We'll need to destroy it to get to my parents." },
        {Messages.SightOfBigShield, "That island with the huge shield is where my parents are! We need to figure out how to save them!" },
        {Messages.SightOfBigShieldPart2, "It looks like there are three beams of light goin to the shield, I bet that's how the shield is powered!" },
        {Messages.SightOfBigShieldPart3, "Hey Twig, I'm sure if we get to the source of those beams of light we will figure out how to free my parents" },
        {Messages.SlowShrine, "This shrine means the Sundew Flora ruin should be nearby. Let's try to find it!"},
        {Messages.SlowRuins, "This must be the Sundew Flora ruin! It sure will be great to have some of those powerful Flora on our side" },
        {Messages.ConfusionShrine, "This is the shrine for the Bellflower Flora! It's ruin must be somewhere near here." },
        {Messages.ConfusionRuins, "You found the Bellflower ruins! Let's go in there and get you that new power!" },
        {Messages.NecroShrine, "This is the Necromushroom shrine, it's ruins can't be far away now!" },
        {Messages.NecroRuins, "This Necromushroom ruin sure is creepy, but at least we can have some of them on our side now!" },
        {Messages.SecondCornerShield, "This is the second shield generator! Once we destroy it, there will be just one left maintaining the shield!" },
        {Messages.FinalCornerShield, "Twig, this is the last shield generator! Let's destroy it quick and get to my parents!" },
        {Messages.SunnyPlains, "I used to love playing in this nice sunny field before all these evil crystals showed up..." },
        {Messages.CrystalMadness, "This area is covered in crystals! It's probably going to be full of enemies, so watch out!" },
        {Messages.CreepyArea, "Twig, I don't like this area, its creepy and everything is dead..." },
        {Messages.Furnace, "Oh no! This ancient tree has been heavily corrupted and has been turned into some sort of furnace to block our path." },
        {Messages.Bridge, "We're almost to my parents! They are on the island at the end of this bridge." },
        {Messages.FinalBattle, "Oh no! There's another furnace blocking us from my parents! Twig, you've got to save them!" },
        {Messages.ShoutForParents, "Don't worry Mommy and Daddy, my friend Twig is going to get you out!" },
        {Messages.GameWon, "You did it Twig! My family is safe! You're the best friend ever!" },
        {Messages.SeeParents, "Look there are my parents, thank you for bringing me home Twig!"},
        {Messages.TurnAround, "Oh no look out behind you"}
    };

    public static string getMessageText(Messages message)
    {
        return messageDict[message];
    }
    #endregion

    #region pauseMessages
    public enum pauseMessages { None, WASD_Mouse, Basic_Commands, Minion_Abilities, Advanced_Movement, Tier_Two, Command_Mode, Structures, Flora_Stones};

    static Dictionary<pauseMessages, TUT_PauseMessage> pauseMessageDict = new Dictionary<pauseMessages, TUT_PauseMessage>()
    {
        {pauseMessages.None, null},
        {pauseMessages.WASD_Mouse, new TUT_PauseMessage("Basic Controls", //title
            "Use WASD to move around the world. Move the mouse to control the camera.", //top text
            Resources.Load<Sprite>("UI/TutorialImages/StartAreaWASD"))}, //image
                    //optional bottom text
        {pauseMessages.Basic_Commands, new TUT_PauseMessage("Flora Commands",
            "Right click on corrupted minions and structures to command your Flora allies to attack them. You can also right click on the ground to command your allies to move to that location.",
            Resources.Load<Sprite>("UI/TutorialImages/ClickToMove_withMouse"),
            "Press Q to command your minions to follow you again. Following minions will battle nearby enemies but not structures.") },
        {pauseMessages.Minion_Abilities, new TUT_PauseMessage("Flora Abilities",
            "Every type of Flora has a different ability that can be activated by looking at them and pressing R when the ability indicator is above the head of the minion",
             Resources.Load<Sprite>("UI/TutorialImages/Ability1_withKey"),
            "Tree, Flower, and Mushroom Flora are consumed by their abilities, but don't worry! Their energy flows back into the living plants of the forest.") },
        {pauseMessages.Advanced_Movement, new TUT_PauseMessage("Advanced Movement",
            "You can jump by pressing Spacebar. Pressing it again while in the air will make you double jump!",
            Resources.Load<Sprite>("UI/TutorialImages/DoubleJump_withBar"),
            "You can also use your Vineweaver powers to move quickly through the world by holding down Shift.") },
        {pauseMessages.Tier_Two, new TUT_PauseMessage("Tier Two Flora",
            "You just unlocked your first Tier Two Flora! These plants are stronger than their mundane bretheren, and have special powers as well.",
            Resources.Load<Sprite>("UI/TutorialImages/tier2MinionsBuildVersion"),
            "You can only have at most three of each type of Tier Two Flora.") },
        {pauseMessages.Command_Mode, new TUT_PauseMessage("Command Mode",
            "Press E to switch to Command Mode. In Command Mode, orders are only issued to selected Flora. Click and drag the mouse over your allies to select them. Tab selects all allies",
             Resources.Load<Sprite>("UI/TutorialImages/CommandMode_withKey"),
            "You can also get into and out of Command Mode by scrolling the mouse wheel.") },
        {pauseMessages.Structures, new TUT_PauseMessage("Shield Generator",
            "Shield Generators are corrupted structures that project a shield over an area. If you see a shield, just follow the sheild beam trail to the shield's generator.",
             Resources.Load<Sprite>("UI/TutorialImages/ShieldGenerator_withArrow"),
            "You will need to destroy all of the Sheild Generators in the forest to be able to rescue Red's parents.") },
        {pauseMessages.Flora_Stones, new TUT_PauseMessage("Flora Stones",
            "Flora stones like this one are key to increasing your Vineweaver powers. Each one you find will allow you to ressurect another type of Flora!",
             Resources.Load<Sprite>("UI/TutorialImages/TreeSplicer_withArrow"),
            "Every Flora Stone you acquire will also increase your maximum amount of allied Flora by 5.") },

    };

    public static TUT_PauseMessage getPauseMessage(pauseMessages message)
    {
        return pauseMessageDict[message];
    }
    #endregion
}
