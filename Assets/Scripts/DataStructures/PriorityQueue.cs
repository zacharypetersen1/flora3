﻿//Noah

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PriorityQueue<T>
{
    private List<Pair> list;


    //
    // Pair Object with its Priority
    //
    public struct Pair
    {
        public T obj;
        public float priority;

        public Pair(T o, float p)
        {
            this.obj = o;
            this.priority = p;
        }

        public int CompareTo(Pair p)
        {
            if (this.priority < p.priority) { return -1; }
            else if (this.priority > p.priority) { return 1; }
            else { return 0; }
        }
    }



    public PriorityQueue()
    {
        this.list = new List<Pair>();
    }

    public bool isEmpty()
    {
        if (list.Count > 0) return false;
        return true;
    }

    public T peek()
    {
        if (isEmpty()) return default(T);
        return list[0].obj;
    }



    public void insert(T obj, float priority)
    {
        list.Add(new Pair(obj, priority));

        int childIndex = list.Count - 1;
        while (childIndex > 0)
        {
            int parentIndex = (childIndex - 1) / 2;
            if (list[childIndex].CompareTo(list[parentIndex]) >= 0) { break; }
            Pair temp = list[childIndex];
            list[childIndex] = list[parentIndex];
            list[parentIndex] = temp;
            childIndex = parentIndex;
        }
    }



    public void pop()
    {
        if (list.Count > 0)
        {
            int length = list.Count - 1;
            list[0] = list[length];
            list.RemoveAt(length);
            --length;

            int parentIndex = 0;
            while (true)
            {
                int childIndex = parentIndex * 2 + 1;
                if (childIndex > length) { break; }

                int rightChildIndex = childIndex + 1;

                if (rightChildIndex <= length &&
                    list[rightChildIndex].CompareTo(list[childIndex]) < 0)
                {
                    childIndex = rightChildIndex;
                }
                if (list[parentIndex].CompareTo(list[childIndex]) <= 0) { break; }

                Pair temp = list[parentIndex];
                list[parentIndex] = list[childIndex];
                list[childIndex] = temp;
                parentIndex = childIndex;
            }
            return;
        }
        return;
    }


    //
    // Clears the queue, made by Andrei
    //
    public void clear()
    {
        while (!isEmpty())
        {
            pop();
        }
    }



    //
    // Returns the priority of the top element in the queue
    //
    public float topPriority()
    {
        if (isEmpty()) throw new UnityException("Cannot check priority, no elements inserted in Priority Queue");
        return list[0].priority;
    }
}


