﻿// Ben & Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX_Minions : MonoBehaviour {

    public bool soundArmed = false;

    AIC_AllyGroup allyGroup;
    List<GameObject> followGroup;
    List<GameObject> selectedGroup;
    int groupCount;

    float minDelay;
    float delayRange;
    double delay;
    float delayEnd;
    List<float> callTimes;
    int currentCallTime = 0;
    bool soundsPrimed = false;
    int command;



	// Use this for initialization
	void Start () {
        allyGroup = GameObject.Find("Player").GetComponent<PLY_AllyManager>().followGroup;
        followGroup = allyGroup.AllyList;
        trimGroup();
        groupCount = allyGroup.getCount();
        setDelay();
        callTimes = new List<float>();
        //StartCoroutine(delayFunction());
	}
	
	// Update is called once per frame
	void Update () {
        followGroup = allyGroup.AllyList; //Harold added this line
        if (groupCount != allyGroup.getCount())
        {
            trimGroup();
            groupCount = allyGroup.getCount();
            setDelay();
        }

        playFollowSounds(followGroup);

        if (soundsPrimed)
        {
            if (currentCallTime >= selectedGroup.Count - 1)
            {
                //playMinionSounds(command, selectedGroup[currentCallTime]);
                soundsPrimed = false;
                currentCallTime = 0;
            }
            else if(currentCallTime == 0 || Time.time > callTimes[currentCallTime])
            {
                playMinionSounds(command, selectedGroup[currentCallTime]);
                currentCallTime++;
            }
        }
	}

    IEnumerator delayFunction()
    {
        yield return new WaitForSeconds(0.3f);
    }

    void trimGroup()
    {
        List<GameObject> removeList = new List<GameObject>();
        int followGroupCount = followGroup.Count;
        for (int i = 0; i < followGroupCount; ++i)
        {
            GameObject ally = followGroup[i];
            if (ally == null)
            {
                removeList.Add(ally);
                continue;
            }
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body == null || body.getMyType() == ENT_DungeonEntity.monsterTypes.treeMinion)
            {
                removeList.Add(ally);
            }
        }
        int removeCount = removeList.Count;
        for (int i = 0; i < removeCount; ++i)
        {
            GameObject ally = removeList[i];
            followGroup.Remove(ally);
        }
    }

    void setDelay()
    {
        minDelay = 5 / Mathf.Sqrt(followGroup.Count);
        delayRange = 2 / Mathf.Sqrt(followGroup.Count);
    }

    void playFollowSounds(List<GameObject> allyGroup)
    {
        if (followGroup.Count != 0)
        {
            if (!soundArmed)
            {
                System.Random random = new System.Random();
                delay = minDelay + random.NextDouble() * delayRange;
                delayEnd = Time.time + (float)delay;
                //Debug.Log("Set delay: " + delay);
                soundArmed = true;
            }
            if (Time.time > delayEnd && soundArmed)
            {
                //Debug.Log("Play sound");
                System.Random random = new System.Random();
                GameObject ally = allyGroup[random.Next(0, followGroup.Count)];
                switch (ally.GetComponent<ENT_Body>().getMyType())
                {
                    case ENT_DungeonEntity.monsterTypes.flowerMinion:
                        FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_flower_idle", ally.transform.position);
                        break;
                    case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                        FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_mushroom_idle", ally.transform.position);
                        break;
                    case ENT_DungeonEntity.monsterTypes.confusionMinion:
                        break;
                    case ENT_DungeonEntity.monsterTypes.necroMinion:
                        break;
                    default:
                        break;
                }
                //Debug.Log("Ally at " + minion.transform.position);
                soundArmed = false;
            }
        }
    }


    public void primeMinionSounds(int x, List<GameObject> allyGroup)
    {
        selectedGroup = allyGroup;
        command = x;
        //Shuffle ally list
        if (selectedGroup.Count != 0)
        {
            for (int i = 0; i < selectedGroup.Count; i++)
            {
                GameObject temp = selectedGroup[i];
                int randIndex = Random.Range(i, selectedGroup.Count);
                selectedGroup[i] = selectedGroup[randIndex];
                selectedGroup[randIndex] = temp;
            }
            /*
            if (x == 0) //GoTo
            {
            */
            callTimes = new List<float>(0);
            if (selectedGroup.Count > 1)
            {
                float timePrev = Time.time;
                for (int j = 1; j < selectedGroup.Count; j++)
                {
                    float smallDelay = (0.15f / Mathf.Sqrt(selectedGroup.Count)) + Random.Range(0, (0.03f / Mathf.Sqrt(selectedGroup.Count))) + timePrev;
                    callTimes.Add(smallDelay);
                    timePrev = smallDelay;
                    /*
                    float currentTime = Time.time;
                    while (currentTime < smallDelay)
                    {
                        //Debug.Log("Time" + currentTime);
                        delayFunction();
                        currentTime += 0.1f;
                    }
                    */
                }
            }
            soundsPrimed = true;
        }
        /*
        }
        else //Attack
        {
            for (int k = 0; k < selectedGroup.Count; k++)
            {
                float smallDelay = 0.05f + Random.Range(0, 0.1f) + Time.time;
                float currentTime = Time.time;
                //Debug.Log("smallDelay" + smallDelay);
                //Debug.Log("INITIAL Time" + currentTime);
                while (currentTime < smallDelay)
                {
                    //Debug.Log("Time" + currentTime);
                    delayFunction();
                    currentTime += 0.1f;
                }
                //Debug.Log("FINAL Time" + currentTime);
                GameObject ally = selectedGroup[k];
                //Debug.Log("hit dat shit btw im: " + k); //which minion in the list
                //FMODUnity.RuntimeManager.PlayOneShot(event, minion.transform.position);
            }
        }
        */
    }

    void playMinionSounds(int command, GameObject ally)
    {
        if (command == 0)
        {
            switch (ally.GetComponent<ENT_Body>().getMyType())
            {
                case ENT_DungeonEntity.monsterTypes.flowerMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_flower_move", ally.transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_mushroom_move", ally.transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.treeMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_tree_move", ally.transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.confusionMinion:
                case ENT_DungeonEntity.monsterTypes.necroMinion:
                case ENT_DungeonEntity.monsterTypes.slowMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_confusion_move", ally.transform.position);
                    break;
                default:
                    break;
            }
        }
        else if (command == 1)
        {
            switch (ally.GetComponent<ENT_Body>().getMyType())
            {
                case ENT_DungeonEntity.monsterTypes.flowerMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_flower_attack", ally.transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_mushroom_attack", ally.transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.treeMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_tree_attack", ally.transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.confusionMinion:
                case ENT_DungeonEntity.monsterTypes.necroMinion:
                case ENT_DungeonEntity.monsterTypes.slowMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Minions/ally_confusion_attack", ally.transform.position);
                    break;
                default:
                    break;
            }
        }
    }
}
