﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBG_EndGameTest : MonoBehaviour {

    bool startFinalBattle = true;

    ENT_PlayerBody player;
    PLY_AllyManager allyManager;
    WaveController finalBattleController;

    List<ENT_Shield> shields = new List<ENT_Shield>();
	// Use this for initialization
	void Start () {
        GameObject playerObj = GameObject.FindGameObjectWithTag("Player");
        player = playerObj.GetComponent<ENT_PlayerBody>();
        finalBattleController = GameObject.Find("FinalBattle").GetComponent<WaveController>();
        if (player != null)
        {
            allyManager = player.GetComponent<PLY_AllyManager>();
            ABI_Purify.rezUnlocked = true;
            for (int i = 0; i < 6; ++i)
            {
                player.unlocked_types.Add((ENT_DungeonEntity.monsterTypes)i);
                allyManager.MaxFollowers += 5;
            }
        }
        else
        {
            Debug.Log("Player and AllyManager are null!");
        }

        shields.Add(GameObject.Find("ShieldGenerator (3)").GetComponent<ENT_Shield>());
        shields.Add(GameObject.Find("ShieldGenerator (2)").GetComponent<ENT_Shield>());
        shields.Add(GameObject.Find("ShieldGenerator (1)").GetComponent<ENT_Shield>());
        shields.Add(GameObject.Find("ShieldGenerator").GetComponent<ENT_Shield>());
    }

    //I don't think this is used?
    IEnumerator beginFinalBattle()
    {
        yield return new WaitForSeconds(20);
        finalBattleController.startFinalBattle();
    }
	
	// Update is called once per frame
	void Update () {
        if (startFinalBattle)
        {
            int shieldCount = shields.Count;
            for (int i = 0; i < shieldCount; ++i)
            {
                ENT_Shield shield = shields[i];
                shield.entity_to_shield.shields = -1;
                shield.cleanse_self();
            }

            //StartCoroutine(beginFinalBattle());
            startFinalBattle = false;

            GameObject playerObj = GameObject.FindGameObjectWithTag("Player");
            //playerObj.transform.position = new Vector3(-528.0f, 69.09602f, -776.9f);
        }
	}
}
