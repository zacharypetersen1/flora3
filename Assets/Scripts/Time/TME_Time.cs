﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TME_Time {



    protected float scalar = 1;
    public type tag;



    //
    // The different types of time in the game
    //
    public enum type
    {
        master, game, player, enemies, environment
    }



    //
    // Returns the delta time based on the time scalar for this time
    //
    public abstract float getDeltaTime();



    //
    // Returns the time scalar that is currently being applied to the delta time
    //
    public abstract float getTimeScalar();



    //
    // Sets the time scalar
    //
    public void setTimeScalar(float setScalar)
    {
        scalar = setScalar;
    }
}
