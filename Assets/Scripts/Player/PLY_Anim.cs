﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_Anim : MonoBehaviour {


    
    public CHA_Motor motor;
    public PLY_PlayerDriver driver;
    public float lookAtEndAngle = 55;
    public float lookAtFalloff = 30;
    public static Animator animator;
    public string footstep = "event:/SFX/twig_footstep_grass";
    ABI_Moving surfScript;

    PLY_HandMagic[] handMagics;

    //public GameObject TwigRezEffect;
    public PLY_SproutDetector sproutDetector;



    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        motor = player.GetComponent<CHA_Motor>();
        driver = player.GetComponent<PLY_PlayerDriver>();
        PLY_Anim.animator = GetComponent<Animator>();
        Resources.LoadAll("");
        handMagics = GetComponentsInChildren<PLY_HandMagic>();
        if (!sproutDetector)
            sproutDetector = player.GetComponentInChildren<PLY_SproutDetector>();
    }


    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.L))
        {
            UTL_Dungeon.makeItInteresting(transform.position);
        }*/
    }


    void OnAnimatorIK(int layerIndex)
    {
        /*float deltaAngle = UTL_Math.getDeltaRotDeg(CAM_Camera.getDollyPhysicalRotZ(), motor.rotation * Mathf.Rad2Deg);
        if (deltaAngle <= lookAtEndAngle)
        {
            animator.SetLookAtWeight(1, 0.5f, 1f);
        }
        else if(deltaAngle <= lookAtEndAngle + lookAtFalloff)
        {
            animator.SetLookAtWeight(1, 0.5f * (1 - (deltaAngle - lookAtEndAngle) / lookAtFalloff), 1f);
        }
        else
        {
            animator.SetLookAtWeight(1, 0f, 1f);
        }

        Vector3 dirVec = UTL_Math.vec2ToVec3(UTL_Math.angleRadToUnitVec(-CAM_Camera.getDollyPhysicalRotZ() * Mathf.Deg2Rad), 0);
        Vector3 lookAtVec = transform.position + 5 * dirVec;
        //animator.SetBoneLocalRotation(HumanBodyBones.Head, new Quaternion(0, CAM_Camera.getDollyPhysicalRotZ(), 0, 0));
        animator.SetLookAtPosition(lookAtVec);*/
        //animator.set
    }

    void footstepTwig()
    {
        // Play footstep sound depending on terrain type
        if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.grass)
        {
            footstep = "event:/SFX/twig_footstep_grass";
        }
        else if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.rock)
        {
            footstep = "event:/SFX/twig_footstep_stone";
        }
        else if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.corrupt)
        {
            footstep = "event:/SFX/twig_footstep_corrupt";
        }
        FMODUnity.RuntimeManager.PlayOneShot(footstep, transform.position);
    }



    //
    // Triggered by animation acknowladging that mounting surfboard is complete
    //
    void onSurfMounted()
    {
        surfScript = (ABI_Moving)ABI_Manager.manager.abilities[ABI_Manager.abilityType.Moving];
        surfScript.onSurfMounted();
    }

    void onRezEnd()
    {
        animator.SetBool("doRez", false);
        ABI_Purify.busyRezing = false;
        /*int magicCount = handMagics.Length;
        for (int i = 0; i < magicCount; ++i)
        {
            PLY_HandMagic magic = handMagics[i];
            magic.turnOff();
        }*/
        driver.configureRunning();
    }

    void RezEffect()
    {
        //print("rezEffect");
        //UTL_Resources.cloneAtLocation(TwigRezEffect, transform.position, false);
        sproutDetector.DoRez();
        UTL_Resources.cloneAtLocation("Abilities/Rez/RezStamp", motor.transform.position);
    }

    void onRezStart()
    {
        ABI_Purify.canCancelRez = true;
        driver.configureRez();
        ABI_Purify.busyRezing = true;
        int magicCount = handMagics.Length;
        for (int i = 0; i < magicCount; ++i)
        {
            PLY_HandMagic magic = handMagics[i];
            magic.turnOn();
        }
    }

    void onRezPart1End()
    {
        ABI_Purify.canCancelRez = false;
    }
}
