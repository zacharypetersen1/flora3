﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_TargetPointer : FUI_FloatingUI
{

    public override void Awake()
    {
        base.Awake();
        target = gameObject;
    }

    // Use this for initialization
    public override void Start()
    {
        fui = (GameObject)MonoBehaviour.Instantiate(Resources.Load("FloatingUI/shittayArrow"));
        setOffset(fui);
        hide();
        fui.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
    }

    public void Update()
    {
        if (CAM_Camera.singleton.currentState != CAM_State.type.targeting)
        {
            target = UTL_Targeting.getTarget();
        }
        if (!target)
        {
            hide();
            target = gameObject;
        }
        else if (target == gameObject)
        {
            hide();
        }
        else
        {
            show();
        }
        base.update();
    }
}
