﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_CommandActivateAbility : MonoBehaviour
{
    public GameObject commandPointer;
    Light light;
    Camera worldCamera;
    Vector3 middleScreen;
    CAM_ScreenResize camResize;

    ParticleSystem PS1;
    ParticleSystem PS2;

    ParticleSystem.MainModule PSMain1;
    ParticleSystem.MainModule PSMain2;
    Vector3 tier1Height = new Vector3(0, 5, 0);

	UIX_Crosshair crosshair;

    Ray ray;
    RaycastHit hit;

    GameObject objPointedTo;
    GameObject lastObjPointedTo;

    int layermask;

    ENT_Body body;

    Dictionary<ENT_DungeonEntity.monsterTypes, Color> typeToColor = new Dictionary<ENT_DungeonEntity.monsterTypes, Color>();
    COM_AllyCommands allyCommand;
    static bool playedMessage = false;
    Vector3 position;

    UIX_Pause pauseScript;

    // Use this for initialization
    private void Awake()
    {
        layermask = (1 << 11) | (1 << 13) | (1 << 15) | (1 << 17);
        commandPointer.SetActive(false);
        typeToColor[ENT_DungeonEntity.monsterTypes.treeMinion] = new Color32(71, 161,53,255);
        typeToColor[ENT_DungeonEntity.monsterTypes.flowerMinion] = new Color32(42, 146, 186, 255);
        typeToColor[ENT_DungeonEntity.monsterTypes.mushroomMinion] = new Color32(151, 143, 111, 255);
        typeToColor[ENT_DungeonEntity.monsterTypes.confusionMinion] = new Color32(36, 31, 119, 255);
        typeToColor[ENT_DungeonEntity.monsterTypes.slowMinion] = new Color32(234, 204, 64, 255);
        typeToColor[ENT_DungeonEntity.monsterTypes.necroMinion] = new Color32(255, 106, 85,255);
    }
    void Start()
    {
        allyCommand = gameObject.GetComponent<COM_AllyCommands>();
        crosshair = Camera.main.GetComponentInChildren<UIX_Crosshair>();
        light = commandPointer.transform.GetComponent<Light>();
        worldCamera = GameObject.Find("Camera").GetComponent<Camera>();
        middleScreen = new Vector3(worldCamera.pixelWidth / 2, worldCamera.pixelHeight / 2, worldCamera.nearClipPlane);

        GameObject scriptObjs = GameObject.FindGameObjectWithTag("Scripts");
        camResize = scriptObjs.GetComponent<CAM_ScreenResize>();

        PS1 = commandPointer.transform.GetChild(0).GetComponent<ParticleSystem>();
        PS2 = commandPointer.transform.GetChild(1).GetComponent<ParticleSystem>();

        PSMain1 = commandPointer.transform.GetChild(0).GetComponent<ParticleSystem>().main;
        PSMain2 = commandPointer.transform.GetChild(1).GetComponent<ParticleSystem>().main;

        pauseScript = scriptObjs.GetComponent<UIX_Pause>();
    }

    void Deactivate()
    {
        body = null;
        if (commandPointer.activeSelf)
            commandPointer.SetActive(false);
    }

    void ActiveUpdate()
    {
        light.color = typeToColor[body.entity_type];
        PSMain1.startColor = typeToColor[body.entity_type];
        PSMain2.startColor = typeToColor[body.entity_type];
        if (lastObjPointedTo != objPointedTo)
        {
            PS1.Stop();
            PS2.Stop();
        
            light.color = typeToColor[body.entity_type];
            PSMain1.startColor = typeToColor[body.entity_type];
            PSMain2.startColor = typeToColor[body.entity_type];
        
            PS1.Play();
            PS2.Play();
        }
        
        if (!commandPointer.activeSelf)
            commandPointer.SetActive(true);
        
        commandPointer.transform.position = body.transform.position + tier1Height;
        
        if (INP_PlayerInput.getButtonDown("Reload"))
        {
            if (body.entity_type == ENT_DungeonEntity.monsterTypes.flowerMinion ||
                    body.entity_type == ENT_DungeonEntity.monsterTypes.treeMinion ||
                    body.entity_type == ENT_DungeonEntity.monsterTypes.mushroomMinion)
            {
                body.activateCommandAbility = true;
            }
            else if (body.entity_type == ENT_DungeonEntity.monsterTypes.confusionMinion || body.entity_type == ENT_DungeonEntity.monsterTypes.slowMinion || body.entity_type == ENT_DungeonEntity.monsterTypes.necroMinion)
            {
                allyCommand.ActivateCommand(new List<GameObject>() { body.gameObject });
            }
        
            if (!playedMessage)
            {
                if (body.entity_type == ENT_DungeonEntity.monsterTypes.flowerMinion ||
                    body.entity_type == ENT_DungeonEntity.monsterTypes.treeMinion ||
                    body.entity_type == ENT_DungeonEntity.monsterTypes.mushroomMinion ||
                    body.entity_type == ENT_DungeonEntity.monsterTypes.confusionMinion ||
                    body.entity_type == ENT_DungeonEntity.monsterTypes.slowMinion ||
                    body.entity_type == ENT_DungeonEntity.monsterTypes.necroMinion)
                {
                    playedMessage = true;
                    TUT_TutorialManager.queueMessage(TUT_Messages.Messages.Tier1AbilityUsed);
                }
        
            }
        }
        lastObjPointedTo = objPointedTo;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject topSelection = SEL_Manager.GetTopSelectedMinion();
        
        if (!topSelection)
        {
            Deactivate();
        }
        else
        {
            body = topSelection.GetComponent<ENT_Body>();
            ActiveUpdate();
        }
    }
}
