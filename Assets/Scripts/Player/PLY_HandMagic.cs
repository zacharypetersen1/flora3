﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_HandMagic : MonoBehaviour {

    ParticleSystem.EmissionModule idlePS;
    ParticleSystem activatePS;
    TrailRenderer trailRend;

	// Use this for initialization
	void Start () {
        trailRend = GetComponent<TrailRenderer>();
        idlePS = transform.Find("IdlePS").GetComponent<ParticleSystem>().emission;
        activatePS = transform.Find("ActivatePS").GetComponent<ParticleSystem>();
    }

    public void turnOn()
    {
        idlePS.enabled = true;
        trailRend.enabled = true;
        activatePS.Play();
	}

    public void turnOff()
    {
        idlePS.enabled = false;
        trailRend.enabled = false;
    }
}
