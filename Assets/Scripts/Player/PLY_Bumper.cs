﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_Bumper : MonoBehaviour {



    public float bumpForceScalar = 2f;
    public float fallOffLowerLimit = 0.7f;
    public float maxRumbleVel = 1f;
    CHA_Motor motor;



	// Use this for initialization
	void Start () {
        motor = GameObject.Find("Player").GetComponent<CHA_Motor>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    //
    // Calculate the bump force vector given a collision
    //
    Vector3 calcBumpForceVec(Vector3 twigPos, Vector3 otherPos, Vector3 twigVel, float forceScalar)
    {
        // Calculate the difference scalar between the angles
        Vector3 toOther = (otherPos - twigPos).normalized;
        float diffScalar = Vector3.Dot(twigVel.normalized, toOther);

        // map [lowerLimit, 1.0] to [0.0, 1.0] to emphasize diff falloff
        diffScalar = Mathf.Clamp(diffScalar, fallOffLowerLimit, 1f);
        diffScalar = (diffScalar - fallOffLowerLimit) / (1 - fallOffLowerLimit);

        // Calculate final force
        Vector3 force = toOther * twigVel.magnitude * diffScalar * forceScalar;

        return force;
    }



    //
    // Runs on trigger enter
    //
    void OnTriggerEnter2D(Collider2D col)
    {
        // if touch enemy, add "bump" force to them
        if(col.tag == "enemy" || col.tag == "friendly")
        {
            // TEMP until rework motor system so that velocity is always vec3 in motor
            Vector3 twigVel = motor.getVelocity();
            // ****
            Vector3 bumpForce = calcBumpForceVec(transform.position, col.transform.position, twigVel, bumpForceScalar);

            //col.GetComponent<CHA_Motor>().addForce(bumpForce);
            //col.GetComponent<CHA_Motor>().addForce(UTL_Math.vec3ToVec2(col.transform.position - transform.position).normalized * bumpForceScalar);
            float rumbleInput = Mathf.Clamp(bumpForce.magnitude, 0, maxRumbleVel);
            float rumbleIntensity = UTL_Math.mapToNewRange(rumbleInput, 0, maxRumbleVel, 0, 0.2f);
            float rumbleTime = UTL_Math.mapToNewRange(rumbleInput, 0, maxRumbleVel, 0.07f, 0.17f);
        }
    }
}
