﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_StateSurf : PLY_MoveState
{

	// FMOD parameters
	float surfVal = 1;
	float mag;

    // Used to interpolate forward input for surfing
    float surfForwardInput;

    //
    // runs when entering state
    //
    public override void StateEnter()
    {
        base.StateEnter();
        surfForwardInput = 1;
    }



    //
    // runs once per frame
    //
    public override void StateUpdate()
    {
        // Nothing
    }



    //
    // runs once per frame at fixed intervals
    //
    public override void StateFixedUpdate()
    {
        //motor.move(CAM_Camera.applyDollyRotZ(INP_PlayerInput.get2DAxis("Movement", true)));

        // Get input
        Vector2 moveAxis = INP_PlayerInput.get2DAxis("Movement", true);

        // Set rotation velocity based on speed
        motor.rotationVelocity = UTL_Math.mapToNewRange(moveAxis.y, -1, 1, driver.surfingRotVelMin, driver.surfingRotVelMax);

        // Alter input to fit surfing mechanics
        moveAxis.x = 0;
        moveAxis.y = motor.shouldHinderSurf() ? 0 : UTL_Math.mapToNewRange(moveAxis.y, -1, 1, 0.5f, 1);
        moveAxis.y = Mathf.SmoothStep(surfForwardInput, moveAxis.y, 0.15f);
        if (PLY_PlayerDriver.singleton.spiderTwig && motor.shouldHinderSurf()) moveAxis.y = 0;
        surfForwardInput = moveAxis.y;
        moveAxis = CAM_Camera.applyDollyRotZ(moveAxis);
        motor.move(new Vector3(moveAxis.x, moveAxis.y, 0));
        //motor.move(CAM_Camera.applyDollyRotZ(INP_PlayerInput.get2DAxis("Movement", true)));

        mag = (motor.getVelocityMagnitude() - 0.5f) * 4f;
		surfVal = Mathf.Clamp (mag, 0, 2);

		//Debug.Log (surfVal);
		MUS_System.singleton.surfSoundSpeed (surfVal);
    }
}
