﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_HandMagicManager: MonoBehaviour
{
    static bool isOn = false;
    static PLY_HandMagic left, right;
    static bool[] isNearby = new bool[6];

    void Start()
    {
        left = GameObject.Find("HandMagicEffectLeft").GetComponent<PLY_HandMagic>();
        right = GameObject.Find("HandMagicEffectRight").GetComponent<PLY_HandMagic>();
    }

    public static void clearAll()
    {
        for(int i = 0; i < 6; i++)
        {
            isNearby[i] = false;
        }
    }

    public static void markAsNearby(ENT_Body.monsterTypes type)
    {
        isNearby[(int)type] = true;
    }

    public static void updateHandMagic()
    {
        for (int i = 0; i < 6; i++)
        {
            if (isNearby[i])
            {
                turnOn();
                return;
            }
        }
        turnOff();
    }

    static void turnOn()
    {
        if (!isOn)
        {
            left.turnOn();
            right.turnOn();
            isOn = true;
        }
    }

    static void turnOff()
    {
        if (isOn)
        {
            print("hello");
            left.turnOff();
            right.turnOff();
            isOn = false;
        }
    }
}
