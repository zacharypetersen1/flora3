﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class PLY_AllyManager : MonoBehaviour {
    public float farFollowDistance = 25;
    public float nearFollowDistance = 20F;
    public float threatenedRange = 50F; //controls range at which monsters will attack enemies
    private allyState state;
    public const float FOLLOWDISTANCESCALE = 1.3F; //0.5
    public float MAX_FOLLOW_DIST = 150;
    float recallTimer = 0f;
    public float RECALL_TIMER = 1F;
    public bool twigIsThreated = false;
    Vector3 infinty = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    public const float DISTANCE_TO_STRAY_FROM_GOTO = 15;
    public UIX_MinionCounter minionCounter;
    private AIC_AllyGroup _lastCommandIssued;
    public AIC_AllyGroup lastCommandIssued
    {
        get {
            if (_lastCommandIssued != null)
            {
                return _lastCommandIssued;
            }
            else
            {
                return followGroup;
            }
        }
        set
        {
            _lastCommandIssued = value;
        }
    }

    //[HideInInspector]
    //public Vector3 compareToPos;

    //public List<float> allyFollowDistances; 

    public enum allyState
    {
        follow, wait, goTo, activate, attack
    }

    public List<AIC_AllyGroup> commandGroups = new List<AIC_AllyGroup>();
    public AIC_AllyGroup followGroup;
    public COM_AllyCommands commandModule;
    PLY_PlayerDriver playerDriver;

    List<AIC_AllyGroup> removeList = new List<AIC_AllyGroup>();

    //private Vector3 waitPosition = Vector3.zero;

    private Transform twigTransform;

    //Add int: max count, currect num, and one for each minion type
    private int maxFollowers = 0;
    private int maxSlow = 3;
    private int maxConfusion = 3;
    private int maxNecro = 3;

    public Dictionary<ENT_DungeonEntity.monsterTypes, int> wateredCounts = new Dictionary<ENT_DungeonEntity.monsterTypes, int>()
    {
        { ENT_DungeonEntity.monsterTypes.treeMinion, 0 },
        { ENT_DungeonEntity.monsterTypes.flowerMinion, 0 },
        { ENT_DungeonEntity.monsterTypes.mushroomMinion, 0 },
        { ENT_DungeonEntity.monsterTypes.slowMinion, 0 },
        { ENT_DungeonEntity.monsterTypes.confusionMinion, 0 },
        { ENT_DungeonEntity.monsterTypes.necroMinion, 0 },
        { ENT_DungeonEntity.monsterTypes.turretMinion, 0 },
    };

    int _wateredCount = 0;
    public int wateredCount
    {
        set
        {
            _wateredCount = value;
        }
        get { return _wateredCount; }
    }

    int _allyCount = 0;
    public int allyCount
    {
        set
        {
            _allyCount = value;
            minionCounter.allyCount = _allyCount;
            //print("AllyCount: " + allyCount + ", MaxFollowers: " + maxFollowers + ", wateredCount: " + wateredCount + ", isGroupMax: " + isGroupMax());
        }
        get { return _allyCount; }
    }

    int _currTrees = 0;
    public int currTrees {
        set
        {
            _currTrees = value;
            minionCounter.treeCount = value;
        }
        get { return _currTrees; }
    }

    int _currFlowers = 0;
    public int currFlowers
    {
        set { _currFlowers = value;
            minionCounter.flowerCount = value;
        }
        get { return _currFlowers; }
    }

    int _currMushrooms = 0;
    public int currMushrooms
    {
        set { _currMushrooms = value;
            minionCounter.mushroomCount = value;
        }
        get { return _currMushrooms; }
    }

    int _currConfusions = 0;
    public int currConfusions
    {
        set { _currConfusions = value;
            minionCounter.confusionCount = value;
        }
        get { return _currConfusions; }
    }

    int _currSlow = 0;
    public int currSlow
    {
        set { _currSlow = value;
            minionCounter.slowCount = value;
        }
        get { return _currSlow; }
    }

    int _currNecro = 0;
    public int currNecro
    {
        set { _currNecro = value;
            minionCounter.necroCount = value;
        }
        get { return _currNecro; }
    }

    int _currTurret = 0;
    public int currTurret
    {
        set { _currTurret = value; }
        get { return _currTurret; }
    }

    public int MaxFollowers
    {
        set { maxFollowers = value;
            minionCounter.maxFollowers = value;
            //print("AllyCount: " + allyCount + ", MaxFollowers: " + maxFollowers + ", wateredCount: " + wateredCount + ", isGroupMax: " + isGroupMax());
        }
        get { return maxFollowers; }
    }

    public int MaxSlow
    {
        set { maxSlow = value;
            minionCounter.maxSlow = value;
        }
        get { return maxSlow; }
    }

    public int MaxConfusion
    {
        set
        {
            maxConfusion = value;
            minionCounter.maxConfusion = value;
        }
        get { return maxConfusion; }
    }

    public int MaxNecro
    {
        set
        {
            maxNecro = value;
            minionCounter.maxNecro = value;
        }
        get { return maxNecro; }
    }

    public bool isGroupMax()
    {
        return allyCount+wateredCount >= maxFollowers;
    }

    public bool isSlowMax()
    {
        return _currSlow + wateredCounts[ENT_DungeonEntity.monsterTypes.slowMinion] >= maxSlow;
    }

    public bool isConfusionMax()
    {
        return _currConfusions + wateredCounts[ENT_DungeonEntity.monsterTypes.confusionMinion] >= maxConfusion;
    }

    public bool isNecroMax()
    {
        return _currNecro + wateredCounts[ENT_DungeonEntity.monsterTypes.necroMinion] >= maxNecro;
    }


    private List<GameObject> ally_group_1 = new List<GameObject>(); // this is group
    private List<GameObject> ally_group_2 = new List<GameObject>(); // this is group
    private List<GameObject> ally_group_3 = new List<GameObject>(); // this is group
    private List<GameObject> ally_group_4 = new List<GameObject>(); // this is group
    private List<GameObject> ally_group_5 = new List<GameObject>(); // this is group
    private List<GameObject> ally_group_6 = new List<GameObject>(); // this is group
    private List<GameObject> ally_group_7 = new List<GameObject>(); // this is group
    private List<List<GameObject>> allies = new List<List<GameObject>>();
    private List<GameObject> allAllies = new List<GameObject>();
    private List<GameObject> allStructures = new List<GameObject>();

    private List<GameObject> enemies = new List<GameObject>();

    public List<List<GameObject>> getAllies() { return allies; }
    public List<GameObject> getEnemies() { return enemies; }

    public void resetAllyManager()
    {
        ally_group_1 = new List<GameObject>(); // this is group
        ally_group_2 = new List<GameObject>(); // this is group
        ally_group_3 = new List<GameObject>(); // this is group
        ally_group_4 = new List<GameObject>(); // this is group
        ally_group_5 = new List<GameObject>(); // this is group
        ally_group_6 = new List<GameObject>(); // this is group
        ally_group_7 = new List<GameObject>(); // this is group

        allies = new List<List<GameObject>>();
        allies.Add(ally_group_1);
        allies.Add(ally_group_2);
        allies.Add(ally_group_3);
        allies.Add(ally_group_4);
        allies.Add(ally_group_5);
        allies.Add(ally_group_6);
        allies.Add(ally_group_7);

        allAllies = new List<GameObject>();
        allStructures = new List<GameObject>();

        commandGroups = new List<AIC_AllyGroup>();
        followGroup = new AIC_AllyGroup(allyState.follow, new List<GameObject>(), this, transform.position);
        commandGroups.Add(followGroup);

        enemies = new List<GameObject>();
}

    public List<GameObject> getAllEnemies
    {
        get { return enemies; }
    }

    public List<GameObject> getAllAllies
    {
        get { return allAllies; }
    }

    public List<GameObject> getAllStructures
    {
        get { return allStructures; }
    }
    GameObject grandGaurdians;
    float scalarToGaurdians = 4;

    private void Awake()
    {
        allies.Add(ally_group_1);
        allies.Add(ally_group_2);
        allies.Add(ally_group_3);
        allies.Add(ally_group_4);
        allies.Add(ally_group_5);
        allies.Add(ally_group_6);
        allies.Add(ally_group_7);

        followGroup = new AIC_AllyGroup(allyState.follow, new List<GameObject>(), this, transform.position);
        commandGroups.Add(followGroup);
        commandModule = gameObject.GetComponent<COM_AllyCommands>();
        playerDriver = gameObject.GetComponent<PLY_PlayerDriver>();
    }

    #region setStateFunctions
    public void setState(GameObject ally, allyState newState)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                Debug.LogWarning("Error: must pass in a gotoPosition in order to change state to go to. Try passing in a Vector3 position to go to.");
                //body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                return;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                Debug.LogWarning("Error: must pass in a target to attack when changing state to Attack. Try passing in the enemy to attack.");
                return;
        }
    }

    public void setState(GameObject ally, allyState newState, Vector3 goToPos)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                //need to set gotoposition
                body.WaitPos = goToPos;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                break;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                Debug.LogWarning("Error: must pass in a target to attack when changing state to Attack. Try passing in the enemy to attack.");
                return;
        }
    }

    public void setState(GameObject ally, allyState newState, GameObject attackTarget)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                Debug.LogWarning("Error: must pass in a gotoPosition in order to change state to go to. Try passing in a Vector3 position to go to.");
                //body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                return;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                body.AttackTarget = attackTarget;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyAttackTree;
                break;
        }
    }

    public void setAllAlliesState(allyState newState)
    {
        //if (state == newState) return;
        state = newState;
        int groupCount = allies.Count;
        for (int iGroup = 0; iGroup < groupCount; ++iGroup)
        {
            List<GameObject> group = allies[iGroup];
            int allyCount = group.Count;
            for (int iAlly = 0; iAlly < allyCount; ++iAlly)
            {
                GameObject ally = group[iAlly];
                setState(ally, newState);
            }
        }
    }

    public void setAllAlliesState(allyState newState, Vector3 goToPos)
    {
        //if (state == newState) return;
        state = newState;
        int allyCount = allAllies.Count;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allAllies[i];
            setState(ally, newState, goToPos);
        }
    }

    public void setAllAlliesState(allyState newState, GameObject target)
    {
        //if (state == newState) return;
        state = newState;
        int allyCount = allAllies.Count;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allAllies[i];
            setState(ally, newState, target);
        }
    }
    #endregion

    public allyState getState()
    {
        return state;
    }


    public void addStructure(GameObject newStructure, bool forcePlacement = false, bool treatAsEnabled = false)
    {
        if (forcePlacement)
        {
            if (treatAsEnabled)
                InsertAtActiveSpot(allStructures, newStructure);
            else
                allStructures.Add(newStructure);
        }
        if (newStructure.activeInHierarchy)
            InsertAtActiveSpot(allStructures, newStructure);
        else
            allStructures.Add(newStructure);
    }

    public void removeStructure(GameObject exStructure)
    {
        allStructures.Remove(exStructure);
    }

    public AIC_AllyGroup addAlly(GameObject newAlly)
    {
        ENT_Body body = newAlly.GetComponent<ENT_Body>();
        if (body == null) return null;
        if (!body.Purity)
        {
            print("Error: Tried to add an evil monster " + newAlly.name + "to ally list");
            return null;
        }
        allAllies.Add(newAlly);
        allies[(int)body.getMyType()].Add(newAlly);

        if (!body.isGhost)
        {
            switch ((int)body.getMyType())
            {
                case 0:
                    currFlowers++;
                    break;
                case 1:
                    currMushrooms++;
                    break;
                case 2:
                    currTrees++;
                    break;
                case 3:
                    currSlow++;
                    break;
                case 4:
                    currNecro++;
                    break;
                case 5:
                    currConfusions++;
                    break;
                case 6:
                    currTurret++;
                    break;
            }
            allyCount++;
        }
        commandModule.FollowCommand(new List<GameObject>() { newAlly });

        SEL_Manager.AddToSelection(newAlly);

        return followGroup;
    }

    public AIC_AllyGroup setCorrectGroup(GameObject newAlly)
    {
        switch(lastCommandIssued.State)
        {
            case allyState.follow:
                setState(newAlly, allyState.follow);
                followGroup.addToList(newAlly);
                return followGroup;
            case allyState.goTo:
                setState(newAlly, allyState.goTo, lastCommandIssued.Destination);
                lastCommandIssued.addToList(newAlly);
                return lastCommandIssued;
            case allyState.attack:
                setState(newAlly, allyState.attack, lastCommandIssued.Target);
                lastCommandIssued.addToList(newAlly);
                return lastCommandIssued;
            case allyState.wait:
                setState(newAlly, allyState.wait, lastCommandIssued.Destination);
                lastCommandIssued.addToList(newAlly);
                return lastCommandIssued;
            default:
                setState(newAlly, allyState.follow);
                followGroup.addToList(newAlly);
                return followGroup;
        }
    }

    public void InsertAtActiveSpot(List<GameObject> list, GameObject minion)
    {
        int count = list.Count;
        for (int i = 0; i < count; ++i)
        {
            GameObject iMinion = list[i];
            if (iMinion.activeInHierarchy)
            {
                list.Insert(i, minion);
                return;
            }
        }
        list.Add(minion);
    }

    public void addEnemy(GameObject newEnemy, bool forcePlacement = false, bool treatAsEnabled = false)
    {
        ENT_Body body = newEnemy.GetComponent<ENT_Body>();
        if (body == null) return;
        if (body.Purity)
        {
            print("Error: Tried to add a good creature " + newEnemy.name + " to enemy list");
            return;
        }
        if (forcePlacement)
        {
            if (treatAsEnabled)
                InsertAtActiveSpot(enemies, newEnemy);
            else
                enemies.Add(newEnemy);
        }
        else
        {
            if (newEnemy.activeInHierarchy)
                InsertAtActiveSpot(enemies, newEnemy);
            else
                enemies.Add(newEnemy);
        }
        
        body.CurrentTree = ENT_Body.BehaviorTree.WorkTree;
    }

    public void removeAlly(GameObject exAlly)
    {
        int groupCount = allies.Count;
        for (int i = 0; i < groupCount; ++i)
        {
            List<GameObject> group = allies[i];
            if (group.Contains(exAlly))
            {
                
                group.Remove(exAlly);
                //allyFollowDistances.RemoveAt(allyCount); //remove element at end of array
                if (!exAlly.GetComponent<ENT_Body>().isGhost)
                {
                    allyCount--;
                }
                switch(exAlly.GetComponent<ENT_Body>().getMyType())
                {
                    case ENT_DungeonEntity.monsterTypes.treeMinion:
                        currTrees--;
                        break;
                    case ENT_DungeonEntity.monsterTypes.flowerMinion:
                        currFlowers--;
                        break;
                    case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                        currMushrooms--;
                        break;
                    case ENT_DungeonEntity.monsterTypes.confusionMinion:
                        currConfusions--;
                        break;
                    case ENT_DungeonEntity.monsterTypes.slowMinion:
                        currSlow--;
                        break;
                    case ENT_DungeonEntity.monsterTypes.necroMinion:
                        currNecro--;
                        break;
                    case ENT_DungeonEntity.monsterTypes.turretMinion:
                        currTurret--;
                        break;
                }
            }

        }
        if (allAllies.Remove(exAlly))
        {
            if (SEL_Manager.getSelectedCount() == 1)
            {
                SEL_Manager.SelectNextMinion();
            }
            SEL_Manager.removeFromSelection(this.gameObject);
        }
        
        //else print("Warning: Tried to remove " + exAlly.name + " From Ally list but it is not in Ally list");
    }

    public void removeEnemy(GameObject exEnemy)
    {
        enemies.Remove(exEnemy);
        //else print("Warning: Tried to remove " + exEnemy.name+ " From enemy list but it is not in enemy list");
    }

    //return null if no enemies in enemy list
    public GameObject getNearestEnemy(Vector3 pos, bool includeStructures = true)
    {
        GameObject nearest = null;
        float leastSqrDist = 1000000;
        int enemyCount = enemies.Count;
        for (int i = 0; i < enemyCount; ++i)
        {
            GameObject enemy = enemies[i];
            if (!enemy.activeInHierarchy)
                continue;
            float dist = UTL_Math.sqrDistance(pos, enemy.transform.position);
            if (dist < leastSqrDist)
            {
                nearest = enemy;
                leastSqrDist = dist;
            }
        }
        if (includeStructures)
        {
            int structureCount = allStructures.Count;
            for (int i = 0; i < structureCount; ++i)
            {
                GameObject structure = allStructures[i];
                if (structure)
                {
                    float dist = UTL_Math.sqrDistance(pos, structure.transform.position);
                    if (dist < leastSqrDist)
                    {
                        nearest = structure;
                        leastSqrDist = dist;
                    }
                }
            }
        }
        //Debug.Log(nearest.name);
        return nearest;
    }

    //public void OnMinionDisable(GameObject minion)
    //{
    //    //keep disabled minions at the end of the list
    //    enemies.Remove(minion);
    //    enemies.Add(minion);
    //}

    public GameObject getNearestEnemyToTwig()
    {
        return getNearestEnemy(transform.position, false);
    }

    public GameObject getNearestVisibleEnemy(Vector3 pos, float maxRange, bool includeStructures = true)
    {
        GameObject nearest = null;
        float leastDist = maxRange * maxRange;
        int enemyCount = enemies.Count;
        for (int i = 0; i < enemyCount; ++i)
        {
            GameObject enemy = enemies[i];
            if (!enemy.activeInHierarchy)
                continue;
            float dist = UTL_Math.sqrDistance(pos, enemy.transform.position);
            if (dist < leastDist)
            {
                if (!UTL_Dungeon.checkLOSWalls(pos, enemy.transform.position))
                    continue;
                nearest = enemy;
                leastDist = dist;
            }
        }
        if (includeStructures)
        {
            int structureCount = allStructures.Count;
            for (int i = 0; i < structureCount; ++i)
            {
                GameObject structure = allStructures[i];
                float dist = UTL_Math.sqrDistance(pos, structure.transform.position);
                if (dist < leastDist)
                {
                    if (!UTL_Dungeon.checkLOSWalls(pos, structure.transform.position))
                        continue;
                    nearest = structure;
                    leastDist = dist;
                }
            }
        }
        return nearest;
    }

    public GameObject getNearestVisibleEnemyWithinSphere(Vector3 pos, float maxRange, Vector3 center, float radius, bool includeStructures = true)
    {
        GameObject nearest = null;
        float leastDist = maxRange * maxRange;
        radius = radius * radius; // square the radius
        int enemyCount = enemies.Count;
        for (int i = 0; i < enemyCount; ++i)
        {
            GameObject enemy = enemies[i];
            if (!enemy.activeInHierarchy)
                continue;
            float dist = UTL_Math.sqrDistance(pos, enemy.transform.position);
            if (dist < leastDist)
            {
                if (UTL_Math.sqrDistance(enemy.transform.position, center) <= radius)
                {
                    if (!UTL_Dungeon.checkLOSWalls(pos, enemy.transform.position))
                        continue;
                    nearest = enemy;
                    leastDist = dist;
                }
            }
        }
        if (includeStructures)
        {
            int structureCount = allStructures.Count;
            for (int i = 0; i < structureCount; ++i)
            {
                GameObject structure = allStructures[i];
                float dist = UTL_Math.sqrDistance(pos, structure.transform.position);
                if (dist < leastDist)
                {
                    if (UTL_Math.sqrDistance(structure.transform.position, center) <= radius)
                    {
                        if (!UTL_Dungeon.checkLOSWalls(pos, structure.transform.position)) continue;
                        nearest = structure;
                        leastDist = dist;
                    }
                }
            }
        }
        return nearest;
    }

    public GameObject getNearestAlly(Vector3 pos, bool includeTwig = true)
    {
        GameObject nearest = null;
        float leastDist = 1000000;
        int allyCount = allAllies.Count;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allAllies[i];
            float dist = UTL_Math.sqrDistance(pos, ally.transform.position);
            if (dist < leastDist)
            {
                nearest = ally;
                leastDist = dist;
            }
        }
        if (includeTwig)
        {
            float dist = UTL_Math.sqrDistance(pos, gameObject.transform.position);
            if (dist < leastDist)
            {
                nearest = gameObject;
                leastDist = dist;
            }
        }
        return nearest;
    }

    public GameObject getNearestVisibleAlly(ENT_Body body, float maxRange, bool includeTwig = true, bool prioritizeTwig = false)
    {
        Vector3 pos = body.gameObject.transform.position;
        GameObject nearest = null;
        float leastDist = maxRange * maxRange;
        if (includeTwig && prioritizeTwig)
        {
            if (UTL_Dungeon.checkLOSWalls(pos, gameObject.transform.position))
            {
                float dist = UTL_Math.sqrDistance(pos, gameObject.transform.position);
                if (dist < leastDist)
                {
                    nearest = gameObject;
                    leastDist = dist;
                }
            }
            if (prioritizeTwig && nearest != null) return nearest;
        }
        int allyCount = allAllies.Count;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allAllies[i];
            float dist = UTL_Math.sqrDistance(pos, ally.transform.position);
            if (dist < leastDist)
            {
                if (!UTL_Dungeon.checkLOSWalls(pos, ally.transform.position))
                    continue;
                nearest = ally;
                leastDist = dist;
            }
        }
        if (includeTwig && !prioritizeTwig)
        {
            float dist = UTL_Math.sqrDistance(pos, gameObject.transform.position);
            if (dist < leastDist)
            {
                if (UTL_Dungeon.checkLOSWalls(pos, gameObject.transform.position))
                {
                    nearest = gameObject;
                    leastDist = dist;
                }
            }
        }
        if (body.inFinalWavesBattle)
        {
            float dist = UTL_Math.sqrDistance(pos, body.GoToAttackTarget); //this is the grand gaurdians
            if (dist/scalarToGaurdians < leastDist)
            {
                nearest = grandGaurdians;
                leastDist = dist;
            }
        }
        return nearest;
    }

    public Vector3 getMidpoint()
    {
        Vector3 tempVec = Vector3.zero;
        int allyCount = allAllies.Count;
        if (allyCount == 0)
            return tempVec;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allAllies[i];
            tempVec += ally.transform.position;
        }
        tempVec /= allyCount;
        return tempVec;
    }

    public bool isMidpointNearTwig()
    {
        Vector3 midpoint = getMidpoint();
        return UTL_Math.sqrDistance(twigTransform.position, midpoint) < farFollowDistance * farFollowDistance;
    }

    public bool amINearTwig(Vector3 myPos)
    {
        return UTL_Math.sqrDistance(twigTransform.position, myPos) < nearFollowDistance * nearFollowDistance;
    }

    /*public Vector3 get
     * ()
    {
        //print("waitPosition: " + waitPosition);
        return waitPosition;
    }*/

    public void setWaitPosition()
    {
        int allyCount = allAllies.Count;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allAllies[i];
            ally.GetComponent<ENT_Body>().WaitPos = twigTransform.position;
        }
    }

    public void setWaitPosition(Vector2 newWaitPos)
    {
        int allyCount = allAllies.Count;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allAllies[i];
            ally.GetComponent<ENT_Body>().WaitPos = newWaitPos;
        }
    }

    public List<GameObject> getAllAlliesWithinRange(Vector3 pos, float range, bool includeTwig = false)
    {
        List<GameObject> inRange = new List<GameObject>();
        range = range * range;
        int allyCount = allAllies.Count;
        for (int i = 0; i < allyCount; ++i)
        {
            GameObject ally = allAllies[i];
            if (UTL_Math.sqrDistance(pos, ally.transform.position) <= range)
            {
                inRange.Add(ally);
            }
        }
        if (includeTwig)
        {
            if (UTL_Math.sqrDistance(pos, transform.position) <= range)
            {
                inRange.Add(gameObject);
            }
        }
        return inRange;
    }

    public List<GameObject> getAllEnemiesWithinRange(Vector3 pos, float range)
    {
        List<GameObject> inRange = new List<GameObject>();
        range = range * range;
        int enemyCount = enemies.Count;
        for (int i = 0; i < enemyCount; ++i)
        {
            GameObject enemy = enemies[i];
            if (!enemy.activeInHierarchy)
                continue;
            if (UTL_Math.sqrDistance(pos, enemy.transform.position) <= range)
                inRange.Add(enemy);
        }
        return inRange;
    }
    /*

    public bool amINearWaitPos(Vector3 myPos)
    {
        return Vector3.Distance(waitPosition, myPos) < individualFollowDistance;
    }

    public bool isMidpointNearWaitPos()
    {
        Vector3 midpoint = getMidpoint();
        return Vector3.Distance(waitPosition, midpoint) < midpointFollowDistance;
    }*/


    // Use this for initialization
    void Start () {
        twigTransform = gameObject.transform;
        setWaitPosition();
        if (minionCounter == null)
        {
            GameObject textObj = GameObject.Find("PlayerUI").transform.FindChild("MaxFollowersText").gameObject;
            if (textObj == null)
            {
                Debug.LogWarning("Error: Max Follower Text not set up properly");
            }
            else
            {
                minionCounter = textObj.GetComponent<UIX_MinionCounter>();
            }
        }
        //to update minion counter maximums
        //MaxSlow = maxSlow;
        //MaxConfusion = maxConfusion;
        //MaxNecro = maxNecro;

        grandGaurdians = GameObject.Find("GrandGuardians");
    }
	
	// Update is called once per frame
	void Update () {
        //setFollowDistances();
        removeList.Clear();
        int commandGroupCount = commandGroups.Count;
        for (int i = 0; i < commandGroupCount; ++i)
        {
            AIC_AllyGroup group = commandGroups[i];
            if (group.State == allyState.attack && (group.Target == null || group.Target.GetComponent<ENT_DungeonEntity>().isAlly || group.Target.GetComponent<ENT_DungeonEntity>().State == "cleansed"))
            {
                //commandModule.FollowCommand(group.AllyList);
                //followGroup.setFollowDistances();
                removeList.Add(group);
            }
            /*else if (group.State == allyState.goTo || group.State == allyState.wait)
            {
                group.assignGoToPoints();
            }*/
            else group.setFollowDistances();
        }
        int removeCount = removeList.Count;
        for (int i = 0; i < removeCount; ++i)
        {
            AIC_AllyGroup removeGroup = removeList[i];
            commandModule.FollowCommand(new List<GameObject>(removeGroup.AllyList));
            followGroup.setFollowDistances();
        }
        recallTimer += TME_Manager.getDeltaTime(TME_Time.type.master);
        if (recallTimer >= RECALL_TIMER && !(playerDriver.activeState == PLY_MoveState.type.surf))
        {
            recallMinions();
            recallTimer = 0;
        }
        
    }

    public List<GameObject> getAlliesOfType(ENT_DungeonEntity.monsterTypes type)
    {
        return allies[(int)type];
    }

    public void recallMinions(List<GameObject> minionsToRecall = null)
    {
        if (minionsToRecall == null)
        {
            minionsToRecall = new List<GameObject>(followGroup.AllyList);
        }   
        float startAngle = Random.Range(0F, 360F);
        //List<Vector3> goToPoints = new List<Vector3>();
        float dist = 5F; //1.5 times navmesh diameter
        bool offset = false;
        List<GameObject> recallList = new List<GameObject>();
        int recallCount = minionsToRecall.Count;
        for (int i = 0; i < recallCount; ++i)
        {
            GameObject minion = minionsToRecall[i];
            NavMeshAgent agent = minion.GetComponent<NavMeshAgent>();
            Animator anim = minion.GetComponentInChildren<Animator>();
            bool appearing = (anim.GetCurrentAnimatorStateInfo(0).IsName("Appear"));
            float distance = Vector3.Distance(minion.transform.position, transform.position);
            if (distance > MAX_FOLLOW_DIST || (agent.pathStatus != NavMeshPathStatus.PathComplete && !appearing && (!twigIsThreated || distance > threatenedRange))) // 
            {
                if (!minion.GetComponent<ENT_Body>().reappear)
                {
                    recallList.Add(minion);
                }
            }
        }
       for (int pointsAssigned = 0; pointsAssigned < recallList.Count; offset = !offset)
        {
            for (float angle = 0F; angle <= 360F; angle += 60F)
            {
                float tempAngle = angle + (offset ? 30 : 0) + startAngle + Random.Range(-10F, 10F);
                Vector3 unitAngleVector = UTL_Math.angleRadToUnitVec((Mathf.Deg2Rad *tempAngle), 0);
                float dot = Vector3.Dot(transform.forward.normalized, unitAngleVector.normalized);
                if (dot > 0)
                {
                    continue;
                }
                Vector3 goToPoint = makeRecallPoint(tempAngle, dist);
                if (goToPoint != infinty)
                {
                    //goToPoints.Add(goToPoint);
                    GameObject minion = recallList[pointsAssigned];
                    minion.GetComponent<ENT_Body>().teleport(goToPoint);
                    pointsAssigned++;
                    if (pointsAssigned >= recallList.Count) break;
                }
            }
            dist += 4f; // 1.5
        }
        //commandModule.FollowCommand(minionsToRecall);
    }

    Vector3 makeRecallPoint(float angle, float dist)
    {
        Vector3 goToPoint = transform.position + UTL_Math.vectorfromAngleAndMagnitude(angle, dist);
        //goToPoint = UTL_Dungeon.snapPositionToTerrain(goToPoint);
        //check if point is valid
        NavMeshHit hit;
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        if (NavMesh.SamplePosition(goToPoint, out hit, 10.0F, navmeshMask))
        {
            return hit.position;
        }
        else return new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    }

    //public 
}
