﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_InteractableManager : MonoBehaviour {

    public int Keys;

	// Use this for initialization
	void Start () {
        Keys = 0;
	}
	
	// Update is called once per frame
	void Update () {
        //Use for interactables with a continuous effect
	}

    // Update at fixed intervals
    private void FixedUpdate()
    {
        //If interactables use a timer put in here
    }

    // Check if player has a key
    public bool hasKey()
    {
        if (Keys < 1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}
