﻿// Zach

using UnityEngine;

public class PLY_WallSensor : MonoBehaviour {

    int dirtWallCounter = 0;        // the number of dirt walls that are currently colliding with
    PLY_PlayerDriver driver;



    //
    // Use this for initialization
    //
    void Start () {
        driver = GameObject.Find("Player").GetComponent<PLY_PlayerDriver>();
	}



    //
    // Update is called once per frame
    //
    void Update () {
	
	}



    //
    // add one to counter when enter collision with dirt wall
    //
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "dirtWall")
        {
            dirtWallCounter++;
            driver.sensingDirtWall = true;
        }
    }



    //
    // subtract one from counter when exit collision with dirt wall
    //
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "dirtWall")
        {
            dirtWallCounter--;
            driver.sensingDirtWall = dirtWallCounter > 0;
        }
    }
}
