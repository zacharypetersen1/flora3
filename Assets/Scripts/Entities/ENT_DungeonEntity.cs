﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic object for monsters, strucutres, and interactables to extend.
/// </summary>
public abstract class ENT_DungeonEntity : MonoBehaviour {

    [HideInInspector]
    public string State; // Can be Dormant, Active, Cleansed, or Purified.
                         //placeholder for model
    public enum monsterTypes { flowerMinion, mushroomMinion, treeMinion, slowMinion, necroMinion, confusionMinion, turretMinion };

    protected GameObject player;
    public GameObject Player
    {
        get { return player; }
    }
    protected Renderer rend;
    protected FUI_HasItem itemHeld;
    public float _CL;
    public float _CL_MAX;

    [HideInInspector]
    public bool vulnerable = true; //Used by ENT_Body and ENT_CrystalSpike

    //public float AttackDC; //Delta Corruption dealt by this unit when it attacks.
    protected GameObject TargetedBy; //Reference to who sent the latest combat message
    // The number of shield towers protecting the entity. While this is above 0, the entity cannot be hurt.
    public int shields =0;//public GameObject shield;

    public PLY_AllyManager allyManager;

    // For dynamic minion spawning
    int waves_spawned;
    public int num_waves; //This shold be the same as the size of the spawn_waves' Wave_lists, to avoid confusion.
    public List<ENT_spawn_wave> wave_points;

    public Material ghostMat;

    public float CL_MAX
    {
        get { return _CL_MAX; }
        set { _CL_MAX = value; }
    }

    protected bool purity;
    public bool Purity
    {
        get { return purity; }
        set
        {
            if ( purity != value)
            {
                purify_self();
            }
            else
            {
                corrupt_self();
            }
        }
    }

    public bool isAlly
    {
        get { return Purity; }
        set { Purity = value; }
    }

    /*
    public GameObject Shield
    {
        get { return shield; }
        set { shield = value; }
    }
    */
    protected bool cleansed = false;

    public virtual float CL
    {
        get { return _CL; }
        set
        {
            _CL = value;
            _CL = Mathf.Clamp(_CL, 0, _CL_MAX);
            on_delta_clarity();

            if (State == "active" && _CL == 0 && !cleansed)
            {
                cleanse_self();
                cleansed = true;
            }
        }
    }

    public virtual void Awake()
    {
        State = "dormant";
        //shields = 0;
        allyManager = GameObject.Find("Player").GetComponent<PLY_AllyManager>();
    }

    // Use this for initialization
    public virtual void Start () {
        player = GameObject.Find("Player");
        itemHeld = gameObject.GetComponent<FUI_HasItem>();
        waves_spawned = 0;
        rend = GetComponentInChildren<Renderer>();
    }

    // Update is called once per frame
    public virtual void Update () {
	    //Code for dynamic minion spawning:
        if(State == "active" && wave_points.Count != 0)
        {
            if(waves_spawned < num_waves && (((float)CL / (float)CL_MAX) < ((((float)num_waves) - (float)waves_spawned) / ((float)num_waves + 1))))
            {
                foreach (ENT_spawn_wave wave_spawner in wave_points)
                {
                    wave_spawner.spawn(waves_spawned);
                }
                waves_spawned++;
            }
        }
	}

    // FixedUpdate is called at regular intervals
    public virtual void FixedUpdate()
    {

    }

    /// <summary>
    /// General function for changing the state of structures, monsters, and other entities to active.
    /// Also used for activating interactables, like purity EMP crystals and heart crystals.
    /// </summary>
    public abstract void activate();

    public virtual void cleanse_self()
    {
        //if (itemHeld)
        //{
        //    if (!string.IsNullOrEmpty(itemHeld.path_to_item))
        //    {
        //        UTL_Resources.cloneAtLocation(itemHeld.path_to_item, transform.position);
        //    }
        //    itemHeld.hide();
        //}
    }

    public virtual void purify_self()
    {
        purity = true;
    }

    public virtual void corrupt_self()
    {
        purity = false;
    }

    public virtual void on_delta_clarity()
    {

    }

    /// <summary>
    /// catches object message and interprets for reciever
    /// </summary>
    /// <param name="combat_message"></param>
    public virtual void receive_combat_message(CBI_CombatMessenger combat_message)
    {
        // trigger callbacks if damage is greater than 0
        if (cleansed) return;
        if(combat_message.Damage_DC > 0)
        {
            inflict_damage(combat_message.Damage_DC);
        }

        // trigger callbacks if healing is greater than 0
        if (combat_message.Healing_DC > 0)
        {
            inflict_healing(combat_message.Healing_DC);
        }

        // trigger callbacks for status and purify attempt
        inflict_status(combat_message.Status_list);

        if(combat_message.Delta_Purity > 0)
        {
            attempt_purify_or_corrupt(combat_message.Delta_Purity);
        }
        

        //source of message

    }

    public virtual void inflict_damage(float DC)
    {
        if (shields > 0)
        {
            Debug.Log("DC blocked by shield.");
            return;
        }
        else
        {
            CL -= DC;
        }
    }

    public virtual void inflict_healing(float DC)
    {
        CL += DC;
    }

    public virtual void inflict_status(List<CBI_CombatMessenger.status> statuses)
    {

    }

    public virtual void attempt_purify_or_corrupt(float deltaPurity)
    {
        print("base class");
    }


    //
    // Finds the midpoint between the Dungeon Entity and its target
    //
    public virtual Vector2 midpoint_between_us(Vector3 target)
    {
        Vector2 returnable = UTL_Math.vec3ToVec2((gameObject.transform.position + target)/2);
        return returnable;
    } 

    public virtual Vector3 getNearestAttackablePoint(Vector3 originalPos)
    {
        return transform.position;
    }
}
