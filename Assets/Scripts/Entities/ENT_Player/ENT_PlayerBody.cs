﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ENT_PlayerBody : ENT_DungeonEntity
{

    /*
    [FMODUnity.EventRef]
    public string TwigHit;
    */

    //
    //

    //Prototype stuff
    //This list keeps track of what type of remanats can be watered
    public List<monsterTypes> unlocked_types;

    public bool invulnerable = false;

    public bool LevelUp { set { levelUp = value; } get { return levelUp; } }
    bool levelUp = false;
    public GameObject levelUpEffect;
    public Material[] levelUpEffectColors;
    int colorIndex;
    ENT_DungeonEntity.monsterTypes floraTypeUnlocked; // set in SplicerController when flora type unlocked
	ENC_EndGameController endGameControl;

    public override void activate()
    {
        throw new NotImplementedException();
    }


    public override void Awake()
    {
        base.Awake();
        AIR_Respawn.SpawnPoint = transform.position;
        //Initialize unlocked_types with default list of basic minions.
        unlocked_types = new List<monsterTypes>();
        //unlocked_types.Add(monsterTypes.flowerMinion);
        //unlocked_types.Add(monsterTypes.mushroomMinion);
        //unlocked_types.Add(monsterTypes.treeMinion);
    }

    //
    // Use this for initialization
    //
    public override void Start ()
    {
        CL = CL_MAX;
        Purity = true;
		endGameControl = GameObject.Find("EndGameManager").GetComponent<ENC_EndGameController>();
        //healthBar = gameObject.GetComponentInChildren<UIX_CameraFacingBillboard_Minion>();
    }

    public override void Update()
    {
        base.Update();

        //Set in ENT_SplicerController when Flora Type is added to Player Skills
        if (LevelUp == true)
        {
            if(levelUpEffect != null) { 

                //turn on effect
                if(levelUpEffect.activeSelf == false)
                {
                    levelUpEffect.SetActive(true);
                }

                //set particle effect color
                switch (floraTypeUnlocked)
                {
                    //setLEvelUpEffectColors index for approriate minion color
                    case monsterTypes.treeMinion:
                        colorIndex = 0; 
                        break;
                    case monsterTypes.flowerMinion:
                        colorIndex = 1;
                        break;
                    case monsterTypes.mushroomMinion:
                        colorIndex = 2;
                        break;
                    case monsterTypes.confusionMinion:
                        colorIndex = 1;
                        break;
                    case monsterTypes.slowMinion:
                        colorIndex = 3;
                        break;
                    case monsterTypes.necroMinion:
                        colorIndex = 4;
                        break;
                    default:
                        break;
                }
                //activate all 3 particle effects for Player Level Up
                foreach (ParticleSystem ps in levelUpEffect.GetComponentsInChildren<ParticleSystem>())
                {
                    ps.gameObject.GetComponent<ParticleSystemRenderer>().material = levelUpEffectColors[colorIndex];
                    ps.Play();
                }
            }
            LevelUp = false;
        }
    }

    public void activateLevelUp(monsterTypes floratype)
    {
        LevelUp = true;
        floraTypeUnlocked = floratype;
    }


    public override void receive_combat_message(CBI_CombatMessenger combat_message)
    {
        if (combat_message.position == transform.position)
        {
            // Set direction to none
            UIX_DamageIndicator.setFlashDirection();
        }
        else
        {
            Vector3 vecOfImpact = combat_message.position - transform.position;
            Vector3 VecCam = Camera.main.transform.forward;
            VecCam.y = 0;
            vecOfImpact.y = 0;
            vecOfImpact = vecOfImpact.normalized;
            VecCam = VecCam.normalized;
            float angle = Mathf.Acos(Vector3.Dot(vecOfImpact, VecCam));
            Vector3 cross = Vector3.Cross(vecOfImpact, VecCam);
            if (cross.y > 0)
            {
                angle = angle * -1;
            }

            UIX_DamageIndicator.setFlashDirection(angle);
        }
        
        base.receive_combat_message(combat_message);
    }

    public void loseGame()
    {
        UIX_DeathFade.triggerFade = true;
        CL = CL_MAX;
        invulnerable = true;
        if (endGameControl.isStarted)
        {
            StartCoroutine(endGameControl.resetFinalBattle());
        }
    }
    //
    // Runs when player takes damage
    //
    public override void inflict_damage(float DC)
    {
        if (invulnerable)
            return;

        if (DBG_DebugOptions.Get().playerInvulnerable)
            return;

        CL -= DC;
        UIX_DamageIndicator.flash(DC);
        if(CL <= 0)
        {
            loseGame();
        }
    }

    public override void inflict_healing(float DC)
    {
        CL += DC;
    }



    //
    // Runs any time the players CL changes
    //
    public override void on_delta_clarity()
    {
        GameObject.Find("HealthBarFill").GetComponent<Image>().fillAmount = CL / CL_MAX;
    }
}
