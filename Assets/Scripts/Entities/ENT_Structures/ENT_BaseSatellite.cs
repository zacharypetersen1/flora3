﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_BaseSatellite : ENT_Structure {

    float timer = 0;

    //float effect_amount;

    // Use this for initialization
    public override void Start () {
        base.Start();
        State = "active";
    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (State == "cleansed")
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.02f, gameObject.transform.position.z);
            timer += 0.02f;
            if (timer > 6)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        State = "cleansed";
        rend.material = ghostMat;
    }
}
