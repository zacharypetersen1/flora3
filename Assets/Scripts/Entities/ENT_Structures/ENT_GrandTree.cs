﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ENT_GrandTree : ENT_Spawner {

	// Use this for initialization
	public override void Start () {
        base.Start();
        priority = 2; //Low priority is higher priority
        //CL_MAX = 100;
        CL = CL_MAX;
        State = "active";
        sightRange = 60;
        spawnLimit = 50;
        spawnRate = 3;
        min_spawn_distance = 3.37f;
        max_spawn_distance = 14f;
        timer = spawnRate; //Start the timer. This line will be moved to the activate function eventually
    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
	}

    public override void cleanse_self()
    {
        base.cleanse_self();
        Debug.Log("Grand Guardian cleansed.");
        //Will change VVV
        SceneManager.LoadScene("Victory_Screen");
    }
}
