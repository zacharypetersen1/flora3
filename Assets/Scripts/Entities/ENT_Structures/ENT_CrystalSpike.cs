﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_CrystalSpike : ENT_Structure {
    private bool playerIsTouching = false;
    private float Delta = 0f;
    private ENT_PlayerBody twigBody;

	// Use this for initialization
	public override void Start () {
        base.Start();
        State = "active";
        twigBody = twig.GetComponent<ENT_PlayerBody>();
        corrupt_self();
    }

    // Update is called once per frame
    public override void Update () {
        base.Update();
        if (State == "active")
        {
            if (playerIsTouching)
            {

                if (Delta <= 0)
                {
                    twigBody.CL -= 5;
                    Delta = 1;
                }
                Delta -= Time.deltaTime;
            }
        }
	}

    public void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            playerEnter();
        }
    }

    public void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            playerExit();
        }
    }

    public void playerEnter()
    {
        playerIsTouching = true;
    }

    public void playerExit()
    {
        playerIsTouching = false;
        Delta = 0;
    }

    public override void purify_self()
    {
        allyManager.removeStructure(gameObject);
        Destroy(gameObject);
    }
}
