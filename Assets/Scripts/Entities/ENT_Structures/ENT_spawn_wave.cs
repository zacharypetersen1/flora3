﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Invisible point that advanced spawners use for spawning waves
/// </summary>
public class ENT_spawn_wave : MonoBehaviour {
    //List matches indices to group sizes of enemies.
    public List<int> wave_list;
    public string minion_file;
    
    public void spawn(int wave_number)
    {
        try
        {
            int num_minions_for_first_wave = wave_list[wave_number];
            if (num_minions_for_first_wave <= 0)
            {
                return; //If the first group of the wave index is empty, skip it.
            }
        }
        catch
        {
            Debug.Log("This spawn point does not have that many waves!");
            return; //If the wave_number given is higher than the number of waves, report it then end.
        }

        //Spawn a bunch of minions around the point, then destroy the object
        for (int i = 0; i < wave_list[wave_number]; i++) 
        {
            float distance_offset = Random.Range(0.5f, 2); //Random Polar coords to spawn
            float radial_offset = Random.Range(0f, 360f);
            float x_offset = Mathf.Cos(radial_offset) * distance_offset;
            float y_offset = Mathf.Sin(radial_offset) * distance_offset;
            Vector2 spawn_offset = new Vector2(transform.position.x + x_offset, transform.position.y + y_offset);
            //Code for generating the monster object in game
            UTL_Resources.cloneAtLocation("Minions/" + minion_file, spawn_offset);
        }
        if (wave_number == wave_list.Count)
        {
            //Destroy the spawn_wave if we just spawned the final wave
            Destroy(gameObject);
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}