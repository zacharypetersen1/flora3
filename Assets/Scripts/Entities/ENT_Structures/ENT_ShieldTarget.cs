﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_ShieldTarget : ENT_Structure {

    public Vector3 setLocation;

    // Use this for initialization
    public override void Start () {
        base.Start();
	}

    public override void Awake()
    {
        base.Awake();
        State = "active";
    }

    public override void Update()
    {
        base.Update();
            
        if (State == "active")
        {
            cleanse_self();
            State = "cleansed";
            gameObject.transform.position = setLocation;
            var children = transform.GetComponentsInChildren<Transform>();
            foreach (Transform child in children)
            {
                if (child.name.Contains("Shield(Clone)"))
                {
                    //child.GetComponent<ParticleSystem>().Pause();
                    child.transform.position = gameObject.transform.position;
                    //child.GetComponent<ParticleSystem>().Play();
                }
            }
        }
    }
}
