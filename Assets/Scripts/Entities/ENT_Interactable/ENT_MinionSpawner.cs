﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_MinionSpawner : ENT_Interact
{

	// Use this for initialization
	public override void Start () {       
        base.Start();
        destroy_after_use = true;
        //allyManager = player.GetComponent<PLY_AllyManager>();
    }
	

    public override void activate()
    {
        base.activate();
        GameObject minion = UTL_Resources.cloneAtLocation("Minions/BasicAlly", gameObject.transform.position);
        minion.transform.position = new Vector3(minion.transform.position.x - 3, minion.transform.position.y, minion.transform.position.z);
        
    }
}
