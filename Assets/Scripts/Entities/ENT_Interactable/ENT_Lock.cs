﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Lock : ENT_Interact
{


    private PLY_InteractableManager interactableManager;
    private ENT_Structure parentStructure;
    public GameObject myMouse;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        interactableManager = player.GetComponent<PLY_InteractableManager>();
        parentStructure = gameObject.transform.parent.GetComponent<ENT_Structure>();
        //Prevents damage to structure while lock is active
        parentStructure.shields += 1;
    }

    public override void activate()
    {
        base.activate();
        if (interactableManager.hasKey())
        {
            interactableManager.Keys -= 1;
            setTriggerable(null);
            fui.hide();
            //Makes the structure vulnerable to damage
            parentStructure.shields -= 1;
            //Removes lock from structure
            Destroy(gameObject);
            //Kills and cleanses structure
            //parentStructure.CL = 0;
            parentStructure.cleanse_self();
        }
        else
        {
            myMouse.SetActive(true);
        }
    }
}
