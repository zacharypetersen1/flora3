﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Interact : MonoBehaviour {

    //player is near interable 
    //Change from static to something else later
    public static GameObject triggerable;
    //[HideInInspector]
    string pathToSpriteObj = "FloatingUI/InteractButton";
    public FUI_Default fui;
    public GameObject player;
    public bool destroy_after_use;
    public bool snapToTerrainPos = true;
    //
    // Use this for initialization
    //
    public virtual void Start()
    {
        player = GameObject.Find("Player");
        fui = UTL_Resources.cloneAsChild(pathToSpriteObj, player).GetComponent<FUI_Default>();
        fui.hide();
        if (snapToTerrainPos) transform.position = UTL_Dungeon.snapPositionToTerrain(transform.position);
        //print("fui " + fui);
    }

    // Update is called once per frame
    public virtual void Update () {
        if (triggerable == gameObject)
        {
            if (INP_PlayerInput.getButtonDown("Interact"))
            {
                //print("K is pressed");
                activate();
            }
        }
	}


    public virtual void setTriggerable(GameObject obj)
    {
        triggerable = obj;
    }

    //
    // Show interactable button prompt and make interactable triggerable
    //
    public virtual void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            setTriggerable(gameObject);
            fui.show();
        }
        //can_interact = true;
    }



    //
    // Hide interactable button prompt and make interactable untriggerable
    //
    public virtual void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            setTriggerable(null);
            fui.hide();
        }
        //can_interact = false;
    }


    public virtual void activate()
    {
        if (destroy_after_use)
        {
            setTriggerable(null);
            fui.hide();
            Destroy(gameObject);
        }
    }
}
