﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_SplicerEmitter : MonoBehaviour {

    public GameObject toEmit;
    public float dist = 60.0f;
    private ENT_SplicerEmissions[] emitter;
    private int numEmissions = 12;
    private GameObject player;
    private bool persist = true;

    bool areParticlesOn = true; //dont keep turning off particles
    public bool Persist
    {
        get { return persist; }
        set
        {
            persist = value;
        }
    }

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player");
        emitter = new ENT_SplicerEmissions[numEmissions];
        for (int i = 0; i != numEmissions; ++i)
        {
            emitter[i] = UTL_Resources.cloneAtLocation(toEmit, gameObject.transform.position).GetComponent<ENT_SplicerEmissions>();
            foreach (ParticleSystem ps in emitter[i].gameObject.GetComponentsInChildren<ParticleSystem>())
            {
                ps.Stop();
            }
        }
	}
	
    void ToggleParticles(bool isOn)
    {
        areParticlesOn = isOn;
        for (int i = 0; i < numEmissions; ++i)
        {
            foreach(ParticleSystem ps in emitter[i].gameObject.GetComponentsInChildren<ParticleSystem>())
            {
                if (!isOn)
                {
                    ps.Stop();
                }
                else
                {
                    ps.Play();

                }
            }          
        }
    }

    void TurnOnSomeParticles(int numToPlay)
    {
        for(int i = 0; i < numToPlay; ++i)
        {
            foreach (ParticleSystem ps in emitter[i].gameObject.GetComponentsInChildren<ParticleSystem>())
            {
                ps.Play();
            }
            emitter[i].launch();
        }
    }

	// Update is called once per frame
	void Update () {

		if (persist)
        {
            float cur_dist = Vector3.Distance(player.transform.position, gameObject.transform.position);
            if (cur_dist <= (dist * .25f) )
            {
                TurnOnSomeParticles(numEmissions);
                //persist = false;
                //turned off in SplicerController
            }
            else if (cur_dist <= (dist * .5f) )
            {
                TurnOnSomeParticles(numEmissions / 3);

            }
            else if (cur_dist <= (dist * .75f) )
            {
                TurnOnSomeParticles(numEmissions / 6);
            }
            else if (cur_dist <= dist) 
            {
                TurnOnSomeParticles(1);
            }
            else 
            {
                /*
                if (areParticlesOff)
                {
                    ToggleParticles(false);
                }
                */
            }
        }
        else
        {
            if (areParticlesOn)
            {
                ToggleParticles(false);
                foreach(var e in emitter)
                {
                    e.reset();
                }
            } 
        }
	}
}
