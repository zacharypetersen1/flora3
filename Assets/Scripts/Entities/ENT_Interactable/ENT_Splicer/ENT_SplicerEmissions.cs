﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_SplicerEmissions : MonoBehaviour {

    private GameObject player;
    private Vector3 startPos;
    private Vector3 endPos;
    private Vector3 center;
    [HideInInspector]
    public bool inFlight = false;
    private float flightTime;
    private float eta;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player");
        startPos = gameObject.transform.position + new Vector3(0,1,0);
    }
	
	// Update is called once per frame
	void Update () {
		if (inFlight)
        {
            Vector3 currentPos = gameObject.transform.position;
            Vector3 posRelCenter = currentPos - center;
            endPos = player.transform.position + new Vector3(0, 2, 0);
            Vector3 endRelCenter = endPos - center;
            eta = Vector3.Distance(currentPos, endPos);
            float fracComplete = (Time.time - flightTime) / eta;

            transform.position = Vector3.Slerp(posRelCenter, endRelCenter, .035f /*fracComplete*/);
            transform.position += center;
            if (fracComplete >= 1)
            {
                reset();
            }
        }
	}

    public void reset()
    {
        gameObject.transform.position = startPos;
        inFlight = false;
        TrailRenderer tr = gameObject.GetComponent<TrailRenderer>();
        if (tr)
        {
            tr.Clear();
        }
    }

    public void launch()
    {
        if (!inFlight)
        {
            endPos = player.transform.position + new Vector3(0,2,0);
            center = (endPos + startPos) * 0.5f;
            eta = Vector3.Distance(startPos, endPos);
            float randRange = eta * 0.025f;
            center += new Vector3(Random.Range(-randRange, randRange), -1, Random.Range(-randRange, randRange));
            flightTime = Time.time; //Time.time
            inFlight = true;
        }
    }
}
