﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_NecroAbility : ENT_MinionAbility
{
    public GameObject enemyEffect;
    public GameObject allyEffect;

    private ParticleSystem[] allyPS;
    private ParticleSystem[] enemyPS;

    public override void Awake()
    {
        base.Awake();

        //Particle effects removed to prevent CTD
        //setupParticleSystem();
        endAllyAbility();
        endEnemyAbility();
        
    }

    public void setupParticleSystem()
    {
        
        
        //get ally particlesystems
        allyEffect = UTL_Resources.cloneAsChild(allyEffect, gameObject);
        PSMeshRendererUpdater psMeshScript = allyEffect.GetComponent<PSMeshRendererUpdater>();
        psMeshScript.MeshObject = gameObject;
        psMeshScript.UpdateMeshEffect();

        allyPS = allyEffect.GetComponentsInChildren<ParticleSystem>();
        foreach (var ps in allyPS)
        {
            //print("turning off an ally particle system");
            ps.Stop();
        }


        //get enemy particlesystems
        enemyEffect = UTL_Resources.cloneAsChild(enemyEffect, gameObject);
        PSMeshRendererUpdater psMeshScript_1 = enemyEffect.GetComponent<PSMeshRendererUpdater>();
        psMeshScript_1.MeshObject = gameObject;
        psMeshScript_1.UpdateMeshEffect();

        enemyPS = enemyEffect.GetComponentsInChildren<ParticleSystem>();
        foreach (var ps in enemyPS)
        {
            //print("turning off an enemy particle system");
            ps.Stop();
        }
        
    }

    protected override void _allyAbility()
    {
        base._allyAbility();
        //Debug.Log("start");
        abilitySuccessful = gameObject.GetComponent<ENT_NecroMinion>().allyResurrect();
        if (abilitySuccessful && allyPS != null)
        {
            foreach (var ps in allyPS)
            {

                ps.Play();
            }
        }
    }

    protected override void _enemyAbility()
    {
        base._enemyAbility();
        //Debug.Log("start enemy ability");
        abilitySuccessful = gameObject.GetComponent<ENT_NecroMinion>().enemyResurrect();
        if (abilitySuccessful && enemyPS != null)
        {
            foreach (var ps in enemyPS)
            {

                ps.Play();
            }
        }
    }

    public override void endAllyAbility()
    {
        base.endAllyAbility();
        if (allyPS != null)
        {
            foreach (var ps in allyPS)
            {
                ps.Stop();
            }
        }
        //ENT_Body body = gameObject.GetComponent<ENT_Body>();
        //body.CurrentTree = body.LastTree;
    }

    public override void endEnemyAbility()
    {
        base.endEnemyAbility();
        if (enemyPS != null)
        {
            foreach (var ps in enemyPS)
            {
                ps.Stop();
            }
        }
        
    }
}
