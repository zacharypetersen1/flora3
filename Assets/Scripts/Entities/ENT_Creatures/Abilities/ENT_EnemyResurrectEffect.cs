﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_EnemyResurrectEffect : ENT_ResurrectParticleEffect
{
    // Use this for initialization
    public override void Start()
    {
        resurrectTimer = 3f;
    }

    // Update is called once per frame
    public override void Update()
    {
        if (resurrectTimer <= 0)
        {
            finishResurrect();
        }
        else
        {
            resurrectTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        }
    }

    public override void cancelResurrect()
    {
        finished = false;
        foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
        {
            if (ps != null) ps.Stop(); ps.Stop();
        }
        Destroy(gameObject, 2);
        resurrectTimer = 100f;

        if (myRemnant != null)
        {
            Collider col = myRemnant.GetComponent<Collider>();
            col.enabled = false;
            col.enabled = true;
        }
    }

    public override void finishResurrect()
    {
        finished = true;
        GameObject newMinion = UTL_Dungeon.spawnMinion(entity_type, transform.position, asAlly);
        myNecromancer.GetComponent<ENT_NecroMinion>().myResurrectedMinions.Add(newMinion);
        newMinion.GetComponent<ENT_Body>().isGhost = true;
        foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
        {
            if (ps != null) ps.Stop();
        }
        resurrectTimer = 100f;
        Destroy(gameObject, 2);
        Destroy(myRemnant);
    }
}
