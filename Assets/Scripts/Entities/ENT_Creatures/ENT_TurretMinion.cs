﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_TurretMinion : ENT_Body
{

    public float turretRange = 25;
    GameObject currentTarget;
    public GameObject CurrentTarget
    {
        get { return currentTarget; }
    }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void purify_self()
    {
        ENT_TurretAbility ability = GetComponent<ENT_TurretAbility>();
        if (isAlly)
        {
            ability.endAllyAbility();
        }
        else
        {
            ability.endEnemyAbility();
        }
        base.purify_self();
    }

    public GameObject setCurrentTarget()
    {
        currentTarget = allyManager.getNearestVisibleEnemy(transform.position, turretRange);
        return currentTarget;
    }

    public override bool isFacing(Vector3 pos)
    {
        //UPDATE THIS TO ROTATE TO ITS CURRENTTARGET
        return true;
    }
}
