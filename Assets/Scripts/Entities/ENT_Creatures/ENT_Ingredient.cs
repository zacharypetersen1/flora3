﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ENT_Ingredient : MonoBehaviour {

    GameObject splicerZone;

    //float timer = 1f;
    
    //use this to start the minion walking to the splicer
	public void activate(GameObject splicerObjective)
    {
        splicerZone = splicerObjective;
        gameObject.tag = "ingredient";
        ENT_Body body = gameObject.GetComponent<ENT_Body>();
        body.commandGroup.removeFromList(gameObject);
        body.vulnerable = false;
        body.enabled = false;
        gameObject.GetComponent<BehaviorExecutor>().enabled = false;
        gameObject.GetComponent<NavMeshAgent>().SetDestination(splicerZone.transform.position);
    }

    /*public void Update()
    {
        timer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (timer <= 0)
        {
            //gameObject.GetComponent<NavMeshAgent>().SetDestination(splicerZone.transform.position);
            timer = 1f;
        }
    }*/
}
