﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Remnant : MonoBehaviour {
    public ENT_DungeonEntity.monsterTypes entity_type;
    float remnantTimer = 500F;
    List<List<GameObject>> ListsImIn = new List<List<GameObject>>();
    List<List<GameObject>> removeList = new List<List<GameObject>>();
    [HideInInspector]
    public bool envRemnant; // true if the remnant/sprout is spawned in the environment rather than by a minion dying
    [HideInInspector]
    public ENV_SproutPatch myPatch;
    bool watered = false;
    public bool isGrowingStage1 = true;
    public bool isGrowingStage2 = false;
    float stage1CurTime = 0, stage1MaxTime = 0.6f;
    float stage2CurTime, stage2MaxTime, stage2MaxTimeHigh = 1, stage2MaxTimeLow = 0.7f;
    public static bool debugUnlockRez = false;
    [HideInInspector]
    public AIC_Zone myZone;
    public AnimationCurve stage1Curve;
    public AnimationCurve stage2Curve;

    //particle system to play when in range and can rez
    public ParticleSystem RezParticles;

    //particle system to play when this remnant type is unlocked
    public ParticleSystem UnlockedParticles;

    //Prototype section
    static protected GameObject player;
    public GameObject Player
    {
        get { return player; }
    }
    static protected PLY_AllyManager am;
    static protected ENT_PlayerBody playerBody;

    #region messageDicts

    static protected Dictionary<ENT_DungeonEntity.monsterTypes, bool> messageBeenPlayed = new Dictionary<ENT_DungeonEntity.monsterTypes, bool>()
    {
        {ENT_DungeonEntity.monsterTypes.treeMinion, false },
        {ENT_DungeonEntity.monsterTypes.flowerMinion, false },
        {ENT_DungeonEntity.monsterTypes.mushroomMinion, false },
        {ENT_DungeonEntity.monsterTypes.slowMinion, false },
        {ENT_DungeonEntity.monsterTypes.confusionMinion, false },
        {ENT_DungeonEntity.monsterTypes.necroMinion, false }
    };

    static protected Dictionary<ENT_DungeonEntity.monsterTypes, TUT_Messages.Messages> typeToMessage = new Dictionary<ENT_DungeonEntity.monsterTypes, TUT_Messages.Messages>()
    {
        {ENT_DungeonEntity.monsterTypes.treeMinion, TUT_Messages.Messages.TreeDesc },
        {ENT_DungeonEntity.monsterTypes.flowerMinion, TUT_Messages.Messages.FlowerDesc },
        {ENT_DungeonEntity.monsterTypes.mushroomMinion, TUT_Messages.Messages.MushroomDesc },
        {ENT_DungeonEntity.monsterTypes.necroMinion, TUT_Messages.Messages.Necrominion },
        {ENT_DungeonEntity.monsterTypes.slowMinion, TUT_Messages.Messages.SlowMinion },
        {ENT_DungeonEntity.monsterTypes.confusionMinion, TUT_Messages.Messages.ConfusionMinion },
    };
    #endregion

    public virtual void Start () {
        if (player == null)
        {
            player = GameObject.Find("Player");
        }
        if (am == null && player != null) am = player.GetComponent<PLY_AllyManager>();
        if (playerBody == null && player != null) playerBody = player.GetComponent<ENT_PlayerBody>();
        transform.localScale = Vector3.zero;
        setRandomRotation();
        myZone = AIC_ZoneManager.putRemnantInZone(gameObject);
        ShutOffParticles();
    }

    bool CanRez()
    {
        if (isGrowingStage2)
        {
            return false;
        }
#if UNITY_EDITOR
        if (debugUnlockRez)
        {
            return true;
        }
#endif
        return !am.isGroupMax();
    }

    public bool Rez()
    {
        if (CanRez())
        {
            if (playerBody.unlocked_types.Contains(entity_type))
            {
                if (!watered)
                {
                    bool result;
                    switch (entity_type)
                    {
                        case ENT_DungeonEntity.monsterTypes.confusionMinion:
                            result = !am.isConfusionMax();
                            break;
                        case ENT_DungeonEntity.monsterTypes.slowMinion:
                            result = !am.isSlowMax();
                            break;
                        case ENT_DungeonEntity.monsterTypes.necroMinion:
                            result = !am.isNecroMax();
                            break;
                        default:
                            result = true;
                            break;
                    }
                    if (result)
                    {
                        am.wateredCount++;
                        am.wateredCounts[entity_type]++;
                        watered = true;
                        isGrowingStage2 = true;
                        stage2CurTime = 0;
                        stage2MaxTime = Random.Range(stage2MaxTimeLow, stage2MaxTimeHigh);
                        StartCoroutine(WaitToRevive(stage2MaxTime));
                        PLY_SproutDetector.RemoveFromNearbySprouts(gameObject);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    IEnumerator WaitToRevive(float timer)
    {
        yield return new WaitForSeconds(timer);
        if (envRemnant)
        {
            ENV_SproutPatch.decrementGlobalSproutCount();
            if (myPatch != null)
            {
                myPatch.decrementSproutCount();
            }
        }
        spawnMinion();
        am.wateredCount--;
        am.wateredCounts[entity_type]--;
        playMessageFirstTime();
        Destroy(gameObject);
    }

    //controlled by entity_type
    void playMessageFirstTime()
    {
        if (!messageBeenPlayed[entity_type])
        {
            messageBeenPlayed[entity_type] = true;
            try
            {
                TUT_Messages.Messages messageToPlay = typeToMessage[entity_type];
                TUT_TutorialManager.queueMessage(typeToMessage[entity_type]);
            }
            catch
            {
                //this means that the message wasn't found in the dictionary
                Debug.LogWarning("No message found for " + entity_type+ "'s description");
            }
        }
    }

    void setSize(AnimationCurve curve, float curGrowTime, float maxGrowTime)
    {
        float growTimeNorm = curGrowTime / maxGrowTime;
        float growSize = curve.Evaluate(growTimeNorm);
        transform.localScale = Vector3.one * growSize;
    }

    void spawnMinion()
    {
        UTL_Dungeon.spawnMinion(entity_type,transform.position,true);
    }

    public void UpdateParticleSystem()
    {
        if (playerBody.unlocked_types.Contains(entity_type))
        {
            if (am.isGroupMax() || isGrowingStage2)
            {
                if (RezParticles.isPlaying)
                    RezParticles.Stop();
            }
            else
            {
                if (!RezParticles.isPlaying)
                    RezParticles.Play();
            }
            //if (PS.activeSelf && (am.isGroupMax() || growing))
            //{
            //    PS.GetComponent<ParticleSystem>().Stop();
            //    PS.SetActive(false);
            //}
            //else if (!PS.activeSelf && !am.isGroupMax())
            //{
            //    PS.SetActive(true);
            //    PS.GetComponent<ParticleSystem>().Play();
            //}
        }
    }

    public void ShutOffParticles()
    {
        RezParticles.Stop();
        //if (PS.activeSelf)
        //{
        //    PS.GetComponent<ParticleSystem>().Stop();
        //    PS.SetActive(false);
        //}
    }

    // Update is called once per frame
    public virtual void Update () {
        if (isGrowingStage1)
        {
            stage1CurTime = Mathf.Min(stage1CurTime + TME_Manager.getDeltaTime(TME_Time.type.environment), stage1MaxTime);
            setSize(stage1Curve, stage1CurTime, stage1MaxTime);
            if(stage1CurTime == stage1MaxTime)
            {
                isGrowingStage1 = false;
            }
        }
        else if (isGrowingStage2)
        {
            stage2CurTime = Mathf.Min(stage2CurTime + TME_Manager.getDeltaTime(TME_Time.type.environment), stage2MaxTime);
            setSize(stage2Curve, stage2CurTime, stage2MaxTime);
            if (stage2CurTime == stage2MaxTime)
            {
                isGrowingStage2 = false;
            }
        }
    }

    void setRandomRotation()
    {
        Quaternion rot = transform.rotation;
        Vector3 euler = rot.eulerAngles;
        euler.y = Random.Range(0f, 360f);
        rot.eulerAngles = euler;
        transform.rotation = rot;
    }

    public void revive(bool asAlly, GameObject necromancer)
    {
        GameObject resurrectEffect;
        if (asAlly)
        {
            resurrectEffect = UTL_Resources.cloneAtLocation("Particles/ally_Resurrect", transform.position);
        }
        else
        {
            resurrectEffect = UTL_Resources.cloneAtLocation("Particles/enemy_Resurrect", transform.position);
        }
             
        ENT_ResurrectParticleEffect effect = resurrectEffect.GetComponent<ENT_ResurrectParticleEffect>();
        necromancer.GetComponent<ENT_NecroMinion>().myResurectAbilities.Add(effect);
        effect.myNecromancer = necromancer;
        effect.myRemnant = gameObject;

        Quaternion rot = resurrectEffect.transform.rotation;
        rot.eulerAngles = new Vector3(90, 0, 0);
        resurrectEffect.transform.rotation = rot;
        effect.entity_type = entity_type;
        effect.asAlly = asAlly;
    }

    public void addToList(List<GameObject> newList)
    {
        if (!ListsImIn.Contains(newList))
        {
            newList.Add(gameObject);
            ListsImIn.Add(newList);
        }
    }

    public void removeFromList(List<GameObject> newList)
    {
        if (ListsImIn.Contains(newList))
        {
            newList.Remove(gameObject);
            ListsImIn.Remove(newList);
        }
    }

    public void removeFromAllListsExcept(List<GameObject> onlyList)
    {
        int listsImInCount = ListsImIn.Count;
        for (int i = 0; i < listsImInCount; ++i)
        {
            List<GameObject> list = ListsImIn[i];
            if (list != onlyList)
            {
                removeList.Add(list);
            }
        }
        int removeListCount = removeList.Count;
        for (int i = 0; i < removeListCount; ++i)
        {
            List<GameObject> toRemove = removeList[i];
            toRemove.Remove(gameObject);
            ListsImIn.Remove(toRemove);
        }
    }

    void OnDestroy()
    {
        List<List<GameObject>> removeList = new List<List<GameObject>>(ListsImIn);
        int removeListCount = removeList.Count;
        for (int i = 0; i < removeListCount; ++i)
        {
            List<GameObject> toRemove = removeList[i];
            removeFromList(toRemove);
        }
        myZone.removeRemnantFromZone(gameObject);
        PLY_SproutDetector.RemoveFromNearbySprouts(gameObject);
    }

    public void setFullyGrown()
    {
        setSize(stage1Curve, 1, 1);
        isGrowingStage1 = false;
        stage1CurTime = 0;
    }
}
