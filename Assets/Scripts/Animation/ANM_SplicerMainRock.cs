﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ANM_SplicerMainRock : MonoBehaviour
{

    Animator anim;
    GameObject player;
    public GameObject twigSpawnPoint;

    public float preDist = 70;
    public float dist = 50;
    private const float saveTime = 15; //save every 15 second if player is in range
    public float saveTimer = saveTime;

#if UNITY_EDITOR
    public string debugSaveFileName = "";
#endif


    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (saveTimer > 0) saveTimer = Mathf.Max(saveTimer - Time.deltaTime, 0);
        float curDist = Vector3.Distance(player.transform.position, transform.position);
        //Debug.Log(curDist);
        if (curDist <= preDist)
        {
            //Debug.Log("pre range");
            anim.SetTrigger("playerInPreRange");
            if (curDist <= dist)
            {
                //Debug.Log("range");
                anim.SetTrigger("playerInRange");
                if(gameObject != null)
                {
                    gameObject.GetComponent<ENT_SplicerEmitter>().enabled = true;
                }
                if (twigSpawnPoint != null)
                {
                    AIR_Respawn.SpawnPoint = twigSpawnPoint.transform.position;
                    if (saveTimer <= 0.01f)
                    {
                        SaveLoad.Save();
                        saveTimer = saveTime;

#if UNITY_EDITOR
                        if (DBG_DebugOptions.Get().createDebugSaves)
                            DebugSave();
#endif
                    }
                    
                }
            }
        }
    }

#if UNITY_EDITOR
    string GetSaveFileName()
    {
        if (debugSaveFileName.Contains(".flora"))
            return debugSaveFileName;
        else if (debugSaveFileName.Length > 0)
        {
            string saveFileName = debugSaveFileName + ".flora";
            return saveFileName;
        }
        else
            return "debugSave.flora";
    }

    void DebugSave()
    {
        SaveLoad.Save(GetSaveFileName());
    }
#endif
}
