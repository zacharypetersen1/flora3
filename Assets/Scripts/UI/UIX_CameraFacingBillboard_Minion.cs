﻿// Ben, Matt, Zach

/*
 * DEPRECATED-- USE NEW HEALTH BARS IN ABI_CLEANSING
 */
using UnityEngine;
using System.Collections;

public class UIX_CameraFacingBillboard_Minion : MonoBehaviour
{
    public Camera m_Camera;
    public RectTransform healthBarObject;

    public GameObject brainObject;
    private AIC_Brain brain;

    void Start()
    {
        brain = gameObject.GetComponentInParent<AIC_Brain>();
        //CL = gameObject.GetComponentInChildren<RectTransform>();
        if (m_Camera == null)
        {
            m_Camera = GameObject.Find("Camera").GetComponent<Camera>();
        }
        // brain.MaxHp = 100;
        // brain.Health = 50;
    }

    void Update()
    {
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
            m_Camera.transform.rotation * Vector3.up);
        //print("Script attached to: " + gameObject.name);
        //updateHealthBar(brain.MaxCL, brain.cl);
    }

    public void updateHealthBar(float maxHealth, float currentHealth)
    {
        // normalize
        healthBarObject.sizeDelta = new Vector2((currentHealth / maxHealth) * 100, 100);

    }

}