﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIX_MinionCounter : MonoBehaviour {
    public TextMeshProUGUI followerText;
    public TextMeshProUGUI treeText;
    public TextMeshProUGUI mushroomText;
    public TextMeshProUGUI flowerText;
    public TextMeshProUGUI confusionText;
    public TextMeshProUGUI slowText;
    public TextMeshProUGUI necroText;

    public Image followerImage;
    public Image treeImage;
    public Image flowerImage;
    public Image mushroomImage;
    public Image confusionImage;
    public Image slowImage;
    public Image necroImage;

    [HideInInspector]
    public static List<fadeData> typeCounterList = new List<fadeData>();

    private int _allyCount;
    private int _treeCount;
    private int _flowerCount;
    private int _mushroomCount;
    private int _confusionCount;
    private int _slowCount;
    private int _necroCount;
    private int _maxFollowers;
    private int _maxSlow;
    private int _maxConfusion;
    private int _maxNecro;
    public float displayTime = 3f; //time to show counter and image for
    private float inverseFadeTime;
    public float fadeTime = 0.5f;
    float alph = 0;

    float textDefaultX = -162f;
    float textDefaultY = 80f;

    float imageDefaultX = -35f;
    float imageDefaultY = 80f;

    public enum FadeState { hidden, fadeIn, visible, fadeOut };
    
    [HideInInspector]
    public class fadeData
    {
        TextMeshProUGUI textObj;
        public TextMeshProUGUI TextObj
        {
            get { return textObj; }
        }

        Image imageObj;
        public Image ImageObj
        {
            get { return imageObj; }
        }

        public float fadeTime ;
        float alph ;
        float displayTime;

        public fadeData(TextMeshProUGUI text, Image image, float fade = 0.5f, float alpha = 0f, float display = 3f)
        {
            textObj = text;
            imageObj = image;
            fadeTime = fade;
            alph = alpha;
            displayTime = display;
        }

        private FadeState _state;
        public FadeState state {
            get { return _state; }
            set { _state = value;
                fadeTimer = 0;
                stayTimer = 0;
            }
        }
        public float fadeTimer;
        public float stayTimer;

        public void setAlpha(float alpha)
        {
            textObj.alpha = alpha;
            Color tempColor = textObj.color;
            tempColor.a = alpha;
            textObj.color = tempColor;
            imageObj.color = tempColor;
        }

        public void update()
        {
            switch (state)
            {
                case FadeState.fadeIn:
                    fadeTimer = Mathf.Clamp(fadeTimer + Time.deltaTime, 0, fadeTime);
                    alph = UTL_Math.mapToNewRange(fadeTimer, 0, fadeTime, 0, 1);
                    setAlpha(alph);
                    if (fadeTimer >= fadeTime)
                    {
                        state = FadeState.visible;
                    }
                    break;
                case FadeState.fadeOut:
                    fadeTimer = Mathf.Clamp(fadeTimer + Time.deltaTime, 0, fadeTime);
                    alph = UTL_Math.mapToNewRange(fadeTimer, 0, fadeTime, 0, 1);
                    setAlpha(1 - alph);
                    if (fadeTimer >= fadeTime)
                    {
                        state = FadeState.hidden;
                        typeCounterList.Remove(this);
                    }
                    break;
                case FadeState.visible:
                    if (displayTime < 0)
                        break;
                    stayTimer = Mathf.Clamp(stayTimer + Time.deltaTime, 0, displayTime);
                    if (stayTimer >= displayTime)
                    {
                        state = FadeState.fadeOut;
                    }
                    break;
                default:
                    break;
            }
        }
        
    }
    
    //private Coroutine _fadeIn;
    //private Coroutine _fadeOut;

    public int allyCount
    {
        get { return _allyCount; }
        set { _allyCount = value;
            followerText.text = _allyCount + "/" + _maxFollowers;
            if (_maxFollowers > 0 && (dataAllyCount.state == FadeState.hidden || dataAllyCount.state == FadeState.fadeOut))
                dataAllyCount.state = FadeState.fadeIn;
        }
    }

    public int treeCount
    {
        get { return _treeCount; }
        set { _treeCount = value;
			treeText.text = _treeCount < 0 ? "0" : _treeCount.ToString();
            if (_treeCount > 0)
            {
                dataTreeCount.state = FadeState.fadeIn;
                if (!typeCounterList.Contains(dataTreeCount))
                    typeCounterList.Add(dataTreeCount);
            }
        }
    }
    public int flowerCount
    {
        get { return _flowerCount; }
        set { _flowerCount = value;
			flowerText.text = _flowerCount < 0 ? "0" : _flowerCount.ToString();
            if (_flowerCount > 0)
            {
                dataFlowerCount.state = FadeState.fadeIn;
                if (!typeCounterList.Contains(dataFlowerCount))
                    typeCounterList.Add(dataFlowerCount);
            }
        }
    }
    public int mushroomCount
    {
        get { return _mushroomCount; }
        set { _mushroomCount = value;
			mushroomText.text = _mushroomCount < 0 ? "0" : _mushroomCount.ToString();
            if (_mushroomCount > 0)
            {
                dataMushroomCount.state = FadeState.fadeIn;
                if (!typeCounterList.Contains(dataMushroomCount))
                    typeCounterList.Add(dataMushroomCount);
            }
        }
    }
    public int confusionCount
    {
        get { return _confusionCount; }
        set { _confusionCount = value;
			confusionText.text = (_confusionCount < 0 ? "0" :_confusionCount.ToString() ) + "/" + _maxConfusion;
            if (_confusionCount > 0)
            {
                dataConfusionCount.state = FadeState.fadeIn;
                if (!typeCounterList.Contains(dataConfusionCount))
                    typeCounterList.Add(dataConfusionCount);
            }
        }
    }
    public int slowCount
    {
        get { return _slowCount; }
        set { _slowCount = value;
			slowText.text = ( _slowCount < 0 ? "0" : _slowCount.ToString() ) + "/" + _maxSlow;
            if (_slowCount > 0)
            {
                dataSlowCount.state = FadeState.fadeIn;
                if (!typeCounterList.Contains(dataSlowCount))
                    typeCounterList.Add(dataSlowCount);
            }
        }
    }
    public int necroCount
    {
        get { return _necroCount; }
        set { _necroCount = value;
			necroText.text = ( _necroCount < 0 ? "0" :_necroCount.ToString() ) + "/" + _maxNecro;
            if (_necroCount > 0)
            {
                dataNecroCount.state = FadeState.fadeIn;
                if (!typeCounterList.Contains(dataNecroCount))
                    typeCounterList.Add(dataNecroCount);
            }
        }
    }

    public int maxFollowers
    {
        get { return _maxFollowers; }
        set { _maxFollowers = value;
            if (maxFollowers > 0)
            {
				followerText.text = ( _allyCount < 0 ? "0" : _allyCount.ToString() ) + "/" + _maxFollowers;
                if (dataAllyCount.state == FadeState.hidden || dataAllyCount.state == FadeState.fadeOut)
                    dataAllyCount.state = FadeState.fadeIn;
            }
        }
    }

    public int maxSlow
    {
        get { return _maxSlow; }
        set { _maxSlow = value;
            if (_maxSlow > 0)
            {
                slowText.text = _slowCount + "/" + _maxSlow;
                dataSlowCount.state = FadeState.fadeIn;
            }
        }
    }

    public int maxConfusion
    {
        get { return _maxConfusion; }
        set
        {
            _maxConfusion = value;
            if (_maxConfusion > 0)
            {
                confusionText.text = _confusionCount + "/" + _maxConfusion;
                dataConfusionCount.state = FadeState.fadeIn;
            }
        }
    }

    public int maxNecro
    {
        get { return _maxNecro; }
        set
        {
            _maxNecro = value;
            if (_maxNecro > 0)
            {
                necroText.text = _necroCount + "/" + _maxNecro;
                dataNecroCount.state = FadeState.fadeIn;
            }
        }
    }

    fadeData dataAllyCount;
    fadeData dataTreeCount;
    fadeData dataFlowerCount;
    fadeData dataMushroomCount;
    fadeData dataSlowCount;
    fadeData dataConfusionCount;
    fadeData dataNecroCount;
    // Use this for initialization
    void Start () {
		if (followerText == null)
        {
            followerText = GetComponent<TextMeshProUGUI>();
        }
        if (followerImage == null)
        {
            try
            {
                GameObject tempObj = GameObject.Find("PlayerUI");
                followerImage = tempObj.transform.Find("MaxFollowersIcon").GetComponent<Image>();
            }
            catch (Exception e)
            {
                Debug.LogError("MaxFollowersIcon Image not found");
                throw e;
            }
        }
        typeCounterList = new List<fadeData>(); //clear on restart
        dataAllyCount = new fadeData(followerText, followerImage, 0.5f, 0, -1.0f); //never fade out ally count
        dataTreeCount = new fadeData(treeText, treeImage);
        dataFlowerCount = new fadeData(flowerText, flowerImage);
        dataMushroomCount = new fadeData(mushroomText, mushroomImage);
        dataSlowCount = new fadeData(slowText, slowImage);
        dataConfusionCount = new fadeData(confusionText, confusionImage);
        dataNecroCount = new fadeData(necroText, necroImage);
        setup();
	}

    void setup()
    {
        dataAllyCount.state = FadeState.hidden;
        dataAllyCount.setAlpha(0);
        dataTreeCount.state = FadeState.hidden;
        dataTreeCount.setAlpha(0);
        dataFlowerCount.state = FadeState.hidden;
        dataFlowerCount.setAlpha(0);
        dataMushroomCount.state = FadeState.hidden;
        dataMushroomCount.setAlpha(0);
        dataSlowCount.state = FadeState.hidden;
        dataSlowCount.setAlpha(0);
        dataConfusionCount.state = FadeState.hidden;
        dataConfusionCount.setAlpha(0);
        dataNecroCount.state = FadeState.hidden;
        dataNecroCount.setAlpha(0);

        //make invisible at start
        inverseFadeTime = 1 / fadeTime;
    }
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < typeCounterList.Count; ++i)
        {
            setheight(typeCounterList[i], i + 1);
        }

        if (_maxFollowers == 0) return;
        dataAllyCount.update();
        dataTreeCount.update();
        dataFlowerCount.update();
        dataMushroomCount.update();
        dataSlowCount.update();
        dataConfusionCount.update();
        dataNecroCount.update();
    }

    public void setheight(fadeData data, int index)
    {
        //Debug.Log(index);
        data.TextObj.GetComponent<RectTransform>().anchoredPosition = new Vector3(textDefaultX, textDefaultY + ((index - 1) * 40), 0);
        data.ImageObj.GetComponent<RectTransform>().anchoredPosition = new Vector3(imageDefaultX, imageDefaultY + ((index - 1) * 40), 0);

    }

    /*IEnumerator fadeIn(float fadeTime)
    {
        float alpha = followerText.alpha;
        Color imgColor = followerImage.color;
        float step = 1 / fadeTime;
        while (alpha < 1F)
        {
            alpha = Mathf.Clamp01(alpha+step);
            followerText.alpha = alpha;
            imgColor.a = alpha;
            followerImage.color = imgColor;
            yield return null;
        }
        _fadeOut = StartCoroutine(fadeOut(fadeTime));
        StopCoroutine(_fadeIn);
    }

    IEnumerator fadeOut(float fadeTime)
    {
        yield return new WaitForSeconds(displayTime);
        float alpha = followerText.alpha;
        Color imgColor = followerImage.color;
        float step = 1 / fadeTime;
        while (alpha > 0F)
        {
            alpha = Mathf.Clamp01(alpha - step);
            followerText.alpha = alpha;
            imgColor.a = alpha;
            followerImage.color = imgColor;
            yield return null;
        }
        StopCoroutine(_fadeOut);
    }*/
}
