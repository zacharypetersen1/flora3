﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIX_ImageSwapper : MonoBehaviour {

    public Sprite spr1 = null;
    public Sprite spr2 = null;
    bool isSpr1 = true;
    float curTime = 0;
    public float swapTime = 0.5f;
    Image img;

	// Use this for initialization
	void Start () {
        img = gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        curTime += Time.deltaTime;
        if(curTime > swapTime)
        {
            isSpr1 = !isSpr1;
            img.sprite = isSpr1 ? spr1 : spr2;
            curTime -= swapTime;
        }
	}
}
