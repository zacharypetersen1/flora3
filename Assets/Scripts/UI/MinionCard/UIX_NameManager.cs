﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UIX_NameManager{

    class randStringList
    {
        List<string> strings;
        int index;

        public randStringList(List<string> setStrings)
        {
            strings = setStrings;
            strings.Shuffle();
        }

        public void loadFromSave(List<string> setStrings, int setIndex)
        {
            strings = setStrings;
            index = setIndex;
        }

        public string getString()
        {
            string ret = strings[index];
            incriment();
            return ret;
        }

        void incriment()
        {
            index++;
            if(index == strings.Count)
            {
                strings.Shuffle();
                index = 0;
            }
        }

        public int stringsLeft()
        {
            return strings.Count - index;
        }
    }

    static randStringList genNames = new randStringList(new List<string>()
    {
        "GenName1",
        "GenName2",
        "GenName3",
        "GenName4",
        "GenName5",
        "GenName6",
        "GenName7",
        "GenName8",
        "GenName9"
    });

    static Dictionary<ENT_Body.monsterTypes, randStringList> minionNames = new Dictionary<ENT_DungeonEntity.monsterTypes, randStringList>()
    {
        {
            ENT_Body.monsterTypes.treeMinion,
            new randStringList(new List<string>() {
                "TreeName1",
                "TreeName2",
                "TreeName3"
            })
        },
        {
            ENT_Body.monsterTypes.flowerMinion,
            new randStringList(new List<string>() {
                "flowerName1",
                "flowerName2",
                "flowerName3"
            })
        },
        {
            ENT_Body.monsterTypes.mushroomMinion,
            new randStringList(new List<string>() {
                "mushroomName1",
                "mushroomName2",
                "mushroomName3"
            })
        },
        {
            ENT_Body.monsterTypes.confusionMinion,
            new randStringList(new List<string>() {
                "confusionName1",
                "confusionName2",
                "confusionName3"
            })
        },
        {
            ENT_Body.monsterTypes.necroMinion,
            new randStringList(new List<string>() {
                "necroName1",
                "necroName2",
                "necroName3"
            })
        },
        {
            ENT_Body.monsterTypes.slowMinion,
            new randStringList(new List<string>() {
                "slowName1",
                "slowName2",
                "slowName3"
            })
        },
    };
    
    static randStringList genModifiers = new randStringList(new List<string>()
    {
        "the Gen1",
        "the Gen2",
        "the Gen3",
        "the Gen4",
        "the Gen5",
        "the Gen6",
        "the Gen7",
        "the Gen8",
        "the Gen9"
    });

    static Dictionary<ENT_Body.monsterTypes, randStringList> minionModifiers = new Dictionary<ENT_DungeonEntity.monsterTypes, randStringList>()
    {
        {
            ENT_Body.monsterTypes.treeMinion,
            new randStringList(new List<string>() {
                "the Tree1",
                "the Tree2",
                "the Tree3"
            })
        },
        {
            ENT_Body.monsterTypes.flowerMinion,
            new randStringList(new List<string>() {
                "the flower1",
                "the flower2",
                "the flower3"
            })
        },
        {
            ENT_Body.monsterTypes.mushroomMinion,
            new randStringList(new List<string>() {
                "the mushroom1",
                "the mushroom2",
                "the mushroom3"
            })
        },
        {
            ENT_Body.monsterTypes.confusionMinion,
            new randStringList(new List<string>() {
                "the confusion1",
                "the confusion2",
                "the confusion3"
            })
        },
        {
            ENT_Body.monsterTypes.necroMinion,
            new randStringList(new List<string>() {
                "the necro1",
                "the necro2",
                "the necro3"
            })
        },
        {
            ENT_Body.monsterTypes.slowMinion,
            new randStringList(new List<string>() {
                "the slow1",
                "the slow2",
                "the slow3"
            })
        },
    };

    static string selectString(randStringList a, randStringList b)
    {
        float val = (float)a.stringsLeft() / (a.stringsLeft() + b.stringsLeft());
        if(Random.value <= val)
        {
            return a.getString();
        }
        else
        {
            return b.getString();
        }
    }

    // Generates a name for a minion
    public static string generateName(ENT_Body.monsterTypes t)
    {
        // Select between general and minion-specific name
        string newName = selectString(genNames, minionNames[t]);

        if (!newName.Contains(" "))
        {
            newName += " " + selectString(genModifiers, minionModifiers[t]);
        }
        return newName;
    }

    // Incriments the index that we are on in a list
    static void incriment(ref int index, List<string> values)
    {
        ++index;
        if (index == values.Count)
        {
            values.Shuffle();
            index = 0;
        }
    }
}

static class ListUtil
{
    public static void Swap<T>(this IList<T> list, int i, int j)
    {
        var temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        for (var i = 0; i < list.Count; i++)
            list.Swap(i, Random.Range(i, list.Count-1));
    }
}