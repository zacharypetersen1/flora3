﻿// Tutorial from Asbjorn Thirslund

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIX_Screen_Fade : MonoBehaviour
{
    private int drawDepth = -1000; // draw hierarchy
    private float alpha = 1.0f;
    private int fadeDir = -1; //-1= fade in, vice versa

    public Texture2D fadeOutTexture; //overlays screen
    public float fadeSpeed = 0.8f; //fade speed

    //bool fade = false;

    void OnGUI()
    {
        //fade out/in the alpha w/direction/speed/
        //Time.delatime to convert the operation to seconds
        alpha += fadeDir * fadeSpeed * Time.deltaTime;
        //force between 0/1 for GUI.color 
        alpha = Mathf.Clamp01(alpha);

        //set GUI color && alpha to it's variable
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha); //set alpha value
        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture); //draw textire fit to entire screen area
    }
    public float BeginFade(int direction)
    {
        fadeDir = direction;
        return (fadeSpeed); //return fadeSpeed variable for Application.LoadLevel();
    }

    // Depricated according to Unity
    /*void OnLevelWasLoaded() //if changed to SceneManager.sceneLoaded, change function name
    {
        //alpha = 1 //if alpha not set to 1 by default
        BeginFade(-1); //call fade in function
    }*/
}