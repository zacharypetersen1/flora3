﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FUI_HealthBars : FUI_FloatingUI {

    //public  GameObject healthBarObject;
    //private GameObject brainObject;
    //private AIC_Brain brain;
    public GameObject fui_healthBar;
    public Material second_material;

    public void updateHealthBar(float maxHealth, float currentHealth)
    {
        // normalize
        //healthBarObject.sizeDelta = new Vector2((currentHealth / maxHealth) * 100, 100);

    }

    public override void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    public override void Start()
    {
        fui = (GameObject)MonoBehaviour.Instantiate(Resources.Load("FloatingUI/maxHealthBar"));
        fui_healthBar = (GameObject)MonoBehaviour.Instantiate(Resources.Load("FloatingUI/healthBar"));
        setOffset(fui);
        setOffset(fui_healthBar);
        fui.GetComponent<Renderer>().material = material;
        fui_healthBar.GetComponent<Renderer>().material = material;
        hide();
        //hide(fui_healthBar);
    }

    public void Update()
    {
        base.update();
        fui_healthBar.transform.LookAt(fui.transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
        fui_healthBar.transform.Rotate(new Vector3(-90, 0, 0));
        setOffset(fui_healthBar);
        //updateHealthBar();
    }
}
