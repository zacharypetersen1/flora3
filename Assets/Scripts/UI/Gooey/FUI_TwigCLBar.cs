﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FUI_TwigCLBar : FUI_PurityBar {

    private ENT_PlayerBody playerBody;

    protected override void Start()
    {
        base.Start();
        playerBody = GameObject.Find("Player").GetComponent<ENT_PlayerBody>();
    }

    protected override void Update()
    {
        subUpdateNoCLNoOffset();
        scaleCLToValue(playerBody.CL, playerBody.CL_MAX);
    }
}
