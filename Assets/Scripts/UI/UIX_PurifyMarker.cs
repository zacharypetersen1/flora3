﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_PurifyMarker : MonoBehaviour {

    public ParticleSystem flowerPurfiySystem;
    public ParticleSystem mushroomPurifySystem;
    public ParticleSystem treePurifySystem;
    public ParticleSystem confusionPurifySystem;
    public ParticleSystem slowPurifySystem;
    public ParticleSystem necroPurifySystem;



    static UIX_PurifyMarker purifyMarker;

	// Use this for initialization
	void Start () {
        //system = GetComponent<ParticleSystem>();
        purifyMarker = this;
	}



    //
    // 
    //
    public static void markPurify(ENT_DungeonEntity.monsterTypes entity_type)
    {
        switch (entity_type)
        {
            case ENT_DungeonEntity.monsterTypes.flowerMinion:
                purifyMarker.flowerPurfiySystem.Emit(1);
                break;
            case ENT_DungeonEntity.monsterTypes.treeMinion:
                purifyMarker.treePurifySystem.Emit(1);
                break;
            case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                purifyMarker.mushroomPurifySystem.Emit(1);
                break;
            case ENT_DungeonEntity.monsterTypes.confusionMinion:
                purifyMarker.confusionPurifySystem.Emit(1);
                break;
            case ENT_DungeonEntity.monsterTypes.slowMinion:
                purifyMarker.slowPurifySystem.Emit(1);
                break;
            case ENT_DungeonEntity.monsterTypes.necroMinion:
                purifyMarker.necroPurifySystem.Emit(1);
                break;
        }
        
    }
	
}
