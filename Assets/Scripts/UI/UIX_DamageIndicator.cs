﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIX_DamageIndicator : MonoBehaviour {

    public enum direction
    {
        center, left, right, up, down
    }
    Image centerImage, leftImage, rightImage, upImage, downImage;
    Dictionary<direction, Image> images;
    direction lastDirection = direction.center;
    float decayRate = 0.022f;
    static UIX_DamageIndicator singleton;
    bool isEnabled = true;

    // Use this for initialization
    void Start () {

        centerImage = GameObject.Find("FlashCenter").GetComponent<Image>();
        rightImage = GameObject.Find("FlashRight").GetComponent<Image>();
        leftImage = GameObject.Find("FlashLeft").GetComponent<Image>();
        upImage = GameObject.Find("FlashUp").GetComponent<Image>();
        downImage = GameObject.Find("FlashDown").GetComponent<Image>();
        images = new Dictionary<direction, Image>
        {
            {direction.right, rightImage },
            {direction.left, leftImage },
            {direction.up, upImage },
            {direction.down, downImage },
            {direction.center, centerImage }
        };
        singleton = this;
    }

    public static void flash(float dc)
    {
        if (!singleton.isEnabled) return;
        Color col = singleton.images[singleton.lastDirection].color;
        col.a = Mathf.Clamp01(col.a + .75f);
        singleton.images[singleton.lastDirection].color = col;
    }

    //angle is in radians
    public static void setFlashDirection(float angle = 1000f)
    {
        if (angle == 1000f)
        {
            singleton.lastDirection = direction.center;
            return;
        }
        
        //convert to degrees
        angle = angle * 180/Mathf.PI;

        //Clamp vector to a cardinal direction
        if (angle <= 45 && angle > -45)
        {
            singleton.lastDirection = direction.up;
        } else if (angle > 45 && angle <= 135)
        {
            singleton.lastDirection = direction.right;
        }
        else if (angle > 135 || angle <= -135)
        {
            singleton.lastDirection = direction.down;
        }
        else if (angle > -135 && angle <= -45)
        {
            singleton.lastDirection = direction.left;
        }
    }



    //
    // Resets all alpha values to 0
    //
    public static void resetFlashes()
    {
        foreach (var img in singleton.images.Values)
        {
            Color col = img.color;
            col.a = 0;
            img.color = col;
        }
    }



    // Update is called once per frame
    void Update () {
        
        foreach(var img in images.Values)
        {
            Color col = img.color;
            col.a = Mathf.Clamp01(col.a - decayRate);
            img.color = col;
        }
	}



    //
    // Sets enabled state of damage indicator
    //
    public static void setEnabled(bool value)
    {
        singleton.isEnabled = value;
    }
}
