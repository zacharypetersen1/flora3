﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIX_NumIcons : MonoBehaviour {

    public GameObject fillObj;
    public GameObject grayObj;

    Image fill;
    Image gray;

    ENT_Body.monsterTypes type;

    SelectionManager selectionManager;

    float percentFill = 0f;

    ENT_MinionAbility ability;

	// Use this for initialization
	void Start () {
        fill = fillObj.GetComponent<Image>();
        gray = grayObj.GetComponent<Image>();
        selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>() ;
        type = gameObject.GetComponent<ENT_Body>().entity_type;
        ability = gameObject.GetComponent<ENT_MinionAbility>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!SelectionManager.isInSelectMode || selectionManager.isSelected(gameObject))
        {
            fillObj.SetActive(true);
            grayObj.SetActive(true);
            percentFill = ability.AllyCoolDownTimer / ability.allyMaxCoolDown;
            fill.fillAmount = 1 - percentFill;
            gray.fillAmount = percentFill;
        } else
        {
            fillObj.SetActive(false);
            grayObj.SetActive(false);
        }
	}
}
