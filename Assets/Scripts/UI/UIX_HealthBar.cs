﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_HealthBar : MonoBehaviour {
    UIX_Bar bar;
    //UIX_Bar purityBar;
    ENT_Body body;
    GameObject minion;

    bool showHealth = true;

    PLY_AllyManager allyMan;
    int allyCount;
    int enemyCount;

    bool currentPurity;

    SpriteRenderer healthRend;
    SpriteRenderer CLRend;

    // Use this for initialization
    void Start () {
        allyMan = GameObject.Find("Player").GetComponent<PLY_AllyManager>();
        allyCount = allyMan.getAllAllies.Count;
        enemyCount = allyMan.getAllEnemies.Count;
        bar = GetComponent<UIX_Bar>();
        //purityBar = transform.parent.transform.GetChild(1).GetComponent<UIX_Bar>();
        body = GetComponentInParent<ENT_Body>();
        currentPurity = body.isAlly;
        minion = transform.parent.parent.gameObject;

        GameObject healthBar = transform.parent.gameObject;
        healthRend = healthBar.GetComponent<SpriteRenderer>();
        CLRend = gameObject.GetComponent<SpriteRenderer>();

        if (body == null)
        {
            Debug.LogError("health bar not set up right on " + gameObject.transform.parent.transform.parent.name);
        }
        else
        {
            bar.MaxValue = body.CL_MAX;
            bar.CurrentValue = body.CL;
            //purityBar.MaxValue = body.CL_MAX;
        }
	}
	
    public void toggleBar(bool isEnable)
    {
        showHealth = isEnable;
        toggleHealthVisible();
    }

	// Update is called once per frame
	void Update () {
        bar.CurrentValue = body.CL;
        //purityBar.CurrentValue = body.purityValue;
        /*
        if (INP_PlayerInput.getButtonDown("ToggleHealth"))
        {
            showHealth = !showHealth;
            toggleHealthVisible();
        }*/
        if(currentPurity != body.isAlly)
        {
            toggleHealthVisible();
            currentPurity = body.isAlly;
        }
        if (body.Purity)
        {
            //purityBar.gameObject.SetActive(false);
        }
        
    }

    void toggleHealthVisible()
    {
        if (showHealth)
        {
            healthRend.enabled = true;
            CLRend.enabled = true;
        }
        else
        {
            healthRend.enabled = false;
            CLRend.enabled = false;
        }
        //healthRend.color = healthColor;
        //CLRend.color = CLColor;
        //purityRend.color = purityColor;
    }
}
