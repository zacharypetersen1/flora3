﻿// Ben & Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_Crosshair : MonoBehaviour {
    // Camera
    Camera worldCamera;
    Vector3 middleScreen;
    CAM_ScreenResize camResize;

    // Ray
    Ray ray;
    RaycastHit hit;
    public static float maxDistance = 750;
    public static LayerMask layermask;

    // Target
    GameObject crosshair;
    Renderer crosshairRend;
    Texture[] textures;
    string _target = "invalid";
    Vector3 _targetPos;
    GameObject _targetObject;

    public string target
    {
        get { return _target; }
    }

    public Vector3 targetPos
    {
        get { return _targetPos; }
    }

    public GameObject targetObject
    {
        get { return _targetObject; }
    }

    // Use this for initialization
    void Start() {
        layermask = (1 << 11) | (1 << 12) | (1 << 13) | (1 << 15);
        worldCamera = GameObject.Find("Camera").GetComponent<Camera>();
        middleScreen = new Vector3(worldCamera.pixelWidth / 2, worldCamera.pixelHeight / 2, worldCamera.nearClipPlane);
        camResize = GameObject.Find("Scripts").GetComponent<CAM_ScreenResize>();

        textures = new Texture[15];
        textures[0] = Resources.Load("UI/Textures/crosshair_dot1") as Texture;
        textures[1] = Resources.Load("UI/Textures/crosshair_dot2") as Texture;
        textures[2] = Resources.Load("UI/Textures/crosshair_X1") as Texture;
        textures[3] = Resources.Load("UI/Textures/crosshair_X2") as Texture;
        textures[4] = Resources.Load("UI/Textures/X") as Texture;
        textures[5] = Resources.Load("UI/Textures/crosshair_dot3") as Texture;
        textures[6] = Resources.Load("UI/Textures/crosshair_dot4") as Texture;
        textures[7] = Resources.Load("UI/Textures/crosshair_dot5") as Texture;
        textures[8] = Resources.Load("UI/Textures/crosshair_dot6") as Texture;
        textures[9] = Resources.Load("UI/Textures/crosshair_dot7") as Texture;
        textures[10] = Resources.Load("UI/Textures/crosshair_dot8") as Texture;
        textures[11] = Resources.Load("UI/Textures/crosshair_dot9") as Texture;
        textures[12] = Resources.Load("UI/Textures/crosshair_dot10") as Texture;
        textures[13] = Resources.Load("UI/Textures/Attack_Cursors_Revised_Green") as Texture;
        textures[14] = Resources.Load("UI/Textures/Attack_Cursors_Revised_Pink") as Texture;


        crosshair = GameObject.Find("Crosshair");
        crosshairRend = crosshair.GetComponent<Renderer>();
        _target = "invalid";
        _targetPos = new Vector3();
        _targetObject = new GameObject();
    }

    // Update is called once per frame
    void Update() {
        middleScreen = camResize.midScreen;
        /*
        if (SelectionManager.isInSelectMode)
        {
            checkTarget(INP_MouseCursor.position);
        }
        else
        {
            checkTarget(middleScreen);
        }
        */
    }


    public void checkTarget()
    {
        Vector3 position;
        if (SelectionManager.isInSelectMode)
        {
            position = INP_MouseCursor.position;
        }
        else
        {
            position = middleScreen;
        }

        ray = worldCamera.ScreenPointToRay(position);
        //please don't change to spherecastall! if you think you should ask Harold
        //var myHits = Physics.SphereCastAll(ray, 1, maxDistance, layermask);
        //if (myHits.Length > 0)
        if (Physics.SphereCast(ray, 0.3f, out hit, maxDistance, layermask))
        {
            if (hit.transform.tag == "Minion")
            {
                ENT_Body body = hit.transform.gameObject.GetComponent<ENT_Body>();
                try
                {
                    if (!body.isAlly)
                    {
                        _target = "enemy";
                        _targetObject = hit.transform.gameObject;
                        crosshairRend.material.mainTexture = textures[14];
                        crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
                    }
                    else
                    {
                        _targetObject = hit.transform.gameObject;
                    }
                }
                catch (System.Exception e)
                {
                    print("Error hovering over: " + hit.transform.gameObject);
                    throw e;

                }
            }

            else if (hit.transform.tag == "enemyStructure")
            {
                _target = "enemy";
                _targetObject = hit.transform.root.gameObject;
                //print("TargetObj: " + _targetObject);
                crosshairRend.material.mainTexture = textures[14];
                crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
                //print("hovering over structure" + hit.transform.gameObject.name);
            }
            else if (hit.transform.tag == "splicer")
            {
                _target = "splicer";
                _targetObject = hit.transform.gameObject;
                //_targetPos = hit.point;
                crosshairRend.material.mainTexture = textures[5];
                crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);

                //Debug.Log("UI Splicer Working");
            }
            else if (hit.transform.tag == "ground")
            {
                _target = "neutral";
                _targetPos = hit.point;
                _targetObject = hit.transform.gameObject;
                crosshairRend.material.mainTexture = textures[13];
                crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
            }
            else
            {
                _targetObject = null;
                _target = "invalid";
                //print("TargetObj is null");
            }
        }
        else
        {
            _target = "";
            _targetObject = null; //THIS LINE MAY NEED TO BE TAKEN OUT
            crosshairRend.material.mainTexture = textures[0];
            crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }
    }
}
