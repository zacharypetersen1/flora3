﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIX_AmmoBar : MonoBehaviour {

    public GameObject ammoBar;
    public Image img;

    UIX_Bar redBar;

    static UIX_AmmoBar singleton;

    // Use this for initialization
    void Start () {
        ammoBar = GameObject.Find("PurifyFill");
        img = ammoBar.GetComponent<Image>();
        singleton = this;
    }

    

    // Update is called once per frame
    void Update () {
		
	}

    public static void setAmmoValue(float ammount)
    {
        singleton.img.fillAmount = ammount;
    }
}