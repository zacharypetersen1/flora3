﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_Bar : MonoBehaviour {
    //Serialized Fields so they can be seen in the Unity's Inspector
    [SerializeField]
    private float maxValue = 0;
    [SerializeField]
    private float curValue = 0;
    private float prevValue = 0;
    [SerializeField]
    private Color barColor = Color.green;
    private float offset;
    private float startingScaleX;
    ENT_DungeonEntity parent;
    Camera cam;

    public float MaxValue
    {
        get { return maxValue; }
        set { maxValue = value; }
    }

    public float CurrentValue
    {
        get { return curValue; }
        set { if (value <= maxValue) { curValue = value; } }
    }

    public Color BarColor
    {
        get { return barColor; }
        set { barColor = value; transform.GetComponent<SpriteRenderer>().color = value; }
    }

    // Use this for initialization
    void Start () {
        parent = transform.GetComponentInParent<ENT_DungeonEntity>();
        // NOTE: For finding values dynamically if not set implicitly

        transform.GetComponent<SpriteRenderer>().color = BarColor;
        prevValue = CurrentValue;
        startingScaleX = transform.localScale.x;
        offset = transform.localPosition.y;
        cam = Camera.main;
    }
	
	// Update is called once per frame
	void Update () {
        if (maxValue == 0) return;
        float new_value = curValue / maxValue;
        //Left justify the bar. Must scale percentage to pixels
        float missing_val = (maxValue - curValue) / maxValue;
        //transform.position = new Vector3(missing_val * 0.5f + startingPosX, transform.position.y, transform.position.z);
        transform.localScale = new Vector3(new_value, transform.localScale.y, 1);
        transform.localPosition = new Vector3(missing_val * 0.5f, 0, 0);
        transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);
    }

    //This function shrinks the bar's x scale based on the variable
    /*public void scaleToValue()
    {
        float new_value = curValue / maxValue;
        //Left justify the bar. Must scale percentage to pixels
        float missing_val = (maxValue - curValue) / maxValue;
        //transform.position = new Vector3(missing_val * 0.5f + startingPosX, transform.position.y, transform.position.z);
        
        transform.localScale = new Vector3(new_value, transform.localScale.y, 1);
        transform.position = new Vector3(parent.transform.position.x + missing_val * 0.5f, parent.transform.position.y + offset, parent.transform.position.z);
        transform.localPosition += new Vector3(missing_val * 0.5f, 0, 0);
        transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
        
    }*/
}
