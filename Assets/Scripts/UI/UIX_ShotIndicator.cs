﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIX_ShotIndicator : MonoBehaviour {

    static UIX_ShotIndicator singleton;
    Image img;

    void Awake()
    {
        singleton = this;
        img = GetComponent<Image>();
    }

	// Use this for initialization
	void Start ()
    {

    }
	


    //
    // Sets UI launch shot image
    //
    public static void setShotIndicator(ABI_Purify.purifyTypes shotType)
    {
        singleton.img.sprite = UTL_Resources.getShotUISprite(shotType);
    }
}
