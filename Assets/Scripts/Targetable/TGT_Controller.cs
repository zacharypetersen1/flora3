﻿// Andrei
using System.Collections.Generic;
using UnityEngine;

public class TGT_Controller : MonoBehaviour
{

    public static TGT_Controller controller;
    public PriorityQueue<TGT_Targetable> targetQueue = new PriorityQueue<TGT_Targetable>();
    public static float maxDist = 10; // Max lock on distance
    public static List<TGT_Targetable> viableTargets = new List<TGT_Targetable>(); // Hoping to replace this with targetQueue if we can make targetQueue iterable

    TGT_Targetable curTarget = null;
    TGT_Targetable topTarget = null;
    static int angleWeight = 1; // Weight of angle for priority calculation
    static int distWeight = 0;  // Weight of distance for priority calculation
    static int enemyWeight = 0; // Weight of being an enemy for priority calculation



    //
    // Calculates priority on objects
    //
    public static float calcPriority(TGT_Targetable target)
    {
        float dist = target.getDistance();
        float angle = target.getAngleToTwig();
        if (angle > 180) angle = 360 - angle;
        int isEnemy = target.isEnemy ? 0 : 100;
        float angleUnits = 180 / 100;
        float distUnits = maxDist / 100;
        float priority = (angle / angleUnits) * angleWeight + (dist / distUnits) * distWeight + isEnemy * enemyWeight;
        return priority;
    }



    //
    // Use this for initialization
    //
    void Start()
    {
        controller = this;
    }



    //
    // Update is called once per frame
    //
    void Update()
    {
        // Debug stuff that changes color of top priority object
        /*targetQueue.clear();
        viableTargets.Clear();*/
    }



    //
    // Sets current target to be the target with the smallest delta angle to current target
    //
    TGT_Targetable getNextTarget(bool clockwise)
    {
        float curAngleOffset = resetDegree(curTarget.getAngleToTwig());
        curAngleOffset *= -1;
        TGT_Targetable bestTarget = null;
        float bestAngle = 0;
        foreach (TGT_Targetable target in viableTargets)
        {
            if (target == curTarget) continue;
            if (bestTarget == null)
            {
                bestTarget = target;
                bestAngle = resetDegree(bestTarget.getAngleToTwig() + curAngleOffset);
            }
            else
            {
                float targAngle = resetDegree(target.getAngleToTwig());
                if (clockwise)
                {
                    if (resetDegree(targAngle + curAngleOffset) < bestAngle)
                    {
                        bestAngle = targAngle;
                        bestTarget = target;
                    }
                }
                else
                {
                    if (resetDegree(targAngle + curAngleOffset) > bestAngle)
                    {
                        bestAngle = targAngle;
                        bestTarget = target;
                    }
                }
            }
        }
        if (bestTarget == null) bestTarget = curTarget;
        foreach (TGT_Targetable target in viableTargets)
        {
            float targetAngle = resetDegree(target.getAngleToTwig());
            Debug.Log(targetAngle);
        }
        return bestTarget;
    }



    //
    // Returns false if no target is currently selected, true otherwise
    //
    public bool targetSelected()
    {
        return curTarget != null;
    }



    //
    // Adjusts given angle to be >= 0 and < 360
    //
    float resetDegree(float angle)
    {
        while (angle < 0)
        {
            angle += 360;
        }
        while (angle >= 360)
        {
            angle -= 360;
        }
        return angle;
    }



    //
    // LateUpdate is called after each
    //
    void LateUpdate()
    {
        // LB was pressed
        /*if (Input.GetKeyDown(KeyCode.Joystick1Button4))
        {
            if (curTarget != null)
            {
                deselectAction();
                curTarget = getNextTarget(false);
                selectAction();
            }
            else
            {
                setCurToTop();
            }
        }
        // RB was pressed
        if (Input.GetKeyDown(KeyCode.Joystick1Button5))
        {
            if (curTarget != null)
            {
                deselectAction();
                curTarget = getNextTarget(true);
                selectAction();
            }
            else
            {
                setCurToTop();
            }
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
            if (curTarget == null)
            {
                setCurToTop();
            }
            else
            {
                deselectAction();
                curTarget = null;
            }
        }*/
    }



    //
    // Sets current target to point to highest priority target
    //
    void setCurToTop()
    {
        topTarget = targetQueue.peek();
        if (topTarget != null)
        {
            if (curTarget != null) deselectAction();
            curTarget = topTarget;
            selectAction();
        }
    }



    //
    // Code to execute upon selecting target
    //
    void selectAction()
    {
        curTarget.gameObject.GetComponent<Renderer>().material.color = Color.red;
        curTarget.isTarget = true;
    }



    //
    // Code to execute upon deselecting target
    //
    void deselectAction()
    {
        curTarget.gameObject.GetComponent<Renderer>().material.color = Color.white;
        curTarget.isTarget = false;
    }
}