﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

// Thanks TowerOfBricks (https://answers.unity.com/questions/8480/how-to-scrip-a-saveload-game-option.html)

[Serializable ()]
public class SAV_SaveFile : ISerializable {
    public SerializableVec3 playerPos; //position to spawn player
    public bool atFinalBattle; //if the player has gotten to the the final battle yet or not
    public List<string> structuresDestroyed; //list of structures that have been destroyed (names)
    public List<TUT_Messages.pauseMessages> pausedMessages; //what tutorial messages have been played
    public List<TUT_Messages.Messages> messages; //what dialogue messages have been played
    public List<ENT_DungeonEntity.monsterTypes> allies; //list of allies
    public List<ENT_DungeonEntity.monsterTypes> unlockedTypes; //data structure representing what Flora have been unlocked
    public float sensitivity; //mouse sensitivity
    public float playerHealth;  //player health
    public List<float> allyHealth; //ally health -- same order as allies
    public List<string> minionNames; // Minion names

    public SAV_SaveFile() { }

    public SAV_SaveFile(SerializationInfo info, StreamingContext ctxt) //called automatically in ISerializable
    {
        playerPos           = (SerializableVec3)info.GetValue("playerPos", typeof(SerializableVec3));
        atFinalBattle       = (bool)info.GetValue("atFinalBattle", typeof(bool));
        structuresDestroyed = (List<string>)info.GetValue("structuresDestroyed", typeof(List<string>));
        pausedMessages      = (List<TUT_Messages.pauseMessages>)info.GetValue("pausedMessages", typeof(List<TUT_Messages.pauseMessages>));
        messages            = (List<TUT_Messages.Messages>)info.GetValue("messages", typeof(List<TUT_Messages.Messages>));
        allies              = (List<ENT_DungeonEntity.monsterTypes>)info.GetValue("allies", typeof(List<ENT_DungeonEntity.monsterTypes>));
        unlockedTypes       = (List<ENT_DungeonEntity.monsterTypes>)info.GetValue("unlockedTypes", typeof(List<ENT_DungeonEntity.monsterTypes>));
        sensitivity         = (float)info.GetValue("sensitivity", typeof(float));
        playerHealth        = (float)info.GetValue("playerHealth", typeof(float));
        allyHealth          = (List<float>)info.GetValue("allyHealth", typeof(List<float>));
        minionNames         = (List<string>)info.GetValue("minionNames", typeof(List<string>));
    }

    public void GetObjectData (SerializationInfo info, StreamingContext context) //called automatically in ISerializable
    {
        info.AddValue("playerPos", playerPos);
        info.AddValue("atFinalBattle", atFinalBattle);
        info.AddValue("structuresDestroyed", structuresDestroyed);
        info.AddValue("pausedMessages", pausedMessages);
        info.AddValue("messages", messages);
        info.AddValue("allies", allies);
        info.AddValue("unlockedTypes", unlockedTypes);
        info.AddValue("sensitivity", sensitivity);
        info.AddValue("playerHealth", playerHealth);
        info.AddValue("allyHealth", allyHealth);
        info.AddValue("minionNames", minionNames);
    }
}


// === This is the class that will be accessed from scripts ===
public class SaveLoad
{

    public static string currentFilePath = "SaveData.flora";    // Edit this for different save files

    //playerPos gotten from floraStone at time of saving
    //atFinalBattle gotten from ENC_EndGameController
    public static List<string> structuresDestroyed = new List<string>(); //must add structures in here when they are destroyed
    public static List<TUT_Messages.pauseMessages> pauseMessagesShown = new List<TUT_Messages.pauseMessages>(); //must add pausemessages in here when they are shown
    public static List<TUT_Messages.Messages> messagesShown = new List<TUT_Messages.Messages>(); //must add messages here when they are shown
    //list of allies gotten from PLY_AllyManager
    //list of unlocked types gotten from ENT_PlayerBody
    //sensitivity from INP_PlayerInput
    //playerHealth from playerBody
    //allyHealth from list of allies

    // Call this to write data
    public static void Save()  // Overloaded
    {
        Save(currentFilePath);
    }
    public static void Save(string filePath)
    {
        SAV_SaveFile data = new SAV_SaveFile();

        GameObject player = GameObject.Find("Player");
        data.playerPos = new SerializableVec3(player.transform.position);
        data.atFinalBattle = GameObject.Find("EndGameManager").GetComponent<ENC_EndGameController>().isStarted;
        data.structuresDestroyed = structuresDestroyed;
        data.pausedMessages = pauseMessagesShown;
        data.messages = messagesShown;
        data.allies = new List<ENT_DungeonEntity.monsterTypes>();
        List<GameObject> allies = player.GetComponent<PLY_AllyManager>().getAllAllies;
        data.allyHealth = new List<float>();
        data.minionNames = new List<string>();
        foreach (GameObject ally in allies)
        {
            data.allies.Add(ally.GetComponent<ENT_Body>().entity_type);
            data.allyHealth.Add(ally.GetComponent<ENT_Body>().CL);
            data.minionNames.Add(ally.GetComponent<ENT_Body>().minionName);
        }
        data.unlockedTypes = player.GetComponent<ENT_PlayerBody>().unlocked_types;
        data.sensitivity = INP_PlayerInput.sensativity;
        data.playerHealth = player.GetComponent<ENT_PlayerBody>().CL;

        Stream stream = File.Open(filePath, FileMode.Create);
        BinaryFormatter bformatter = new BinaryFormatter();
        bformatter.Binder = new VersionDeserializationBinder();
        bformatter.Serialize(stream, data);
        stream.Close();
    }

    // Call this to load from a file into "data"
    public static bool Load() { return Load(currentFilePath); }   // Overloaded
    public static bool Load(string filePath)
    {
        try
        {
            SAV_SaveFile data = new SAV_SaveFile();
            Stream stream = File.Open(filePath, FileMode.Open);
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            data = (SAV_SaveFile)bformatter.Deserialize(stream);
            stream.Close();

            GameObject player = GameObject.Find("Player");
            player.transform.position = data.playerPos.realVec;

            if (data.atFinalBattle)
            {
                GameObject endGameManager = GameObject.Find("EndGameManager");
                endGameManager.GetComponent<ENC_EndGameController>().ScriptStartFinalBattle();
            }

            //structuresDestroyed
            foreach (string toDestroy in data.structuresDestroyed)
            {
                GameObject structure = GameObject.Find(toDestroy);
                structuresDestroyed.Add(toDestroy);
                if (structure)
                {
                    GameObject.Destroy(structure);

                }
            }

            foreach (TUT_Messages.pauseMessages pMessage in data.pausedMessages)
            {
                TUT_PauseMessageManager.SkipMessage(pMessage);
                pauseMessagesShown.Add(pMessage); //for second save
            }

            foreach (TUT_Messages.Messages message in data.messages)
            {
                TUT_TutorialManager.SkipMessage(message);
                messagesShown.Add(message); //for second save
            }
            INP_PlayerInput.sensativity = data.sensitivity;
            player.GetComponent<ENT_PlayerBody>().CL = data.playerHealth;

            return true;
        }
        catch (Exception e)
        {
            Debug.Log("Could not load save file");
            Debug.LogError(e);
            return false;
        }
    }

    public static bool LoadMinions() { return LoadMinions(currentFilePath); }
    public static bool LoadMinions(string filePath)
    {
        try
        {
            SAV_SaveFile data = new SAV_SaveFile();
            Stream stream = File.Open(filePath, FileMode.Open);
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            data = (SAV_SaveFile)bformatter.Deserialize(stream);
            stream.Close();

            GameObject player = GameObject.Find("Player");
            foreach (ENT_DungeonEntity.monsterTypes unlocked in data.unlockedTypes)
            {
                ENT_PlayerBody pb = player.GetComponent<ENT_PlayerBody>();
                ABI_Purify.rezUnlocked = true;
                int floraStoneCount = ENT_SplicerController.allFloraStones.Count;
                for (int i = 0; i < floraStoneCount; ++i)
                {
                    ENT_SplicerController floraStone = ENT_SplicerController.allFloraStones[i];
                    floraStone.splicerEmitter.Persist = false;
                }
                if (!pb.unlocked_types.Contains(unlocked))
                {
                    player.GetComponent<ENT_PlayerBody>().unlocked_types.Add(unlocked);
                    player.GetComponent<PLY_AllyManager>().MaxFollowers += 5;
                }
            }

            Vector3 randomOffset = new Vector3(0, 0, 0);
            for (int i = 0; i < data.allies.Count; ++i)
            {
                ENT_DungeonEntity.monsterTypes ally = data.allies[i];
                randomOffset.x = UnityEngine.Random.Range(-5.0f, 5.0f);
                randomOffset.z = UnityEngine.Random.Range(-5.0f, 5.0f);
                GameObject newAlly = UTL_Dungeon.spawnMinion(ally, player.transform.position, true);
                newAlly.GetComponent<ENT_Body>().CL = data.allyHealth[i];
                newAlly.GetComponent<ENT_Body>().minionName = data.minionNames[i];
            }
            return true;
        }
        catch (Exception e)
        {
            Debug.Log("Could not load save file");
            Debug.LogError(e);
            return false;
        }
    }

    public static bool SaveFileExists()
    {
        return File.Exists(currentFilePath);
    }
}

// === This is required to guarantee a fixed serialization assembly name, which Unity likes to randomize on each compile
// Do not change this
public sealed class VersionDeserializationBinder : SerializationBinder
{
    public override Type BindToType(string assemblyName, string typeName)
    {
        if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
        {
            Type typeToDeserialize = null;

            assemblyName = Assembly.GetExecutingAssembly().FullName;

            // The following line of code returns the type. 
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return typeToDeserialize;
        }

        return null;
    }
}

//needed because unity is dumb and doesn't let you serialize a vector. WTF PEOPLE!
//I blame trevor for this one
[Serializable]
public class SerializableVec3 : ISerializable
{
    public Vector3 realVec;

    public SerializableVec3(Vector3 vec)
    {
        realVec = vec;
    }
    public SerializableVec3(SerializationInfo info, StreamingContext context) //called automatically in ISerializable
    {
        realVec.x = (float)info.GetValue("x", typeof(float));
        realVec.y = (float)info.GetValue("y", typeof(float));
        realVec.z = (float)info.GetValue("z", typeof(float));

    }
    public void GetObjectData(SerializationInfo info, StreamingContext context) //called automatically in ISerializable
    {
        info.AddValue("x", realVec.x);
        info.AddValue("y", realVec.y);
        info.AddValue("z", realVec.z);
    }
}
