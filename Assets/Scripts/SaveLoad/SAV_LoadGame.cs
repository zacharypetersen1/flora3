﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SAV_LoadGame : MonoBehaviour {

    public static bool wantLoad = false;
    public static bool resetLevel = false;

#if UNITY_EDITOR
    private static bool debugLoaded = false;
#endif

    // Use this for initialization
    void Start () {
        if (!SaveLoad.SaveFileExists())
        {
            GameObject continueButton = GameObject.Find("Continue");
            if (continueButton) continueButton.SetActive(false);
        }
#if UNITY_EDITOR
        debugLoaded = false;
        if (DBG_DebugOptions.Get().loadDebugSaves)
        {
            DebugLoad();
        }
#endif
    }

    private void OnLevelWasLoaded(int level)
    {
        if (resetLevel)
        {
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Steam"))
            {
                GameObject player = GameObject.Find("Player");
                if (player)
                {
                    player.GetComponent<PLY_AllyManager>().resetAllyManager();
                    resetLevel = false;
                }

            }
        }
        if (wantLoad)
        {
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Steam"))
            {
                SaveLoad.Load();
                //kill piney :P
                GameObject piney = GameObject.Find("PineyTheHero");
                if (piney)
                {
                    piney.GetComponent<ENT_Body>().cleanse_self();
                    GameObject.DestroyImmediate(piney);
                }
            }
        }
#if UNITY_EDITOR
        else if (DBG_DebugOptions.Get().loadDebugSaves)
        {
            DebugLoad();
        }
#endif
    }

#if UNITY_EDITOR
    void DebugLoad()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Steam"))
        {
            string filename = DBG_DebugOptions.Get().GetDebugSaveFileName();
            bool success = SaveLoad.Load(filename);
            if (!success)
            {
                debugLoaded = true;
                Debug.LogError("Could not load debug save file " + filename + ", does the file exist?");
            }
            else //if success
            {
                GameObject piney = GameObject.Find("PineyTheHero");
                if (piney)
                {
                    piney.GetComponent<ENT_Body>().cleanse_self();
                    GameObject.DestroyImmediate(piney);
                }
            }
        }
        else
            debugLoaded = false;
    }
#endif

    void Update () {
        if (wantLoad)
        {
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Steam"))
            {
                SaveLoad.LoadMinions();
                wantLoad = false;
            }
        }
#if UNITY_EDITOR
        else if (!debugLoaded && DBG_DebugOptions.Get().loadDebugSaves)
        {
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Steam"))
            {
                GameObject piney = GameObject.Find("PineyTheHero");
                if (piney)
                {
                    piney.GetComponent<ENT_Body>().cleanse_self();
                    GameObject.DestroyImmediate(piney);
                }
                string filename = DBG_DebugOptions.Get().GetDebugSaveFileName();
                SaveLoad.LoadMinions(filename);
                debugLoaded = true;
            }
        }
#endif
    }

    static void LoadGame() { wantLoad = true; resetLevel = true; }

    public void LoadGameButton()
    {
        LoadGame();
        GameObject mainMenuObj = GameObject.Find("Scripts");
        if (mainMenuObj)
        {
            UIX_MainMenu mainMenuScript = mainMenuObj.GetComponent<UIX_MainMenu>();
            if (mainMenuScript)
            {
                mainMenuScript.fadeTimeGame();
            }
        }
    }
}
