%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: RedMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/ArmPole.L
    m_Weight: 0
  - m_Path: Armature/ArmPole.L/ArmPole.L_end
    m_Weight: 0
  - m_Path: Armature/ArmPole.R
    m_Weight: 0
  - m_Path: Armature/ArmPole.R/ArmPole.R_end
    m_Weight: 0
  - m_Path: Armature/HandHandle.L
    m_Weight: 0
  - m_Path: Armature/HandHandle.L/HandHandle.L_end
    m_Weight: 0
  - m_Path: Armature/HandHandle.R
    m_Weight: 0
  - m_Path: Armature/HandHandle.R/HandHandle.R_end
    m_Weight: 0
  - m_Path: Armature/LegHandle.L
    m_Weight: 0
  - m_Path: Armature/LegHandle.L/LegHandle.L_end
    m_Weight: 0
  - m_Path: Armature/LegHandle.R
    m_Weight: 0
  - m_Path: Armature/LegHandle.R/LegHandle.R_end
    m_Weight: 0
  - m_Path: Armature/LegPoleTarget.L
    m_Weight: 0
  - m_Path: Armature/LegPoleTarget.L/LegPoleTarget.L_end
    m_Weight: 0
  - m_Path: Armature/LegPoleTarget.R
    m_Weight: 0
  - m_Path: Armature/LegPoleTarget.R/LegPoleTarget.R_end
    m_Weight: 0
  - m_Path: Armature/Root
    m_Weight: 0
  - m_Path: Armature/Root/HipController
    m_Weight: 0
  - m_Path: Armature/Root/HipController/BackFlapUpper.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/BackFlapUpper.L/BackFlapLower.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/BackFlapUpper.L/BackFlapLower.L/BackFlapLower.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/BackFlapUpper.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/BackFlapUpper.R/BackFlapLower.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/BackFlapUpper.R/BackFlapLower.R/BackFlapLower.R_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/AnkleInside.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/AnkleInside.L/AnkleInside.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/AnkleOutside.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/AnkleOutside.L/AnkleOutside.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/Heel.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/Heel.L/Foot.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/Heel.L/Foot.L/Toe.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/Heel.L/Foot.L/Toe.L/Toe.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/Heel.L/UpperFoot.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/LowerLeg.L/Heel.L/UpperFoot.L/UpperFoot.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/SkirtBack.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/SkirtBack.L/SkirtBack.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/SkirtFrontUpper.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/SkirtFrontUpper.L/SkirtFrontLower.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/SkirtFrontUpper.L/SkirtFrontLower.L/SkirtFrontLower.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/SkirtOutside.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.L/UpperLeg.L/SkirtOutside.L/SkirtOutside.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/AnkleInside.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/AnkleInside.R/AnkleInside.R_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/AnkleOutside.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/AnkleOutside.R/AnkleOutside.R_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/Heel.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/Heel.R/Foot.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/Heel.R/Foot.R/Toe.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/Heel.R/Foot.R/Toe.R/Toe.R_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/Heel.R/UpperFoot.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/LowerLeg.R/Heel.R/UpperFoot.R/UpperFoot.R_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/SkirtBack.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/SkirtBack.R/SkirtBack.R_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/SkirtFrontUpper.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/SkirtFrontUpper.R/SkirtFrontLower.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/SkirtFrontUpper.R/SkirtFrontLower.R/SkirtFrontLower.R_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/SkirtOutside.L.001.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/Hip.R/UpperLeg.R/SkirtOutside.L.001.R/SkirtOutside.L.001.R_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/OutsideFlapUpper.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/OutsideFlapUpper.L/OutsideFlapLower.L
    m_Weight: 0
  - m_Path: Armature/Root/HipController/OutsideFlapUpper.L/OutsideFlapLower.L/OutsideFlapLower.L_end
    m_Weight: 0
  - m_Path: Armature/Root/HipController/OutsideFlapUpper.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/OutsideFlapUpper.R/OutsideFlapLower.R
    m_Weight: 0
  - m_Path: Armature/Root/HipController/OutsideFlapUpper.R/OutsideFlapLower.R/OutsideFlapLower.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Rib1_1.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Rib1_1.L/Rib1_2.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Rib1_1.L/Rib1_2.L/Rib1_3.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Rib1_1.L/Rib1_2.L/Rib1_3.L/Rib1_3.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Rib1_1.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Rib1_1.R/Rib1_2.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Rib1_1.R/Rib1_2.R/Rib1_3.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Rib1_1.R/Rib1_2.R/Rib1_3.R/Rib1_3.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Rib2_1.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Rib2_1.L/Rib2_2.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Rib2_1.L/Rib2_2.L/Rib2_3.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Rib2_1.L/Rib2_2.L/Rib2_3.L/Rib2_3.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Rib2_1.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Rib2_1.R/Rib2_2.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Rib2_1.R/Rib2_2.R/Rib2_3.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Rib2_1.R/Rib2_2.R/Rib2_3.R/Rib2_3.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone/Bone.001
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone/Bone.001/Bone.003
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone/Bone.001/Bone.003/Bone.003_end
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone/Bone.002
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone/Bone.002/Bone.004
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone/Bone.002/Bone.004/Bone.004_end
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.008
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.008/Bone.009
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.008/Bone.009/Bone.010
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.008/Bone.009/Bone.010/Bone.011
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.008/Bone.009/Bone.010/Bone.011/Bone.012
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.008/Bone.009/Bone.010/Bone.011/Bone.012/Bone.012_end
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.008/Bone.013
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.008/Bone.013/Bone.013_end
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.014
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/Bone.005/Bone.006/Bone.007/Bone.014/Bone.014_end
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/vineTarget
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Bone.015/vineTarget/vineTarget_end
    m_Weight: 1
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Rib3_1.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Rib3_1.L/Rib3_2.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Rib3_1.L/Rib3_2.L/Rib3_3.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Rib3_1.L/Rib3_2.L/Rib3_3.L/Rib3_3.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Rib3_1.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Rib3_1.R/Rib3_2.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Rib3_1.R/Rib3_2.R/Rib3_3.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Rib3_1.R/Rib3_2.R/Rib3_3.R/Rib3_3.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Rib4_1.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Rib4_1.L/Rib4_2.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Rib4_1.L/Rib4_2.L/Rib4_3.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Rib4_1.L/Rib4_2.L/Rib4_3.L/Rib4_3.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Rib4_1.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Rib4_1.R/Rib4_2.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Rib4_1.R/Rib4_2.R/Rib4_3.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Rib4_1.R/Rib4_2.R/Rib4_3.R/Rib4_3.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Rib5_1.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Rib5_1.L/Rib5_2.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Rib5_1.L/Rib5_2.L/Rib5_3.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Rib5_1.L/Rib5_2.L/Rib5_3.L/Rib5_3.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Rib5_1.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Rib5_1.R/Rib5_2.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Rib5_1.R/Rib5_2.R/Rib5_3.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Rib5_1.R/Rib5_2.R/Rib5_3.R/Rib5_3.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/ShoulderPadBackUpper.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/ShoulderPadBackUpper.L/ShoulderPadBackLower.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/ShoulderPadBackUpper.L/ShoulderPadBackLower.L/ShoulderPadBackLower.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/ShoulderPadTopUpper.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/ShoulderPadTopUpper.L/ShoulderPadTopMid.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/ShoulderPadTopUpper.L/ShoulderPadTopMid.L/ShoulderPadTopLower.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/ShoulderPadTopUpper.L/ShoulderPadTopMid.L/ShoulderPadTopLower.L/ShoulderPadTopLower.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/Hand.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/Hand.L/Hand.Upper.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/Hand.L/Hand.Upper.L/HandMid.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/Hand.L/Hand.Upper.L/HandMid.L/HandLower.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/Hand.L/Hand.Upper.L/HandMid.L/HandLower.L/HandLower.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/Hand.L/Hand.Upper.L/ThumbUpper.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/Hand.L/Hand.Upper.L/ThumbUpper.L/ThumbLower.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/Hand.L/Hand.Upper.L/ThumbUpper.L/ThumbLower.L/ThumbLower.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/handTarget.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.L/ShoulderOuter.L/UpperArm.L/LowerArm.L/handTarget.L/handTarget.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/ShoulderPadBackUpper.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/ShoulderPadBackUpper.R/ShoulderPadBackLower.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/ShoulderPadBackUpper.R/ShoulderPadBackLower.R/ShoulderPadBackLower.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/ShoulderPadTopUpper.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/ShoulderPadTopUpper.R/ShoulderPadTopMid.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/ShoulderPadTopUpper.R/ShoulderPadTopMid.R/ShoulderPadTopLower.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/ShoulderPadTopUpper.R/ShoulderPadTopMid.R/ShoulderPadTopLower.R/ShoulderPadTopLower.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/Hand.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/Hand.R/Hand.Upper.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/Hand.R/Hand.Upper.R/HandMid.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/Hand.R/Hand.Upper.R/HandMid.R/HandLower.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/Hand.R/Hand.Upper.R/HandMid.R/HandLower.R/HandLower.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/Hand.R/Hand.Upper.R/ThumbUpper.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/Hand.R/Hand.Upper.R/ThumbUpper.R/ThumbLower.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/Hand.R/Hand.Upper.R/ThumbUpper.R/ThumbLower.R/ThumbLower.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/handTarget.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/ShoulderInner.R/ShoulderOuter.L.001.R/UpperArm.R/LowerArm.R/handTarget.R/handTarget.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Chin
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Chin/Chin_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/HairController
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/HairController/HairController_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadBackUpper
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadBackUpper/TopHeadBackLower
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadBackUpper/TopHeadBackLower/TopHeadBackLower_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadFront
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadFront/TopHeadFront_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadSide.L
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadSide.L/TopHeadSide.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadSide.R
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/Head/TopHeadSide.R/TopHeadSide.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/LowerHead
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/LowerHead/Hair1
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/LowerHead/Hair1/Hair2
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/LowerHead/Hair1/Hair2/Hair3
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/LowerHead/Hair1/Hair2/Hair3/Hair4
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/LowerHead/Hair1/Hair2/Hair3/Hair4/Hair5
    m_Weight: 0
  - m_Path: Armature/Root/Spine1/Spine2/Spine3/Spine4/Spine5/Spine6/Neck/LowerHead/Hair1/Hair2/Hair3/Hair4/Hair5/Hair5_end
    m_Weight: 0
  - m_Path: Armature/Root/SpineHandle
    m_Weight: 0
  - m_Path: Armature/Root/SpineHandle/SpineHandle_end
    m_Weight: 0
  - m_Path: RedEyeLeftBack
    m_Weight: 1
  - m_Path: RedEyeLeftFront
    m_Weight: 1
  - m_Path: RedEyeRightBack
    m_Weight: 1
  - m_Path: RedEyeRightFront
    m_Weight: 1
  - m_Path: Twig
    m_Weight: 1
  - m_Path: TwigEye.L
    m_Weight: 1
  - m_Path: TwigEye.R
    m_Weight: 1
  - m_Path: TwigMouth
    m_Weight: 1
