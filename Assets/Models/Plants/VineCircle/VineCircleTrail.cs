﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VineCircleTrail : MonoBehaviour {

    ParticleSystem system;
    ParticleSystem.MainModule main;
    CHA_Motor motor;

    // Use this for initialization
    void Start () {
        system = gameObject.GetComponent<ParticleSystem>();
        main = system.main;
        motor = GameObject.Find("Player").GetComponent<CHA_Motor>();
	}
	
	// Update is called once per frame
	void Update () {
        main.startRotationY = -motor.rotation + Mathf.PI/2;// - 280;

	}
}
