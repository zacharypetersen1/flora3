﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfLogAnim : MonoBehaviour {

    Animator playerAnim, logAnim;

	// Use this for initialization
	void Start () {
        logAnim = gameObject.GetComponent<Animator>();
        playerAnim = GameObject.Find("TwigMesh").GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        // Get VelocityAnimScalar from player animator
        logAnim.SetFloat("velocityAnimScalar", playerAnim.GetFloat("velocityAnimScalar"));
	}
}
