﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Flora/Terrain" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_MapTex("Map Texture", 2D) = "white" {}
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_NoiseScalar("Noise Scalar", float) = 1
		_Octives("Noise Octives", int) = 3
		_GrassTex("Grass Texture", 2D) = "white" {}
		_GrassCol1("Grass Color 1", Color) = (1,1,1,1)
		_GrassCol2("Grass Color 2", Color) = (1,1,1,1)
		_GrassCol3("Grass Color 3", Color) = (1,1,1,1)
		_GrassCol4("Grass Color 4", Color) = (1,1,1,1)


		_RockTex("Rock Texture", 2D) = "gray" {}
		_CorruptTex("Corrupt Texture", 2D) = "purple" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_TexScalar("Texture Scalar", Range(1,500)) = 1
		_MapScalar("Map Scalar", Float) = 1
		_HighIntensity("High Light Intensity", float) = 1
		_LowIntensity("Low Light Intensity", float) = 0
		_CutoffIntensity("Intensity Cutoff", float) = 0.5
		_RampLevels("Ramp Levels", int) = 2
		_ShadowIntensity("Shadow Intensity", float) = 0.3
	}
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag			

			#include "UnityCG.cginc" // for UnityObjectToWorldNormal
			#include "Lighting.cginc" // for _LightColor0

			// compile shader into multiple variants, with and without shadows
			// (we don't care about any lightmaps yet, so skip these variants)
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			// shadow helper functions and macros
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv     : TEXCOORD0;
				SHADOW_COORDS(1)
				float3 wpos   : TEXCOORD2;
				float4 pos : SV_POSITION;
				fixed4 diff : COLOR0;			// diffuse lighting color
			};

			sampler2D _MainTex;
			sampler2D _MapTex;

			sampler2D _NoiseTex;
			float _NoiseScalar;
			int _Octives;
			
			sampler2D _GrassTex;
			float4 _GrassCol1;
			float4 _GrassCol2;
			float4 _GrassCol3;
			float4 _GrassCol4;
			sampler2D _RockTex;
			sampler2D _CorruptTex;

			float4 _MainTex_ST;
			float _TexScalar;
			float _MapScalar;

			float _HighIntensity;
			float _LowIntensity;
			float _CutoffIntensity;
			int   _RampLevels;
			float _ShadowIntensity;

			v2f vert(appdata v)
			{
				v2f o;
				o.wpos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.uv = v.uv;
				o.pos = UnityObjectToClipPos(v.vertex);

				// get vertex normal in world space
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				// dot product between normal and light direction for
				// standard diffuse (Lambert) lighting
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				// factor in the light color
				o.diff = nl * _LightColor0;
				// compute shadows data
				TRANSFER_SHADOW(o)

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the textures
				fixed4 mapCol  = tex2D(_MapTex, i.wpos.xz / _MapScalar);
				fixed4 rockCol = tex2D(_RockTex, i.uv * _TexScalar);
				fixed4 crptCol = tex2D(_CorruptTex, i.uv * _TexScalar);
				int mult = 1;
				fixed4 noiseCol = fixed4(.5, 1, 1, 1);
				for (int iter = 0; iter < _Octives; iter++) {
					noiseCol += tex2D(_NoiseTex, i.uv * _NoiseScalar * mult);
					mult *= 4;
					noiseCol *= 0.5;
				}
				fixed4 grssCol = tex2D(_GrassTex, i.uv * _TexScalar);
				if (noiseCol.r > 0.6) {
					grssCol *= _GrassCol1;
				}
				else if (noiseCol.r > 0.5) {
					grssCol *= _GrassCol2;
				}
				else if (noiseCol.r > 0.25) {
					grssCol *= _GrassCol3;
				}
				else {
					grssCol *= _GrassCol4;
				}
				
				
				fixed4 col = mapCol.r * rockCol + mapCol.g * grssCol + mapCol.b * crptCol;

				// get shadow attenuation
				fixed shadow = SHADOW_ATTENUATION(i);

				// determine level
				float rampLevel = floor(i.diff * (_RampLevels / _CutoffIntensity));
				float multiplier = _LowIntensity + ((_HighIntensity - _LowIntensity) / _RampLevels) * rampLevel;
				
				// apply lighting
				col *= i.diff * ((1 - _ShadowIntensity) + (_ShadowIntensity * shadow));

				// apply cel effect
				//col *= i.diff > _CutoffIntensity ? _HighIntensity : _LowIntensity;

				return col;
			}
			ENDCG
		}

		// shadow casting support
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
	FallBack "Diffuse"
}
