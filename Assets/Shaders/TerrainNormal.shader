﻿Shader "Flora/TerrainNormal"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_AngleDelta("Anlge Delta Allowance", float) = 0.3
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				half3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				half3 worldNormal : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _AngleDelta;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
			
				// get normal delta from up vector
				float delta = (dot(i.worldNormal, half3(0, 0, 1)));

				if (delta <= _AngleDelta) 
				{
					col.rgb = half3(.3, .9, .3);
				}
				else 
				{
					col.rgb = half3(.5, .5, .5);
				}
				return col;
			}
			ENDCG
		}
	}
}
