﻿Shader "Custom/CelTerrainFirstPass" {
	Properties{

		// Control Texture ("Splat Map")
		[HideInInspector] _Control("Control (RGBA)", 2D) = "red" {}

		// Terrain textures - each weighted according to the corresponding colour
		// channel in the control texture
		[HideInInspector] _Splat3("Layer 3 (A)", 2D) = "white" {}
		[HideInInspector] _Splat2("Layer 2 (B)", 2D) = "white" {}
		[HideInInspector] _Splat1("Layer 1 (G)", 2D) = "white" {}
		[HideInInspector] _Splat0("Layer 0 (R)", 2D) = "white" {}

		// Used in fallback on old cards & also for distant base map
		[HideInInspector] _MainTex("BaseMap (RGB)", 2D) = "white" {}
		[HideInInspector] _Color("Main Color", Color) = (1,1,1,1)

		// Let the user assign a lighting ramp to be used for toon lighting
		_Ramp("Toon Ramp (RGB)", 2D) = "gray" {}
		_Ramp2("Toon Ramp2(RGB)", 2D) = "gray" {}
		_FogRamp("Fog Ramp", 2D) = "white" {}

		// Colour of toon outline
		_OutlineColor("Outline Color", Color) = (0,0,0,1)
		_Outline("Outline width", Range(.002, 0.03)) = .005
	}

	SubShader{
		Tags{
			"SplatCount" = "4"
			"Queue" = "Geometry-100"
			"RenderType" = "Opaque"
		}

		// TERRAIN PASS 
		CGPROGRAM
		#pragma surface surf ToonRamp exclude_path:prepass
		#pragma target 4.0

		// Access the Shaderlab properties
		uniform sampler2D _Control;
		uniform sampler2D _Splat0,_Splat1,_Splat2,_Splat3;
		uniform fixed4 _Color;
		uniform sampler2D _Ramp, _Ramp2, _FogRamp;

		// Custom lighting model that uses a texture ramp based
		// on angle between light direction and normal
		inline half4 LightingToonRamp(SurfaceOutput s, half3 lightDir, half atten)
		{
			#ifndef USING_DIRECTIONAL_LIGHT
			lightDir = normalize(lightDir);
			#endif
			// Wrapped lighting
			half d = dot(s.Normal, lightDir) * 0.5 + 0.5;
			// Applied through ramp
			half3 ramp = tex2D(_Ramp, float2(d,d)).rgb;
			half3 ramp2 = tex2D(_Ramp2, float2(d,d)).rgb;
			half3 colClose = s.Albedo.rgb * ramp * (atten * 5.6);
			half3 colFar = s.Albedo.rgb * ramp2 * (atten * 5.6);

			s.Alpha = clamp(s.Alpha/1500, 0, 1);
			half4 c;
			c.rgb = colClose * (1-s.Alpha) + colFar*s.Alpha;

			c.a = 0;
			return c;
		}

		// Surface shader input structure
		struct Input {
			float2 uv_Control : TEXCOORD0;
			float2 uv_Splat0 : TEXCOORD1;
			float2 uv_Splat1 : TEXCOORD2;
			float2 uv_Splat2 : TEXCOORD3;
			float2 uv_Splat3 : TEXCOORD4;
			float4 screenPos : TEXCOORD5;
		};

		// Surface Shader function
		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 splat_control = tex2D(_Control, IN.uv_Control);
			fixed4 col;
			col = splat_control.r * tex2D(_Splat0, IN.uv_Splat0);
			col += splat_control.g * tex2D(_Splat1, IN.uv_Splat1);
			col += splat_control.b * tex2D(_Splat2, IN.uv_Splat2);
			col += splat_control.a * tex2D(_Splat3, IN.uv_Splat3);
			fixed4 fogCol = tex2D(_FogRamp, float2(clamp(IN.screenPos.a / 4000, 0, 1), 0));
			o.Albedo = lerp(col.rgb, fogCol.rgb, fogCol.a) * 0.35;
			o.Alpha = IN.screenPos.a;
		}
		ENDCG

	} // End SubShader

	// Specify dependency shaders   
	Dependency "AddPassShader" = "Custom/CelTerrainAddPass"

	// Fallback to Diffuse
	Fallback "Diffuse"

} // Ehd Shader