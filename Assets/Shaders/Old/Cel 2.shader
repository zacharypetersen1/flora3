﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Flora/Cel2"
{
	Properties
	{
		_Color("Color Modifier", Color) = (1, 1, 1, 1)
		[NoScaleOffset] _MainTex("Main Texture", 2D) = "white" {}
		[NoScaleOffset] _RampTex("Ramp Texture", 2D) = "gray" {}
		
		_RampLevels("Ramp Levels", Range(0, 10)) = 2
		_RampIntensity("Ramp Intensity", Range(0, 1)) = 0.3
		
		_LightScalar("Light Scalar", Range(0, 10)) = 1
		_CutoffIntensity("Light Intensity Cutoff", float) = 0.5

		_HighIntensity("High Light Intensity", Range(0, 10)) = 1
		_HighColor("High Light Color", Color) = (1, 1, 1, 1)

		_LowIntensity("Low Light Intensity", Range(0, 10)) = 0
		_LowColor("Low Light Color", Color) = (1, 1, 1, 1)

		_GradYCap("Gradient Y Cap", Range(0, 10)) = 1
		_GradIntensity("Gradient Intensity", Range(0, 1)) = 0.3

		_RimPower("Rim Power", float) = 1
		_RimScalar("Rim Scalar", Range(0, 10)) = 1.4
	}
	
	SubShader
	{
		Pass
		{
			// indicate that our pass is the "base" pass in forward
			// rendering pipeline. It gets ambient and main directional
			// light data set up; light direction in _WorldSpaceLightPos0
			// and color in _LightColor0
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc" // for UnityObjectToWorldNormal
			#include "Lighting.cginc" // for _LightColor0

			// compile shader into multiple variants, with and without shadows
			// (we don't care about any lightmaps yet, so skip these variants)
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			// shadow helper functions and macros
			#include "AutoLight.cginc"

			struct v2f
			{
				float2 uv : TEXCOORD0;
				SHADOW_COORDS(1)
				float3 locPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				float3 worldPos : TEXCOORD4;
				fixed4 diff : COLOR0; // diffuse lighting color
				float4 pos : SV_POSITION;
			};

			v2f vert(appdata_base v)
			{
				v2f o;
				o.locPos = v.vertex;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;
				
				// get normal (? space)
				o.worldNormal = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);

				// get position in world space
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);

				// get vertex normal in world space
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				
				// dot product between normal and light direction for
				// standard diffuse (Lambert) lighting
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				
				// factor in the light color
				o.diff = nl * _LightColor0;
				
				// compute shadows data
				TRANSFER_SHADOW(o)

				return o;
			}

			float4    _Color;
			sampler2D _MainTex;
			sampler2D _RampTex;
			int       _RampLevels;
			float     _RampIntensity;
			float     _LightScalar;
			float     _CutoffIntensity;
			float     _HighIntensity;
			float4    _HighColor;
			float     _LowIntensity;
			float4    _LowColor;
			float     _GradYCap;
			float     _GradIntensity;
			float     _RimPower;
			float    _RimScalar;

			fixed4 frag(v2f i) : SV_Target
			{
				_RampLevels -= 1;

				// get view direction && light direction for rim lighting
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				// sample texture
				fixed4 col = tex2D(_MainTex, i.uv);
				
				// Rim Lighting
				float3 rim = pow(1.0 - saturate(dot(viewDirection, i.worldNormal)), _RimPower);
				float3 rimLighting = saturate(dot(i.worldNormal, lightDirection)) * rim;

				// get shadow attenuation
				fixed shadow = SHADOW_ATTENUATION(i);

				// calculate light intensity
				float intensity = clamp(i.diff.b * _LightScalar, 0, 1);
				
				// factor in the ramp texture
				float rampOffset = (tex2D(_RampTex, i.uv).b - 0.5) * _RampIntensity;
				intensity = clamp (intensity + rampOffset, 0, 1);
				
				// factor in the shadow
				intensity *= shadow;

				
				// determine level
				float rampLevel = round(intensity * _RampLevels);
				
				// get light multiplier based on level
				float lightMultiplier = _LowIntensity + ((_HighIntensity - _LowIntensity) / (_RampLevels)) * rampLevel;

				// get color multiplier based on level
				float4 highColor = (rampLevel / _RampLevels) * _HighColor;
				float4 lowColor = ((_RampLevels - rampLevel) / _RampLevels) * _LowColor;
				float4 mixColor = (highColor + lowColor) / 2;

				// multiply by lighting
				//col *= intensity > _CutoffIntensity ? _HighIntensity : _LowIntensity + 0.4 * i.diff;// +clamp(i.locPos.y, 0, 1)*0.3;
				
				// apply light multiplier
				col *= lightMultiplier;

				col *= _Color * mixColor;

				// apply vertical gradient
				col *= (1 - _GradIntensity/2 +  clamp(i.locPos.y, 0, _GradYCap)*_GradIntensity);
				
				//return float4(rimLighting, 1);

				// apply rim lighting
				if (rimLighting.b >= 0.2) {
					col *= _RimScalar;
				}
				//col += float4(rimLighting, 0);

				//col += rem_col * .3 * i.diff;
				/*col += i.diff / 2;
				if (i.diff > _CutoffIntensity)
				{
					col *= .5;
				}*/
				return col;
			}
				
			ENDCG
		}

		// shadow casting support
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}