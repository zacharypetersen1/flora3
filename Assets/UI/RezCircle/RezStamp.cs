﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RezStamp : MonoBehaviour {

    public Color color;
    public AnimationCurve alphaCurve, timeCurve;
    public float maxTime;
    float curTime = 0;
    Material mat;

	// Use this for initialization
	void Start () {
        mat = transform.FindChild("Projector").GetComponent<Projector>().material;
    }
	
	// Update is called once per frame
	void Update () {
        curTime += TME_Manager.getDeltaTime(TME_Time.type.player);
        if(curTime >= maxTime)
        {
            onEnd();
        }
        else
        {
            float a = alphaCurve.Evaluate(curTime / maxTime);
            mat.SetColor("_Color", new Color(color.r, color.g, color.b, a));
            float t = timeCurve.Evaluate(curTime / maxTime);
            mat.SetFloat("_TimeTime", t);
        }
    }

    void onEnd()
    {
        Destroy(this.gameObject);
    }
}
