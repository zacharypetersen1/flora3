﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RezCirclePart1 : MonoBehaviour {

    public AnimationCurve c;
    public float maxTime = 1.5f;
    float curTime = 0;
    float maxSize;
    Projector p;
    Animator a;
    private bool active = false;

	// Use this for initialization
	void Start () {
        p = GetComponent<Projector>();
        maxSize = p.orthographicSize;
        a = GameObject.Find("TwigMesh").GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if(active)
        {
            curTime += TME_Manager.getDeltaTime(TME_Time.type.player);
            
        }
	}

    public void initialize()
    {
        curTime = 0;
        active = true;
    }
}
