﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

//[ExecuteInEditMode]
public class SelectionManager : MonoBehaviour
{
    Color noneColor;
    Color hoveringColor;

    public bool canMakeSelections = true;
    public GameObject hoverProjector;
    public GameObject selectionProjector;

    public enum mouseButtons { selectButton, deselectButton, addButton };

    public static bool isInSelectMode = false;

    //custom inspector values
    public GameObject unitsContainer;

    public bool selectByName = false;
    public string selectedName;

    public KeyCode keyMultiselect;
    public KeyCode keyDeselect;

    public KeyCode keySetControlGroup;
    public KeyCode keyAddToControlGroup;

    bool fullProjector = false;
    bool hoveringMinions = false;
    Projector cursorProjector;

    float colliderLargeRadius = 5;
    float colliderSmallRadius = 1.3f;

    Texture fullHover;
    Texture partHover;

    SEL_Cursor sel_cursor;
    int deselectBounds = 30;            // How much mouse is allowed to move for single click deselect
    float deselectTime = .5f;          // How much time is allowed for single click to deselect
    SphereCollider col;

    bool painting = false;

    List<SelectableObject> selectedGameObjects = new List<SelectableObject>();
    List<SelectableObject> hoveredGameObjects = new List<SelectableObject>();
    List<GameObject> disabledObjects = new List<GameObject>();
    List<ControlGroupUnit> controlGroupUnits = new List<ControlGroupUnit>();

    // all of our selected objects before we start tabbing through them
    List<SelectableObject> groupSelectedGameObjects = new List<SelectableObject>();

    bool isSelecting = false;
    Vector3 mousePosition = Vector3.zero;
    Vector3 mousePositionSave = Vector3.zero; // Used for detecting a deselect click

    bool lastCommandWasSelectAll = false;

    UIX_Pause pauseScript;

    GameObject lastTopMinion;

    struct ControlGroupUnit
    {
        public SelectableObject selectableObject;
        public int controlGroup;

        public ControlGroupUnit(SelectableObject selectableObject, int controlGroup)
        {
            this.selectableObject = selectableObject;
            this.controlGroup = controlGroup;
        }
    }

    struct SelectableObject
    {
        public GameObject gameObject;
        public GameObject projectorGameObject;
        public Projector projector;
        public SelectableUnit selectable;
        public bool isActive;
    }

    // Use this for initialization
    void Start()
    {
        if (!Camera.main)
        {
            Debug.Log("The SelectionManager requires a camera with the tag 'MainCamera'");
        }
        TME_Timer.addTimer("Sel_mouse_down", TME_Time.type.game);
        sel_cursor = GameObject.Find("SelectionCursor").GetComponent<SEL_Cursor>();

        noneColor = new Vector4(0, 1, 1, 1);
        hoveringColor = new Vector4(0, 1, 0, 1);

        fullHover = Resources.Load("UI/Textures/fullhover") as Texture;
        partHover = Resources.Load("UI/Textures/parthover") as Texture;
        cursorProjector = GameObject.Find("SelectionProjector").GetComponent<Projector>();
        col = GameObject.Find("SelectionCursor").GetComponent<SphereCollider>();
        col.radius = colliderLargeRadius;
        pauseScript = GameObject.FindGameObjectWithTag("Scripts").GetComponent<UIX_Pause>();
        lastTopMinion = null;
        //cursorProjector.material.SetTexture("_ShadowTex", fullHover);

    }

    // Update is called once per frame
    void Update()
    {
        if (!Camera.main)
            return;

        if (unitsContainer)
        {

            // Remove selected objects that no longer exist
            selectedGameObjects = selectedGameObjects.Where(i => i.gameObject != null).ToList();
            groupSelectedGameObjects = groupSelectedGameObjects.Where(i => i.gameObject != null).ToList();
            disabledObjects = disabledObjects.Where(i => i != null).ToList();

            setProjectors();
            //if (isInSelectMode) setProjectors();
            //else hideSelections();

            // Remove any disabled objects
            selectedGameObjects = selectedGameObjects.Where(i => !disabledObjects.Any(j => j == i.gameObject)).ToList();
            hoveredGameObjects = hoveredGameObjects.Where(i => !disabledObjects.Any(j => j == i.gameObject)).ToList();
            groupSelectedGameObjects = groupSelectedGameObjects.Where(i => !disabledObjects.Any(j => j == i.gameObject)).ToList();

            if (isInSelectMode)
            {
                if (canMakeSelections)
                {
                    //updateControlGroups();

                    // If we press a mouse button, save mouse location and begin selection
                    if (selDown() || desDown() || addDown())
                    {
                        ClearSelection();
                        painting = true;
                        isSelecting = true;
                        mousePositionSave = getMouse();
                        TME_Timer.setTime("Sel_mouse_down", deselectTime);
                    }

                    // Mouse button release
                    else if (selUp() || desUp() || addUp() || selModeUp())
                    {
                        painting = false;
                        //cursorProjector.material.SetTexture("_ShadowTex", partHover);
                        //col.radius = colliderSmallRadius;
                        if (isSelecting)
                        {
                            if (!TME_Timer.timeIsUp("Sel_mouse_down") && checkMouseBounds(mousePositionSave, getMouse()))
                            {
                                if (sel_cursor.getHovered().Count == 0)
                                {
                                    ClearSelection();
                                }
                                else
                                {
                                    foreach (GameObject selectable in sel_cursor.getHovered())
                                    {
                                        parseGameObjectForSelection(selectable);
                                    }
                                }
                            }
                        }
                        isSelecting = false;
                    }
                    else if (selGet())
                    {
                        //if (painting)
                        //{
                            foreach (GameObject selectable in sel_cursor.getHovered())
                            {
                                parseGameObjectForSelection(selectable);
                            }
                        //}
                        //else
                        //{
                        //    if (!checkMouseBounds(mousePositionSave, getMouse()))
                        //    {
                        //        painting = true;
                        //        cursorProjector.material.SetTexture("_ShadowTex", fullHover);
                        //        col.radius = colliderLargeRadius;
                        //    }
                        //}
                    }
                    if (sel_cursor.getHovered().Count == 0)
                    {
                        cursorProjector.material.SetColor("_Color", noneColor);
                    }
                    else
                    {
                        cursorProjector.material.SetColor("_Color", hoveringColor);
                    }
                }
            }
        }
    }

    #region Private Methods

    void updateControlGroups()
    {
        bool setControlGroup = false;
        bool addControlGroup = false;

        // Check for 'set' key press
        if (Input.GetKey(keySetControlGroup))
        {
            setControlGroup = true;
        }

        // Check for 'add' key press
        if (Input.GetKey(keyAddToControlGroup))
        {
            addControlGroup = true;
        }

        // Check for num key press
        int pressedNumber = -1;
        for (int i = 0; i < 10; ++i)
        {
            if (Input.GetKeyUp("" + i))
            {
                pressedNumber = i;
            }
        }

        // Set control group
        if (setControlGroup && pressedNumber >= 0)
        {
            controlGroupUnits = controlGroupUnits.Where(i => i.controlGroup != pressedNumber).ToList();
            foreach (SelectableObject selectableObject in selectedGameObjects)
            {
                controlGroupUnits.Add(new ControlGroupUnit(selectableObject, pressedNumber));
            }
        }

        // Add to control group
        else if (addControlGroup && pressedNumber >= 0)
        {
            foreach (SelectableObject selectableObject in selectedGameObjects)
            {
                controlGroupUnits.Add(new ControlGroupUnit(selectableObject, pressedNumber));
            }
        }

        // Select control group
        else if (pressedNumber >= 0)
        {
            ClearSelection();
            foreach (ControlGroupUnit controlGroupUnit in controlGroupUnits)
            {
                if (controlGroupUnit.controlGroup == pressedNumber)
                {
                    AddGameObjectToSelection(controlGroupUnit.selectableObject.gameObject);
                }
            }
        }
    }

    RaycastHit getHoveredObjects()
    {
        // Check for gameobjects directly under cursor
        RaycastHit hoverHitSelection;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(getMouse()), out hoverHitSelection))
        {
            // parseGameObjectForHover(hoverHitSelection.transform.gameObject);
        }

        if (isSelecting)
        {
            foreach (Transform transform in unitsContainer.transform)
            {
                if (IsWithinSelectionBounds(transform.gameObject))
                {
                    // parseGameObjectForHover(transform.gameObject);
                }
            }
        }
        return hoverHitSelection;
    }

    void setProjectors()
    {
        for (int i = 0; i < groupSelectedGameObjects.Count; ++i)
        {
            SelectableObject groupSelectedObject = groupSelectedGameObjects[i];
            if (groupSelectedObject.projectorGameObject)
            {
                //if the minion is also in the selected game objects we will overwrite this projection mode later
                SEL_Projection projectionScript = groupSelectedObject.projectorGameObject.GetComponentInChildren<SEL_Projection>();
                projectionScript.SetProjectionMode(SEL_Projection.selectionMode.group);
            }
        }

        for (int x = 0; x < selectedGameObjects.Count; x++)
        {
            SelectableObject selectedObject = selectedGameObjects[x];
            if (selectedObject.projector)
            {
                if (selectedObject.selectable && selectedObject.selectable.selectionSize > 0)
                {
                    selectedObject.projector.orthographicSize = selectedObject.selectable.selectionSize;
                }
                if (disabledObjects.Contains(selectedObject.gameObject)) detachSelection(selectedObject);
            }
            if (selectedObject.projectorGameObject)
            {
                SEL_Projection projectionScript = selectedObject.projectorGameObject.GetComponentInChildren<SEL_Projection>();
                projectionScript.SetProjectionMode(SEL_Projection.selectionMode.current);
            }
            selectedGameObjects[x] = selectedObject;
        }
    }

    void detachSelection(SelectableObject selectedObject, bool leaveIfInGroup = false, bool skipCallback = false)
    {
        if (!leaveIfInGroup || !groupSelectedGameObjects.Contains(selectedObject))
        {
            DestroyImmediate(selectedObject.projectorGameObject);
        }
        
        if (selectedObject.selectable && !skipCallback)
        {
            selectedObject.selectable.OnEndSelection();
        }
    }

    void hideSelections()
    {
        for (int x = 0; x < selectedGameObjects.Count; x++)
        {
            SelectableObject selectedObject = selectedGameObjects[x];
            if (selectedObject.projector)
            {
                Destroy(selectedObject.projectorGameObject);
            }
            selectedGameObjects[x] = selectedObject;
        }
    }

    void parseGameObjectForSelection(GameObject sender, bool addToGroupSelectionOnly = false)
    {
        bool validObject = true;

        if (disabledObjects.Contains(sender)) validObject = false;

        if (selectByName && sender.name != selectedName)
        {
            validObject = false;
        }

        if (sender.transform.parent != unitsContainer.transform)
        {
            validObject = false;
        }

        if (validObject)
        {
            bool containsObject = false;
            List<SelectableObject> objectsToRemove = new List<SelectableObject>();
            for (int x = 0; x < selectedGameObjects.Count; x++)
            {
                SelectableObject selectedObject = selectedGameObjects[x];
                if (selectedObject.gameObject == sender)
                {
                    containsObject = true;
                    selectedObject.isActive = true;
                }

                selectedGameObjects[x] = selectedObject;

                // If object is selcted, and we are deselcting, deselect the object
                if ((addUp() || (selModeUp() && addGet())) && selectedObject.gameObject == sender)
                {

                    detachSelection(selectedObject);
                    objectsToRemove.Add(selectedObject);
                }
            }

            selectedGameObjects = selectedGameObjects.Except(objectsToRemove).ToList();

            // Object isn't selected, and we're not deselecting
            if ((!containsObject && !addUp() && !addGet() || (selModeUp() && !addGet() && !containsObject)))
            {
                var selectable = sender.GetComponent<SelectableUnit>();
                if (selectable && selectable != selectable.playerOwned)
                {
                    return;
                }

                //see if gameobject already has projector
                GameObject newSelectionObject;
                SEL_Projection projScript = sender.GetComponentInChildren<SEL_Projection>();
                if (!projScript)
                {
                    newSelectionObject = Instantiate(selectionProjector, sender.transform.position, hoverProjector.transform.rotation) as GameObject;
                }
                else
                {
                    newSelectionObject = projScript.gameObject.transform.parent.gameObject;
                }
                

                SelectableObject selectedGameObject = new SelectableObject();
                selectedGameObject.gameObject = sender;
                selectedGameObject.projectorGameObject = newSelectionObject;
                selectedGameObject.isActive = true;

                if (addToGroupSelectionOnly)
                {
                    if (!groupSelectedGameObjects.Contains(selectedGameObject))
                    {
                        groupSelectedGameObjects.Add(selectedGameObject);
                    }
                    return;
                }

                var projector = newSelectionObject.GetComponentInChildren<Projector>();
                if (projector)
                {
                    selectedGameObject.projector = projector;
                }

                if (selectable)
                {
                    selectedGameObject.selectable = selectable;
                    selectedGameObject.selectable.OnBeginSelection();
                    if (projector)
                    {
                        selectedGameObject.projector.orthographicSize = selectedGameObject.selectable.selectionSize;
                    }
                }

                selectedGameObjects.Add(selectedGameObject);
                if (!groupSelectedGameObjects.Contains(selectedGameObject))
                {
                    groupSelectedGameObjects.Add(selectedGameObject);
                }

                newSelectionObject.transform.SetParent(sender.transform);
            }
            //if (!containsObject &&)
        }
    }

    bool checkMouseBounds(Vector3 initialPosition, Vector3 finalPosition)
    {
        float x = Mathf.Abs(initialPosition.x - finalPosition.x);
        float y = Mathf.Abs(initialPosition.y - finalPosition.y);
        return (x < deselectBounds && y < deselectBounds) ;
    }

    void OnGUI()
    {
        if (isSelecting)
        {
            // Create a rect from both mouse positions
            //var rect = Utils.GetScreenRect(mousePosition, getMouse());
            //Utils.DrawScreenRect(rect, new Color(0.2f, 0.8f, 0.2f, 0.25f));
            //Utils.DrawScreenRectBorder(rect, 1, new Color(0.2f, 0.8f, 0.2f));
        }
    }

    bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (!isSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds(camera, mousePosition, getMouse());
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }


    #endregion

    #region Control Scheme Methods

    // Change this function if not using hardware cursor
    Vector3 getMouse()
    {
        return INP_MouseCursor.position;
    }

    
    bool selUp()     { return !pauseScript.isPaused() && Input.GetMouseButtonUp((int)mouseButtons.selectButton);     }
    bool selDown()   { return !pauseScript.isPaused() && Input.GetMouseButtonDown((int)mouseButtons.selectButton);   }
    bool selGet()    { return !pauseScript.isPaused() && Input.GetMouseButton((int)mouseButtons.selectButton);       }
    bool desUp()     { return false; }//Input.GetMouseButtonUp((int)mouseButtons.deselectButton);   }
    bool desDown()   { return false; }//Input.GetMouseButtonDown((int)mouseButtons.deselectButton); }
    bool desGet()    { return false; }//Input.GetMouseButton((int)mouseButtons.deselectButton);     }
    bool addUp()     { return false; }//Input.GetMouseButtonUp((int)mouseButtons.addButton);        }
    bool addDown()   { return false; }//Input.GetMouseButtonDown((int)mouseButtons.addButton);      }
    bool addGet()    { return false; }//Input.GetMouseButton((int)mouseButtons.addButton);          }
    bool selModeUp() { return !pauseScript.isPaused() && INP_PlayerInput.getButtonUp("SelectMode");                  }


    #endregion

    #region Public Methods

    //returns a list of currently selected objects
    public GameObject[] GetSelectedObjects()
    {
        return selectedGameObjects.Select(i => i.gameObject).ToArray();
    }

    public GameObject GetTopSelectedObject()
    {
        lastTopMinion = null;

        // prefer minion closest to the center of the selecion cursor
        GameObject cursorCenterMinion = sel_cursor.centerMinion;
        if (isInSelectMode && cursorCenterMinion != null)
        {
            lastTopMinion = cursorCenterMinion;
        }

        if (lastTopMinion == null)
        {
            for (int i = selectedGameObjects.Count - 1; i >= 0; --i)
            {
                GameObject obj = selectedGameObjects[i].gameObject;
                if (obj != null)
                {
                    lastTopMinion = obj;
                    break;
                }
            }
        }           

        return lastTopMinion;
    }

    public int GetSelectedCount()
    {
        return selectedGameObjects.Count;
    }

    public int GetGroupSelectedCount()
    {
        return groupSelectedGameObjects.Count;
    }

    public GameObject GetSelectedObjectFromIndex(int index)
    {
        if (index < 0 || index > selectedGameObjects.Count)
            return null;

        return selectedGameObjects[index].gameObject;
    }

    public GameObject GetGroupSelectedObjectFromIndex(int index)
    {
        if (index < 0 || index > groupSelectedGameObjects.Count)
            return null;

        return groupSelectedGameObjects[index].gameObject;
    }

    public int GetGroupSelectedIndexFromObject(GameObject go)
    {
        if (!go)
            return -1;

        int maxIndex = groupSelectedGameObjects.Count;
        for (int i = 0; i < maxIndex; ++i)
        {
            SelectableObject selObj = groupSelectedGameObjects[i];
            if (selObj.gameObject == go)
            {
                return i;
            }
        }
        return -1;
    }
    
    public void selectAll()
    {
        List<GameObject> allObjectsList = getSelectables();
        foreach (GameObject selectedObject in allObjectsList)
        {
            parseGameObjectForSelection(selectedObject);
        }
        lastCommandWasSelectAll = true;
    }

    public void toggleSelectAll()
    {
        if (lastCommandWasSelectAll)
        {
            ClearSelection();
        }
        else
        {
            selectAll();
        }
    }

    //returns an ACTUAL list of currently selected objects
    public List<GameObject> GetSelectedObjectsAsList()
    {
        return selectedGameObjects.Select(i => i.gameObject).ToList();
    }

    public List<GameObject> GetHoveredObjects()
    {
        return hoveredGameObjects.Select(i => i.gameObject).ToList();
    }

    //adds a new object to selection
    public void AddGameObjectToSelection(GameObject newObject)
    {
        parseGameObjectForSelection(newObject);
    }

    public void AddGameObjectToGroupSelection(GameObject newObject)
    {
        parseGameObjectForSelection(newObject, true);
    }

    //removes an object from selection
    public void RemoveGameObjectFromSelection(GameObject removedObject)
    {
        foreach (SelectableObject selectedObject in selectedGameObjects)
        {
            if (selectedObject.gameObject == removedObject)
            {
                detachSelection(selectedObject);
                selectedGameObjects.Remove(selectedObject);
                groupSelectedGameObjects.Remove(selectedObject);
            }
        }
    }

    public void disableObject(GameObject objectToDisable)
    {
        disabledObjects.Add(objectToDisable);
    }

    public void enableObject(GameObject objectToEnable)
    {
        disabledObjects.Remove(objectToEnable);
    }

    public void ClearSelection(bool clearGroup = true)
    {
        foreach (SelectableObject selectedObject in selectedGameObjects)
        {
            detachSelection(selectedObject, !clearGroup);
        }
        selectedGameObjects.Clear();
        if (clearGroup)
        {
            foreach (SelectableObject groupSelectedObject in groupSelectedGameObjects)
            {
                detachSelection(groupSelectedObject, false, true);
            }
            groupSelectedGameObjects.Clear();
        }
            
        lastCommandWasSelectAll = false;
    }

    // Returns [filtered] game objects that are in units container
    public List<GameObject> getSelectables()
    {
        List<GameObject> newSelectedObjects = new List<GameObject>();
        bool isFiltered = false;

        if (selectByName && selectedName.Length > 0)
        {
            isFiltered = true;
            newSelectedObjects.AddRange(Utils.FindGameObjectsWithName(selectedName, unitsContainer.transform));
        }

        if (!isFiltered && newSelectedObjects.Count == 0)
        {
            newSelectedObjects.AddRange(Utils.FindGameObjectsInTransform(unitsContainer.transform));
        }
        newSelectedObjects = newSelectedObjects.Where(i => !disabledObjects.Any(j => j == i)).ToList();
        return newSelectedObjects;
    }

    public bool isSelected(GameObject obj)
    {
        return selectedGameObjects.Exists(x => x.gameObject == obj);
    }

    //public bool isSelected(GameObject obj)
    //{
    //    return (GetSelectedObjectsAsList().Contains(obj));
    //}

    #endregion
}