﻿using UnityEngine;

public class ObjectRotator : MonoBehaviour {

    public float xSpeed = 0;
    public float ySpeed = 0;
    public float zSpeed = 0;
    public bool overrideOtherRotations = false;
    Quaternion oldRotation;

    // Use this for initialization
    void Start () {
        oldRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        if (overrideOtherRotations)
        {
            transform.rotation = oldRotation;
        }
        transform.Rotate(new Vector3(xSpeed, ySpeed, zSpeed) * Time.deltaTime);
        oldRotation = transform.rotation;
	}
}
